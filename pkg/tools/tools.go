package tools

import (
	"os"
	"strings"

	"github.com/google/uuid"
)

func SaveFileToFileStorage(fileInputName string, fileContent []byte) (fileName string, err error) {
	extension := strings.Split(fileInputName, ".")[len(strings.Split(fileInputName, "."))-1]
	fileName = uuid.New().String() + "." + extension
	err = os.WriteFile("file-store/"+fileName, fileContent, 0755)
	return
}

func SaveConvertedFileToFileStorage(fileContent []byte) (fileName string, err error) {
	fileInputName := uuid.New().String() + ".jpg"
	extension := strings.Split(fileInputName, ".")[len(strings.Split(fileInputName, "."))-1]
	fileName = uuid.New().String() + "." + extension
	err = os.WriteFile("file-store/"+fileName, fileContent, 0755)
	return
}
