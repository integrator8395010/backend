package repository

import (
	"backend/internal/domain/step"
	"backend/internal/repository/step/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "step"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"step_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateStep(ctx context.Context, step *step.Step) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsStep...).Values(step.Id(), step.KvizId(), step.Title(), step.Answers(), step.CreatedAt(), step.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateStep(ctx context.Context, id uuid.UUID, updateFn func(step *step.Step) (*step.Step, error)) (step *step.Step, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upStep, err := r.oneStepTx(ctx, tx, id)
	if err != nil {
		return
	}

	stepNew, err := updateFn(upStep)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("kviz_id", stepNew.KvizId()).Set("title", stepNew.Title()).Set("answers", stepNew.Answers()).Set("modified_at", stepNew.ModifiedAt()).Where("id = ?", stepNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteStep(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadStepList(ctx context.Context) (stepsList []*step.Step, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsStep...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoStep, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Step])
	if err != nil {
		return nil, err
	}

	stepsList = []*step.Step{}

	for _, step := range daoStep {
		stepIns, err := r.toDomainStep(&step)
		if err != nil {
			return nil, err
		}

		stepsList = append(stepsList, stepIns)
	}
	return
}

func (r *Repository) oneStepTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *step.Step, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsStep...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoStep, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Step])
	if err != nil {
		return nil, err
	}

	return r.toDomainStep(&daoStep)
}
