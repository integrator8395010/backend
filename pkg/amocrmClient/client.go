package amocrmClient

import (
	"fmt"
	"net/http"

	"gitlab.com/kanya384/gotools/logger"
)

const (
	sendLeadComplex   = "/api/v4/leads/complex"
	getPipelinesList  = "/api/v4/leads/pipelines"
	getLeadFieldsList = "/api/v4/leads/custom_fields"
	getUsersList      = "/api/v4/users"
	getStatusesList   = "/api/v4/users"
	refreshPath       = "/oauth2/access_token"
)

type Client struct {
	logger       logger.Interface
	clientId     string
	clientSecret string
	redirectUri  string
	baseDomain   string
	token        string
	refreshToken string
}

func NewAmoClient(baseDomain, clientId, clientSecret, redirectUri, token, refreshToken string, logger logger.Interface) *Client {
	return &Client{
		token:        token,
		refreshToken: refreshToken,
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  redirectUri,
		logger:       logger,
		baseDomain:   baseDomain,
	}
}

func (c *Client) RequestTokenWithAuthorizationCode(authCode string) (token, refreshToken string, err error) {
	requestRefreshData := TokenWithAuthCodeRequest{
		ClientId:     c.clientId,
		ClientSecret: c.clientSecret,
		GrantType:    "authorization_code",
		Code:         authCode,
		RedirectUri:  c.redirectUri,
	}
	response := &RefreshTokenResponse{}
	_, err = c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, refreshPath), http.MethodPost, "application/json", requestRefreshData, response)
	if err != nil {
		return
	}

	return response.AccessToken, response.RefreshToken, nil
}

func (c *Client) RefreshToken() (token, refreshToken string, err error) {
	requestRefreshData := RefreshTokenRequest{
		ClientId:     c.clientId,
		ClientSecret: c.clientSecret,
		GrantType:    "refresh_token",
		RefreshToken: c.refreshToken,
		RedirectUri:  c.redirectUri,
	}
	response := &RefreshTokenResponse{}
	_, err = c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, refreshPath), http.MethodPost, "application/json", requestRefreshData, response)
	if err != nil {
		return
	}

	return response.AccessToken, response.RefreshToken, nil
}
