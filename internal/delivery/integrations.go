package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	jsonSite "backend/internal/delivery/site"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// ReadIntegrationsOfSite
// @Summary Получить список интеграций сайта.
// @Description Получить список интеграций сайта.
// @Tags integration
// @Produce json
// @Param id path string true "AmocrmIntegration site ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.ReadIntegrationsOfSiteResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/list/{id} [get]
func (d *Delivery) ReadIntegrationsOfSite(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	amocrm, bitrix, mail, leadactiv, err := d.services.Integration.ReadIntegrationsOfSite(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	var amocrmResponse *jsonIntegration.AmocrmIntegrationResponse
	if amocrm != nil {
		amocrmResponse = jsonIntegration.ToAmocrmIntegrationResponse(amocrm)
	}

	var bitrixResponse *jsonIntegration.BitrixIntegrationResponse
	if bitrix != nil {
		bitrixResponse = jsonIntegration.ToBitrixIntegrationResponse(bitrix)
	}

	var mailResponse *jsonIntegration.MailIntegrationResponse
	if mail != nil {
		mailResponse = jsonIntegration.ToMailIntegrationResponse(mail)
	}

	var leadactivResponse *jsonIntegration.LeadactivAmocrmIntegrationResponse
	if leadactiv != nil {
		leadactivResponse = jsonIntegration.ToLeadactivAmocrmIntegartionResponse(leadactiv)
	}

	c.JSON(http.StatusOK, jsonIntegration.ReadIntegrationsOfSiteResponse{Amocrm: amocrmResponse, Bitrix: bitrixResponse, Mail: mailResponse, Leadactiv: leadactivResponse})
}

// ReadSitesOfProjectWithIntegrations
// @Summary Получить список доменов c интеграциями.
// @Description Получить список доменов c интеграциями.
// @Tags integration
// @Produce json
// @Param id path string true "Photo ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.ReadSitesWithIntegrationsResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/sites/true/{id} [get]
func (d *Delivery) ReadSitesOfProjectWithIntegrations(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	sites, err := d.services.Integration.ReadSitesOfProjectWithIntegrations(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	sitesList := []*jsonSite.SiteResponse{}
	for _, site := range sites {
		sitesList = append(sitesList, jsonSite.ToSiteResponse(site))
	}

	c.JSON(http.StatusOK, jsonIntegration.ReadSitesWithIntegrationsResponse{List: sitesList})
}

// ReadSitesOfProjectWithoutIntegrations
// @Summary Получить список доменов без интеграций.
// @Description Получить список доменов без интеграций.
// @Tags integration
// @Produce json
// @Param id path string true "Photo ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.ReadSitesWithoutIntegrationsResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/sites/false/{id} [get]
func (d *Delivery) ReadSitesOfProjectWithoutIntegrations(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	sites, err := d.services.Integration.ReadSitesOfProjectWithoutIntegrations(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	sitesList := []*jsonSite.SiteResponse{}
	for _, site := range sites {
		sitesList = append(sitesList, jsonSite.ToSiteResponse(site))
	}

	c.JSON(http.StatusOK, jsonIntegration.ReadSitesWithoutIntegrationsResponse{List: sitesList})
}
