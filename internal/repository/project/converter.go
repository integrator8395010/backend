package repository

import (
	"backend/internal/domain/project"
	"backend/internal/repository/project/dao"
)

func (r *Repository) toDomainProject(dao *dao.Project) (*project.Project, error) {
	return project.NewWithId(dao.Id, dao.Name, dao.Photo, dao.CreatedBy, dao.CreatedAt, dao.ModifiedAt) // pass is empty for security reasons
}
