package integration

import (
	"backend/internal/domain/integration/email"
	"time"

	"github.com/google/uuid"
)

type MailIntegration struct {
	id          uuid.UUID
	siteId      uuid.UUID
	reciptients []email.Email
	createdAt   time.Time
	modifiedAt  time.Time
}

func (m MailIntegration) Id() uuid.UUID {
	return m.id
}

func (m MailIntegration) SiteId() uuid.UUID {
	return m.siteId
}

func (m MailIntegration) Recipient() []email.Email {
	return m.reciptients
}

func (m *MailIntegration) AddRecipient(recipient email.Email) {
	m.reciptients = append(m.reciptients, recipient)
}

func (m MailIntegration) CreatedAt() time.Time {
	return m.createdAt
}

func (m MailIntegration) ModifiedAt() time.Time {
	return m.modifiedAt
}

func NewMailIntegrationWithId(id uuid.UUID, siteId uuid.UUID, createdAt time.Time, modifiedAt time.Time, mailList ...email.Email) (*MailIntegration, error) {
	return &MailIntegration{
		id:          id,
		siteId:      siteId,
		reciptients: mailList,
		createdAt:   createdAt,
		modifiedAt:  modifiedAt,
	}, nil
}

func NewMailIntegration(siteId uuid.UUID, mailList ...email.Email) (*MailIntegration, error) {
	return &MailIntegration{
		id:          uuid.New(),
		siteId:      siteId,
		reciptients: mailList,
		createdAt:   time.Now(),
		modifiedAt:  time.Now(),
	}, nil
}
