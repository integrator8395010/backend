package integration

import (
	jsonSite "backend/internal/delivery/site"
)

type ReadIntegrationsOfSiteResponse struct {
	Mail      *MailIntegrationResponse            `json:"mail"`
	Amocrm    *AmocrmIntegrationResponse          `json:"amocrm"`
	Bitrix    *BitrixIntegrationResponse          `json:"bitrix"`
	Leadactiv *LeadactivAmocrmIntegrationResponse `json:"leadactiv"`
}

type ReadSitesWithIntegrationsResponse struct {
	List []*jsonSite.SiteResponse `json:"list"`
}

type ReadSitesWithoutIntegrationsResponse struct {
	List []*jsonSite.SiteResponse `json:"list"`
}
