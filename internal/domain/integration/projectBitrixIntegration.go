package integration

import (
	"time"

	"github.com/google/uuid"
)

type ProjectBitrixIntegration struct {
	id         uuid.UUID
	projectId  uuid.UUID
	baseUrl    string
	createdAt  time.Time
	modifiedAt time.Time
}

func NewProjectBitrixIntegrationWithId(
	id uuid.UUID,
	projectId uuid.UUID,
	baseUrl string,
	createdAt time.Time,
	modifiedAt time.Time) (*ProjectBitrixIntegration, error) {
	return &ProjectBitrixIntegration{
		id:         id,
		projectId:  projectId,
		baseUrl:    baseUrl,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func NewProjectBitrixIntegration(
	projectId uuid.UUID,
	baseUrl string,
) (*ProjectBitrixIntegration, error) {
	return &ProjectBitrixIntegration{
		id:         uuid.New(),
		baseUrl:    baseUrl,
		projectId:  projectId,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func (m ProjectBitrixIntegration) Id() uuid.UUID {
	return m.id
}

func (m ProjectBitrixIntegration) ProjectId() uuid.UUID {
	return m.projectId
}

func (m ProjectBitrixIntegration) BaseUrl() string {
	return m.baseUrl
}

func (m ProjectBitrixIntegration) CreatedAt() time.Time {
	return m.createdAt
}

func (m ProjectBitrixIntegration) ModifiedAt() time.Time {
	return m.modifiedAt
}
