package delivery

import (
	jsonStep "backend/internal/delivery/step"
	"backend/internal/domain/step"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CrateStep
// @Summary Создание шага квиза.
// @Description Создание шага квиза.
// @Tags step
// @Accept json
// @Produce json
// @Param   contact 	body 		jsonStep.CreateStepRequest  true  "Данные по контакту"
// @Success 201			{object} 	jsonStep.StepResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /step/ [put]
func (d *Delivery) CreateStep(c *gin.Context) {
	request := jsonStep.CreateStepRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	step, err := step.New(kvizId, request.Title, request.Answers)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Step.CreateStep(context.Background(), step)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonStep.ToStepResponse(step))
}

// UpdateStep
// @Summary Обновление шага квиза.
// @Description Обновление шага квиза.
// @Tags step
// @Accept json
// @Produce json
// @Param data body jsonStep.UpdateStepRequest true "Body"
// @Success 200			{object} 	jsonStep.StepResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /step/ [post]
func (d *Delivery) UpdateStep(c *gin.Context) {
	request := jsonStep.UpdateStepRequest{}
	if err := c.ShouldBindJSON(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.ID)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	stepNew, err := step.NewWithId(id, kvizId, request.Title, request.Answers, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldStep *step.Step) (*step.Step, error) {
		return step.NewWithId(oldStep.Id(), stepNew.KvizId(), stepNew.Title(), stepNew.Answers(), oldStep.CreatedAt(), stepNew.ModifiedAt())
	}

	stepResp, err := d.services.Step.UpdateStep(context.Background(), stepNew.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonStep.ToStepResponse(stepResp))
}

// DeleteStep
// @Summary Удаление шага квиза.
// @Description Удаление шага квиза.
// @Tags step
// @Produce json
// @Param id path string true "Step ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /step/{id} [delete]
func (d *Delivery) DeleteStep(c *gin.Context) {
	id := c.Param("id")
	stepId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Step.DeleteStep(context.Background(), stepId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadStepList
// @Summary Список шагов.
// @Description Список шагов.
// @Tags step
// @Produce json
// @Success 200			{object} 	jsonStep.StepListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /step/ [get]
func (d *Delivery) ReadStepList(c *gin.Context) {
	stepList, err := d.services.Step.ReadStepList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	stepListResponse := make([]*jsonStep.StepResponse, len(stepList))

	for index, step := range stepList {
		stepListResponse[index] = jsonStep.ToStepResponse(step)
	}

	c.JSON(http.StatusOK, jsonStep.StepListResponse{Steps: stepListResponse})
}
