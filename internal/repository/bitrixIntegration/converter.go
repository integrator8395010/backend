package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/bitrixDefaultValue"
	"backend/internal/domain/integration/bitrixFiledAssociation"
	"backend/internal/repository/bitrixIntegration/dao"
)

func (r *Repository) toDomainBitrixIntegration(dao *dao.BitrixIntegration) (*integration.BitrixIntegration, error) {
	associationsList := []*bitrixFiledAssociation.BitrixFiledAssociation{}
	for _, item := range dao.Associations {
		for key, val := range item {
			association := bitrixFiledAssociation.NewBitrixFieldAssociation(key, val)
			associationsList = append(associationsList, association)
		}
	}

	defaultValuesList := []*bitrixDefaultValue.BitrixDefaultValue{}
	for _, item := range dao.DefaultValues {
		for key, val := range item {
			defaultValue := bitrixDefaultValue.NewBitrixDefaultValue(key, val)
			defaultValuesList = append(defaultValuesList, defaultValue)
		}
	}
	return integration.NewBitrixIntegrationWithId(dao.Id, dao.SiteId, dao.Responsible, integration.SendType(dao.SendType), dao.CreatedAt, dao.ModifiedAt, associationsList, defaultValuesList)
}

func (r *Repository) toRepositoryDaoBitrixIntegration(integration *integration.BitrixIntegration) (*dao.BitrixIntegration, error) {
	associationsList := []map[string]string{}
	for _, association := range integration.Associations() {
		associationsList = append(associationsList, map[string]string{association.LeadField(): association.BitrixField()})
	}

	defaultValuesList := []map[string]interface{}{}
	for _, item := range integration.DefaultValues() {
		defaultValuesList = append(defaultValuesList, map[string]interface{}{item.Field(): item.Value()})
	}
	return &dao.BitrixIntegration{
		Id:            integration.Id(),
		SiteId:        integration.SiteId(),
		Responsible:   integration.Responsible(),
		Associations:  associationsList,
		DefaultValues: defaultValuesList,
		SendType:      string(integration.SendType()),
		CreatedAt:     integration.CreatedAt(),
		ModifiedAt:    integration.ModifiedAt(),
	}, nil
}
