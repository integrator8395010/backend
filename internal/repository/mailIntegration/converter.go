package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/email"
	"backend/internal/repository/mailIntegration/dao"
)

func (r *Repository) toDomainIntegration(dao *dao.MailIntegration) (*integration.MailIntegration, error) {
	emailsList := make([]email.Email, len(dao.Recipients))
	for index, emailInput := range dao.Recipients {
		email, err := email.New(emailInput)
		if err != nil {
			return nil, err
		}
		emailsList[index] = email
	}
	return integration.NewMailIntegrationWithId(dao.Id, dao.SiteId, dao.CreatedAt, dao.ModifiedAt, emailsList...)
}
