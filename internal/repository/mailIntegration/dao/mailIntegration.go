package dao

import (
	"time"

	"github.com/google/uuid"
)

type MailIntegration struct {
	Id         uuid.UUID `db:"id"`
	SiteId     uuid.UUID `db:"site_id"`
	Recipients []string  `db:"recipients"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsMailIntegration = []string{
	"id",
	"site_id",
	"recipients",
	"created_at",
	"modified_at",
}
