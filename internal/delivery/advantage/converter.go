package advantage

import "backend/internal/domain/advantage"

func ToAdvantageResponse(advantage *advantage.Advantage) *AdvantageResponse {
	return &AdvantageResponse{
		ID:         advantage.Id().String(),
		KvizId:     advantage.KvizId().String(),
		Title:      advantage.Title(),
		Photo:      advantage.Photo(),
		CreatedAt:  advantage.CreatedAt(),
		ModifiedAt: advantage.ModifiedAt(),
	}
}
