package step

import (
	"backend/internal/domain/step"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateStep(ctx context.Context, step *step.Step) (err error) {
	err = s.repository.Step.CreateStep(ctx, step)
	if err != nil {
		s.logger.Error("error creating step: %s", err.Error())
		return
	}
	return
}

func (s *service) UpdateStep(ctx context.Context, id uuid.UUID, updateFn func(step *step.Step) (*step.Step, error)) (step *step.Step, err error) {
	step, err = s.repository.Step.UpdateStep(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error updating step: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteStep(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.Step.DeleteStep(ctx, id)
	if err != nil {
		s.logger.Error("error deleting step: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadStepList(ctx context.Context) (stepsList []*step.Step, err error) {
	stepsList, err = s.repository.Step.ReadStepList(ctx)
	if err != nil {
		s.logger.Error("error reading step list: %s", err.Error())
		return
	}
	return
}
