package integration

import (
	"backend/internal/domain/integration/amocrmDefaultValue"
	"backend/internal/domain/integration/amocrmFieldAssociation"

	"time"

	"github.com/google/uuid"
)

type AmocrmIntegration struct {
	id            uuid.UUID
	siteId        uuid.UUID
	pipelineId    int
	statusId      int
	responsible   int
	unsorted      bool
	associations  []*amocrmFieldAssociation.AmocrmFieldAssociation
	defaultValues []*amocrmDefaultValue.AmocrmDefaultValue
	createdAt     time.Time
	modifiedAt    time.Time
}

func NewAmocrmIntegrationWithId(
	id uuid.UUID,
	siteId uuid.UUID,
	pipelineId int,
	statusId int,
	responsible int,
	unsorted bool,
	associations []*amocrmFieldAssociation.AmocrmFieldAssociation,
	defaultValues []*amocrmDefaultValue.AmocrmDefaultValue,
	createdAt time.Time,
	modifiedAt time.Time) (*AmocrmIntegration, error) {
	return &AmocrmIntegration{
		id:            id,
		siteId:        siteId,
		pipelineId:    pipelineId,
		statusId:      statusId,
		responsible:   responsible,
		unsorted:      unsorted,
		associations:  associations,
		defaultValues: defaultValues,
		createdAt:     createdAt,
		modifiedAt:    modifiedAt,
	}, nil
}

func NewAmocrmIntegration(
	siteId uuid.UUID,
	pipelineId int,
	statusId int,
	responsible int,
	unsorted bool,
	associations []*amocrmFieldAssociation.AmocrmFieldAssociation,
	defaultValues []*amocrmDefaultValue.AmocrmDefaultValue,
) (*AmocrmIntegration, error) {
	return &AmocrmIntegration{
		id:            uuid.New(),
		siteId:        siteId,
		pipelineId:    pipelineId,
		statusId:      statusId,
		responsible:   responsible,
		unsorted:      unsorted,
		associations:  associations,
		defaultValues: defaultValues,
		createdAt:     time.Now(),
		modifiedAt:    time.Now(),
	}, nil
}

func (m AmocrmIntegration) Id() uuid.UUID {
	return m.id
}

func (m AmocrmIntegration) SiteId() uuid.UUID {
	return m.siteId
}

func (m AmocrmIntegration) PipelineId() int {
	return m.pipelineId
}

func (m AmocrmIntegration) StatusId() int {
	return m.statusId
}

func (m AmocrmIntegration) Responsible() int {
	return m.responsible
}

func (m AmocrmIntegration) IsUnsorted() bool {
	return m.unsorted
}

func (m AmocrmIntegration) Associations() []*amocrmFieldAssociation.AmocrmFieldAssociation {
	return m.associations
}

func (m AmocrmIntegration) DefaultValues() []*amocrmDefaultValue.AmocrmDefaultValue {
	return m.defaultValues
}

func (m *AmocrmIntegration) AddAssociations(associations ...*amocrmFieldAssociation.AmocrmFieldAssociation) {
	m.associations = append(m.associations, associations...)
}

func (m *AmocrmIntegration) AddDefaultValues(defaultValues ...*amocrmDefaultValue.AmocrmDefaultValue) {
	m.defaultValues = append(m.defaultValues, defaultValues...)
}

func (m AmocrmIntegration) CreatedAt() time.Time {
	return m.createdAt
}

func (m AmocrmIntegration) ModifiedAt() time.Time {
	return m.modifiedAt
}
