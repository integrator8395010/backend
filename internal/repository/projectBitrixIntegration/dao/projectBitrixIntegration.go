package dao

import (
	"time"

	"github.com/google/uuid"
)

type ProjectBitrixIntegration struct {
	Id         uuid.UUID `db:"id"`
	ProjectId  uuid.UUID `db:"project_id"`
	BaseUrl    string    `db:"base_url"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsProjectBitrixIntegration = []string{
	"id",
	"project_id",
	"base_url",
	"created_at",
	"modified_at",
}
