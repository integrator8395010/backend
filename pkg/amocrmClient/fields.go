package amocrmClient

import (
	"fmt"
	"net/http"
)

func (c *Client) GetLeadFields(page int) (jsonString string, err error) {
	jsonString, err = c.sendRequestWithoutDecode(fmt.Sprintf("https://%s%s?page=%d", c.baseDomain, getLeadFieldsList, page), http.MethodGet, "application/json")
	if err != nil {
		return
	}
	return
}

func (c *Client) GetUsersList(page int) (jsonString string, err error) {
	jsonString, err = c.sendRequestWithoutDecode(fmt.Sprintf("https://%s%s/?page=%d", c.baseDomain, getUsersList, page), http.MethodGet, "application/json")
	if err != nil {
		return
	}
	return
}

func (c *Client) GetPipelinesList() (jsonString string, err error) {
	jsonString, err = c.sendRequestWithoutDecode(fmt.Sprintf("https://%s%s", c.baseDomain, getPipelinesList), http.MethodGet, "application/json")
	if err != nil {
		return
	}
	return
}

func (c *Client) GetStatusesOfPipeline(pipelineId int) (jsonString string, err error) {
	jsonString, err = c.sendRequestWithoutDecode(fmt.Sprintf("https://%s%s/%d/statuses", c.baseDomain, getPipelinesList, pipelineId), http.MethodGet, "application/json")
	if err != nil {
		return
	}
	return
}
