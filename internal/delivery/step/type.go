package step

import (
	"time"
)

type CreateStepRequest struct {
	KvizId  string    `json:"kvizId"`
	Title   string    `json:"title"`
	Answers []*string `json:"answers"`
}

type StepListResponse struct {
	Steps []*StepResponse `json:"stepList"`
}

type StepResponse struct {
	ID         string    `json:"id"`
	KvizId     string    `json:"kvizId"`
	Title      string    `json:"title"`
	Answers    []*string `json:"answers"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type UpdateStepRequest struct {
	ID      string    `json:"id"`
	KvizId  string    `json:"kvizId"`
	Title   string    `json:"title"`
	Answers []*string `json:"answers"`
}
