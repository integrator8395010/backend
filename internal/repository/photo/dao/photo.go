package dao

import (
	"time"

	"github.com/google/uuid"
)

type Photo struct {
	Id         uuid.UUID `db:"id"`
	KvizId     uuid.UUID `db:"kviz_id"`
	Photo      string    `db:"photo"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsPhoto = []string{
	"id",
	"kviz_id",
	"photo",
	"created_at",
	"modified_at",
}
