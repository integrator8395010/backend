package lead

import (
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	"golang.org/x/net/idna"
)

type Lead struct {
	id             uuid.UUID
	url            string
	siteId         uuid.UUID
	name           string
	phone          string
	email          string
	comment        string
	roistat        int
	ip             string
	userAgent      string
	utmMedium      string
	utmContent     string
	utmTerm        string
	utmSource      string
	utmCampaign    string
	yclid          string
	gclid          string
	fbid           string
	source         Source
	spam           bool
	yandexClientId int64
	createdAt      time.Time
	modifiedAt     time.Time
}

func (c Lead) Id() uuid.UUID {
	return c.id
}

func (c Lead) Url() string {
	return c.url
}

func (c Lead) SiteId() uuid.UUID {
	return c.siteId
}

func (c Lead) Name() string {
	return c.name
}

func (c Lead) Phone() string {
	return c.phone
}

func (c Lead) Email() string {
	return c.email
}

func (c Lead) Comment() string {
	if c.comment == "" {
		return "нет комментария"
	}
	return c.comment
}

func (c Lead) Roistat() int {
	return c.roistat
}

func (c Lead) Ip() string {
	return c.ip
}

func (c Lead) UserAgent() string {
	return c.userAgent
}

func (c Lead) UtmMedium() string {
	return c.utmMedium
}

func (c Lead) UtmContent() string {
	return c.utmContent
}

func (c Lead) UtmTerm() string {
	return c.utmTerm
}

func (c Lead) UtmSource() string {
	return c.utmSource
}

func (c Lead) UtmCampaign() string {
	return c.utmCampaign
}

func (c Lead) Yclid() string {
	return c.yclid
}

func (c Lead) Gclid() string {
	return c.gclid
}

func (c Lead) Fbid() string {
	return c.fbid
}

func (c Lead) Source() Source {
	return c.source
}

func (c Lead) Spam() bool {
	return c.spam
}

func (c *Lead) SetSpam(spam bool) {
	c.spam = spam
}

func (c Lead) YandexClientId() int64 {
	return c.yandexClientId
}

func (c Lead) CreatedAt() time.Time {
	return c.createdAt
}

func (c Lead) ModifiedAt() time.Time {
	return c.modifiedAt
}

func (c Lead) FieldByName(key string) (value interface{}, err error) {
	LeadFieldsNames := map[string]interface{}{
		"id":             c.id,
		"url":            c.url,
		"name":           c.name,
		"phone":          c.phone,
		"email":          c.email,
		"comment":        c.comment,
		"roistat":        c.roistat,
		"ip":             c.ip,
		"userAgent":      c.userAgent,
		"utmMedium":      c.utmMedium,
		"utmContent":     c.utmContent,
		"utmTerm":        c.utmTerm,
		"utmSource":      c.utmSource,
		"utmCampaign":    c.utmCampaign,
		"yclid":          c.yclid,
		"gclid":          c.gclid,
		"spam":           c.spam,
		"yandexClientId": c.yandexClientId,
		"fbid":           c.fbid,
	}

	value, ok := LeadFieldsNames[key]
	if !ok {
		return nil, fmt.Errorf("no field with key: %s", key)
	}
	return
}

func NewWithId(
	id uuid.UUID,
	url string,
	siteId uuid.UUID,
	name string,
	phone string,
	email string,
	comment string,
	roistat int,
	ip string,
	userAgent string,
	utmMedium string,
	utmContent string,
	utmTerm string,
	utmSource string,
	utmCampaign string,
	yclid string,
	gclid string,
	fbid string,
	source Source,
	spam bool,
	yandexClientId int64,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Lead, error) {
	urlDecoded, err := decodeUrl(url)
	if err != nil {
		return nil, fmt.Errorf("error while parsing url - %s : %s", url, err.Error())
	}
	if phone == "" {
		return nil, fmt.Errorf("no phone provided")
	}
	return &Lead{
		id:             id,
		url:            urlDecoded,
		siteId:         siteId,
		name:           name,
		phone:          parseNumbersFromPhone(phone),
		email:          email,
		comment:        comment,
		roistat:        roistat,
		ip:             ip,
		userAgent:      userAgent,
		utmMedium:      utmMedium,
		utmContent:     utmContent,
		utmTerm:        utmTerm,
		utmSource:      utmSource,
		utmCampaign:    utmCampaign,
		yclid:          yclid,
		gclid:          gclid,
		fbid:           fbid,
		source:         source,
		spam:           spam,
		yandexClientId: yandexClientId,
		createdAt:      createdAt,
		modifiedAt:     modifiedAt,
	}, nil
}

func New(
	url string,
	siteId uuid.UUID,
	name string,
	phone string,
	email string,
	comment string,
	roistat int,
	ip string,
	userAgent string,
	utmMedium string,
	utmContent string,
	utmTerm string,
	utmSource string,
	utmCampaign string,
	yclid string,
	gclid string,
	fbid string,
	source Source,
	spam bool,
	yandexClientId int64,
) (*Lead, error) {
	urlDecoded, err := decodeUrl(url)
	if err != nil {
		return nil, fmt.Errorf("error while parsing url - %s : %s", url, err.Error())
	}
	if phone == "" {
		return nil, fmt.Errorf("no phone provided")
	}
	return &Lead{
		id:             uuid.New(),
		url:            urlDecoded,
		siteId:         siteId,
		name:           name,
		phone:          parseNumbersFromPhone(phone),
		email:          email,
		comment:        comment,
		roistat:        roistat,
		ip:             ip,
		userAgent:      userAgent,
		utmMedium:      utmMedium,
		utmContent:     utmContent,
		utmTerm:        utmTerm,
		utmSource:      utmSource,
		utmCampaign:    utmCampaign,
		yclid:          yclid,
		gclid:          gclid,
		fbid:           fbid,
		source:         source,
		spam:           spam,
		yandexClientId: yandexClientId,
		createdAt:      time.Now(),
		modifiedAt:     time.Now(),
	}, nil
}

func decodeUrl(url string) (string, error) {
	url = strings.ReplaceAll(url, "%3B", ";")
	url = strings.ReplaceAll(url, ";", "|")
	url = strings.ReplaceAll(url, "www.", "")
	url = strings.ReplaceAll(url, "/?", "?")
	url, err := parseUrl(url)
	if err != nil {
		return "", fmt.Errorf("error parsing url %s: %s", url, err.Error())
	}
	p := idna.New()
	urlUnicode, err := p.ToUnicode(url)
	if err != nil {
		return "", fmt.Errorf("error decoding url %s: %s", url, err.Error())
	}
	return urlUnicode, nil
}

func parseUrl(input string) (string, error) {
	url, err := url.Parse(input)
	if err != nil {
		return "", err
	}
	urlValue := url.Host + url.Path
	urlValue = strings.TrimSuffix(urlValue, "/")
	return urlValue, nil
}

func parseNumbersFromPhone(in string) string {
	reg, _ := regexp.Compile("[^a-zA-Z0-9]")
	return reg.ReplaceAllString(in, "")
}
