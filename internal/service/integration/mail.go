package integration

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateMailIntegration(ctx context.Context, integration *integration.MailIntegration) (err error) {
	err = s.repository.MailIntegration.CreateMailIntegration(ctx, integration)
	if err != nil {
		s.logger.Error("error create mailIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) UpdateMailIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.MailIntegration) (*integration.MailIntegration, error)) (integration *integration.MailIntegration, err error) {
	integration, err = s.repository.MailIntegration.UpdateMailIntegration(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error update mailIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteMailIntegration(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.MailIntegration.DeleteMailIntegration(ctx, id)
	if err != nil {
		s.logger.Error("error create mailIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadMailIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.MailIntegration, err error) {
	err = s.repository.MailIntegration.DeleteMailIntegration(ctx, siteId)
	if err != nil {
		if err.Error() != "no rows in result set" {
			s.logger.Error("error read mailIntegration: %s", err.Error())
		}
		return
	}
	return
}
