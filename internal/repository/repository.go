package repository

import (
	advantage "backend/internal/repository/advantage"
	amocrmIntegration "backend/internal/repository/amocrmIntegration"
	bitrixIntegration "backend/internal/repository/bitrixIntegration"
	kviz "backend/internal/repository/kviz"
	lead "backend/internal/repository/lead"
	leadactivAmoSecrets "backend/internal/repository/leadactivAmoSecrets"
	leadactivIntegration "backend/internal/repository/leadactivIntegration"
	leadsSendLog "backend/internal/repository/leadsSendLog"
	mailIntegration "backend/internal/repository/mailIntegration"
	photo "backend/internal/repository/photo"
	plan "backend/internal/repository/plan"
	project "backend/internal/repository/project"
	projectAmocrmIntegration "backend/internal/repository/projectAmocrmIntegration"
	projectBitrixIntegration "backend/internal/repository/projectBitrixIntegration"
	site "backend/internal/repository/site"
	step "backend/internal/repository/step"
	user "backend/internal/repository/user"

	"gitlab.com/kanya384/gotools/psql"
)

type Repository struct {
	Advantage                *advantage.Repository
	Kviz                     *kviz.Repository
	Photo                    *photo.Repository
	Plan                     *plan.Repository
	Lead                     *lead.Repository
	LeadsSendLog             *leadsSendLog.Repository
	Project                  *project.Repository
	Site                     *site.Repository
	Step                     *step.Repository
	User                     *user.Repository
	ProjectAmoCrmIntegartion *projectAmocrmIntegration.Repository
	AmoCrmIntegration        *amocrmIntegration.Repository
	ProjectBitrixIntegartion *projectBitrixIntegration.Repository
	BitrixIntegration        *bitrixIntegration.Repository
	MailIntegration          *mailIntegration.Repository
	LeadactivIntegration     *leadactivIntegration.Repository
	LeadactivAmoSecrets      *leadactivAmoSecrets.Repository
}

func NewRepository(pg *psql.Postgres) (*Repository, error) {
	advantage, err := advantage.New(pg, advantage.Options{})
	if err != nil {
		return nil, err
	}

	kviz, err := kviz.New(pg, kviz.Options{})
	if err != nil {
		return nil, err
	}

	lead, err := lead.New(pg, lead.Options{})
	if err != nil {
		return nil, err
	}

	leadsSendLog, err := leadsSendLog.New(pg, leadsSendLog.Options{})
	if err != nil {
		return nil, err
	}

	plan, err := plan.New(pg, plan.Options{})
	if err != nil {
		return nil, err
	}

	project, err := project.New(pg, project.Options{})
	if err != nil {
		return nil, err
	}

	site, err := site.New(pg, site.Options{})
	if err != nil {
		return nil, err
	}

	step, err := step.New(pg, step.Options{})
	if err != nil {
		return nil, err
	}

	user, err := user.New(pg, user.Options{})
	if err != nil {
		return nil, err
	}

	photo, err := photo.New(pg, photo.Options{})
	if err != nil {
		return nil, err
	}

	projectAmoCrm, err := projectAmocrmIntegration.New(pg, projectAmocrmIntegration.Options{})
	if err != nil {
		return nil, err
	}

	projectBitrixCrm, err := projectBitrixIntegration.New(pg, projectBitrixIntegration.Options{})
	if err != nil {
		return nil, err
	}

	amocrm, err := amocrmIntegration.New(pg, amocrmIntegration.Options{})
	if err != nil {
		return nil, err
	}

	bitrix, err := bitrixIntegration.New(pg, bitrixIntegration.Options{})
	if err != nil {
		return nil, err
	}

	mail, err := mailIntegration.New(pg, mailIntegration.Options{})
	if err != nil {
		return nil, err
	}

	leadactivIntegration, err := leadactivIntegration.New(pg, leadactivIntegration.Options{})
	if err != nil {
		return nil, err
	}

	leadactivAmoSecrets, err := leadactivAmoSecrets.New(pg, leadactivAmoSecrets.Options{})
	if err != nil {
		return nil, err
	}

	return &Repository{
		advantage,
		kviz,
		photo,
		plan,
		lead,
		leadsSendLog,
		project,
		site,
		step,
		user,
		projectAmoCrm,
		amocrm,
		projectBitrixCrm,
		bitrix,
		mail,
		leadactivIntegration,
		leadactivAmoSecrets,
	}, nil

}
