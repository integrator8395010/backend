package amocrmClient

import (
	"backend/pkg/amocrmClient/contact/customField"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

const (
	sendNoteForLead = "/api/v4/leads/notes"
	sendTask        = "/api/v4/tasks"
	filterContacts  = "/api/v3/contacts?query="
	filterLeads     = "/api/v4/leads"
)

func (c *Client) SendLead(params SendLeadComplexRequest) (responseId int, err error) {
	phone := ""
	switch params.Leads[0].Embedded.Contacts[0].CustomFieldValues[0].(type) {
	case customField.CustomFieldValue:
		phone = params.Leads[0].Embedded.Contacts[0].CustomFieldValues[0].(customField.CustomFieldValue).Values[0].Value
	default:
		phone = params.Leads[0].Embedded.Contacts[0].CustomFieldValues[0].(customField.CustomField).Values[0].Value
	}

	if params.DontCheckRepeat {
		return c.doSendLeadComplex(params)
	}

	responseId, duplicate, err := c.checkContact(phone, params.Leads[0].ResponsibleUserId, params.Leads[0].Text)
	if err != nil {
		return 0, fmt.Errorf("error check on duplicate: %s", err.Error())
	}

	if duplicate {
		return responseId, nil
	}

	return c.doSendLeadComplex(params)
}

func (c *Client) doSendLeadComplex(params SendLeadComplexRequest) (responseId int, err error) {

	response := []*AmoLeadComplexResponse{}

	code, err := c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, sendLeadComplex), http.MethodPost, "application/json", params.Leads, &response)
	if err != nil {
		message := ""
		for index, field := range params.Leads[0].CustomFileds {
			message += fmt.Sprintf("%d) %d - %v | ", index+1, field.FieldId, field.Values)
		}
		return 0, fmt.Errorf("error sending lead complex %s: %s", message, err.Error())
	}
	if code == 401 {
		err = errors.New("unauthorized, check token")
		c.RefreshToken()
		return
	}
	if code >= 400 {
		err = errors.New("unhandled error")
		return
	}

	responseId = response[0].LeadId
	err = c.AddNoteToLead(responseId, params.Leads[0].ResponsibleUserId, params.Leads[0].Text)
	if err != nil {
		return 0, fmt.Errorf("error adding note to lead: %s", err.Error())
	}
	return responseId, err
}

func (c *Client) searchContactByPhone(phone string) (contacts []int, err error) {
	response := FilterResponse{}
	url := fmt.Sprintf("https://%s%s%s", c.baseDomain, filterContacts, phone)

	code, err := c.sendRequest(url, http.MethodGet, "application/json", nil, &response)
	if err != nil {
		return
	}

	if len(response.Embedded.Contacts) > 0 {
		contacts = make([]int, len(response.Embedded.Contacts))
		for index, contact := range response.Embedded.Contacts {
			contacts[index] = contact.Id
		}
	}

	if code != 200 && code != 201 {
		return contacts, fmt.Errorf("error sending task for lead")
	}

	return
}

func (c *Client) getLeadsLinkedToContact(contactId int) (leadsIds []int, err error) {
	url := fmt.Sprintf("/api/v4/contacts/%d/links", contactId)
	filter := map[string]interface{}{
		"filter": map[string]interface{}{
			"to_entity_type": "leads",
		},
	}

	response := FilterResponse{}
	_, err = c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, url), http.MethodGet, "application/json", filter, &response)
	if err != nil {
		return
	}

	leadsIds = []int{}
	for _, link := range response.Embedded.Links {
		if link.Type == "leads" {
			leadsIds = append(leadsIds, link.EntityId)
		}
	}

	return
}

func (c *Client) AddNoteToLead(leadId int, responsibleId int, text string) (err error) {
	request := []map[string]interface{}{{
		"entity_id":  leadId,
		"created_by": responsibleId,
		"note_type":  "common",
		"params": map[string]string{
			"text": text,
		}},
	}
	_, err = c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, sendNoteForLead), http.MethodPost, "application/json", request, nil)
	if err != nil {
		return
	}
	return
}

func (c *Client) AddTask(entityId int, entityType string, responsibleId int, text string, completeTill int) (err error) {
	request := []map[string]interface{}{{
		"entity_id":           entityId,
		"responsible_user_id": responsibleId,
		"entity_type":         entityType,
		"text":                text,
		"complete_till":       completeTill,
	}}

	code, err := c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, sendTask), http.MethodPost, "application/json", request, nil)
	if err != nil {
		return
	}
	if code != 200 && code != 201 {
		return fmt.Errorf("error sending task for lead")
	}
	return
}

func (c *Client) sendRequest(url, method, contentType string, data interface{}, response interface{}) (code int, err error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return 0, err
	}

	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("no body in response")
	}

	if resp.StatusCode > 201 {
		return resp.StatusCode, fmt.Errorf("bad response code from server %s - code %d, body of response: %s", url, code, string(body))
	}

	if response == nil {
		return resp.StatusCode, err
	}
	err = json.Unmarshal(body, response)
	if err != nil {
		return 0, fmt.Errorf("error decode response from %s - error %s, body of response: %s", url, err.Error(), string(body))
	}

	return resp.StatusCode, err
}

func (c *Client) sendRequestWithoutDecode(url, method, contentType string) (response string, err error) {

	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return
	}

	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	responseBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	response = string(responseBytes)

	return
}
