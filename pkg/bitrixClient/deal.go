package bitrixClient

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/lead"
	"fmt"
	"strconv"
)

const (
	phoneContactSearch = "crm.contact.list.json"
	dealSearch         = "crm.deal.list.json"
	sendDeal           = "crm.deal.add"
	sendContact        = "crm.contact.add"
)

func (c *Client) CreateContact(lead *lead.Lead) (contactId int, err error) {
	contact := map[string]interface{}{
		"fields": map[string]interface{}{
			"PHONE": []map[string]string{{
				"VALUE":      lead.Phone(),
				"VALUE_TYPE": "WORK",
			}},
		},
		"params": map[string]string{"REGISTER_SONET_EVENT": "Y"},
	}

	if lead.Name() != "" {
		contact["fields"].(map[string]interface{})["NAME"] = lead.Name()
	}

	if lead.Email() != "" {
		contact["fields"].(map[string]interface{})["Email"] = map[string]string{
			"VALUE":      lead.Email(),
			"VALUE_TYPE": "WORK",
		}
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(sendContact, contact, &response)
	if err != nil {
		return
	}
	if result, ok := response["result"]; ok {
		return int(result.(float64)), nil
	}
	return
}

func (c *Client) SendDeal(lead *lead.Lead, integration *integration.BitrixIntegration) (responseId int, err error) {
	contactId, err := c.searchContactByPhone(lead.Phone())
	if err == nil {
		urlField := getSiteUrlFileldOfIntegration(integration.Associations())
		dealId, responsibleId, err := c.searchDealByContactId(contactId, lead.Url(), urlField)
		if err == nil {
			//if deal exists send comment and notification
			err = c.sendComment(dealId, "deal", fmt.Sprintf("Повторное обращение: %s", lead.Comment()))
			if err != nil {
				return dealId, fmt.Errorf("error sending comment for deal: %s", err.Error())
			}
			crmUrl, err := c.getCrmHostFromUrl()
			if err != nil {
				return 0, fmt.Errorf("error getting host from url: %s", err.Error())
			}
			err = c.sendNotification(responsibleId, fmt.Sprintf("Повторное обращение: %s/crm/deal/details/%d/", crmUrl, dealId))
			if err != nil {
				return dealId, fmt.Errorf("error sending comment for deal: %s", err.Error())
			}

			return dealId, nil
		}
	}
	contactId, err = c.CreateContact(lead)
	if err != nil {
		return 0, fmt.Errorf("error creating contact: %s", err.Error())
	}
	responseId, err = c.doSendDeal(lead, contactId, integration)
	return
}

func (c *Client) doSendDeal(lead *lead.Lead, contactId int, integration *integration.BitrixIntegration) (dealId int, err error) {
	dealParams := map[string]interface{}{
		"fields": map[string]interface{}{
			"TITLE":          "Заявка с сайта",
			"CONTACT_ID":     contactId,
			"ASSIGNED_BY_ID": integration.Responsible(),
			"COMMENTS":       lead.Comment(),
			"WEB":            []map[string]string{{"VALUE": lead.Url(), "VALUE_TYPE": "WORK"}},
		},
		"params": map[string]string{"REGISTER_SONET_EVENT": "Y"},
	}

	if lead.Name() != "" {
		dealParams["fields"].(map[string]interface{})["NAME"] = lead.Name()
	}

	for _, value := range integration.DefaultValues() {
		dealParams["fields"].(map[string]interface{})[value.Field()] = value.Value()
	}

	for _, value := range integration.Associations() {
		fieldValue, err := lead.FieldByName(value.LeadField())
		if err != nil {
			return 0, fmt.Errorf("not founded lead field by name %s: %s", value.LeadField(), err.Error())
		}
		dealParams["fields"].(map[string]interface{})[value.BitrixField()] = fieldValue
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(sendDeal, dealParams, &response)
	if err != nil {
		return
	}

	if result, ok := response["result"]; ok {
		return int(result.(float64)), nil
	}
	return
}

func (c *Client) searchContactByPhone(phone string) (contactId int, err error) {
	fitler := map[string]string{"PHONE": phone}

	request := map[string]interface{}{
		"order":  map[string]string{"DATE_CREATE": "DESC"},
		"filter": fitler,
		"select": []string{"ID"},
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(phoneContactSearch, request, &response)
	if err != nil {
		return
	}
	if result, ok := response["result"]; ok {
		list := result.([]interface{})
		if len(list) > 0 {
			stringId := list[0].(map[string]interface{})["ID"].(string)
			id, err := strconv.Atoi(stringId)
			if err != nil {
				return 0, err
			}
			return id, nil
		}
		return 0, fmt.Errorf("no contacts")
	}
	return
}

func (c *Client) searchDealByContactId(contactId int, url, urlField string) (dealId, responsibleId int, err error) {
	fitler := map[string]interface{}{"CONTACT_ID": contactId}
	if urlField != "" {
		fitler[urlField] = url
	}

	request := map[string]interface{}{
		"order":  map[string]string{"DATE_CREATE": "DESC"},
		"filter": fitler,
		"select": []string{"ID", "ASSIGNED_BY_ID"},
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(dealSearch, request, &response)
	if err != nil {
		return
	}
	if result, ok := response["result"]; ok {
		list := result.([]interface{})
		if len(list) > 0 {

			stringId := list[0].(map[string]interface{})["ID"].(string)
			id, err := strconv.Atoi(stringId)
			if err != nil {
				return 0, 0, err
			}

			stringResponsibleId := list[0].(map[string]interface{})["ASSIGNED_BY_ID"].(string)
			responsibleId, err := strconv.Atoi(stringResponsibleId)
			if err != nil {
				return 0, 0, err
			}

			return id, responsibleId, nil
		}
		return 0, 0, fmt.Errorf("no deal")
	}
	return
}
