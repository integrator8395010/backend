package repository

import (
	"context"
	"os/user"

	"github.com/google/uuid"
)

type Repository interface {
	CreateUser(ctx context.Context, user *user.User) (err error)
	UpdateUser(ctx context.Context, updateFn func(user *user.User) (*user.User, error)) (err error)
	DeleteUser(ctx context.Context, id uuid.UUID) (err error)
	FindUserByCredetinals(ctx context.Context, login, pass string) (user *user.User, err error)
	ReadUserList(ctx context.Context) (statuses []*user.User, err error)
}
