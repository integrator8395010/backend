package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"backend/internal/domain/integration"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateLeadactivAmocrmIntegration
// @Summary Создать параметры интеграции c amo лидактива для сайта.
// @Description  Создать параметры интеграции c amo лидактива для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.CreateLeadactivAmocrmIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.LeadactivAmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/leadactiv [put]
func (d *Delivery) CreateLeadactivAmocrmIntegration(c *gin.Context) {
	request := jsonIntegration.CreateLeadactivAmocrmIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	integration, err := integration.NewLeadactivIntegration(siteId, request.Responsible)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.CreateLeadactivIntegration(context.Background(), integration)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToLeadactivAmocrmIntegartionResponse(integration))
}

// UpdateLeadactivAmocrmIntegration
// @Summary Обновить параметры интеграции c amo лидактива для сайта.
// @Description Обновить параметры интеграции c amo лидактива для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.UpdateLeadactivAmocrmIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.LeadactivAmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/leadactiv [post]
func (d *Delivery) UpdateLeadactivAmocrmIntegration(c *gin.Context) {
	request := jsonIntegration.UpdateLeadactivAmocrmIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	updateFn := func(oldIntegration *integration.LeadactivIntegration) (*integration.LeadactivIntegration, error) {
		return integration.NewLeadactivIntegrationWithId(id, siteId, request.Responsible, oldIntegration.CreatedAt(), time.Now())
	}

	leadactivIntegrationNew, err := d.services.Integration.UpdateLeadactivIntegration(context.Background(), id, updateFn)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToLeadactivAmocrmIntegartionResponse(leadactivIntegrationNew))
}

// DeleteLeadactivAmocrmIntegration
// @Summary Удалить параметры интеграции сайта.
// @Description Удалить параметры интеграции сайта.
// @Tags integration
// @Produce json
// @Param id path string true "LeadactivAmocrmIntegration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/leadactiv/{id} [delete]
func (d *Delivery) DeleteLeadactivAmocrmIntegration(c *gin.Context) {
	id := c.Param("id")
	integrationId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.DeleteLeadactivIntegration(context.Background(), integrationId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadLeadactivAmocrmIntegrationOfSite
// @Summary Получить параметры интеграции c amo лидактива для сайта.
// @Description Получить параметры интеграции c amo лидактива для сайта.
// @Tags integration
// @Produce json
// @Param id path string true "site ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.LeadactivAmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/leadactiv/{id} [get]
func (d *Delivery) ReadLeadactivAmocrmIntegrationOfSite(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	leadactivIntegration, err := d.services.Integration.ReadLeadactivIntegrationOfSite(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToLeadactivAmocrmIntegartionResponse(leadactivIntegration))
}

// ReadLeadactivUsersList
// @Summary Получить список пользователей в амо лидактива.
// @Description Получить список пользователей в амо лидактива.
// @Tags integration
// @Produce json
// @Param page path string true "integration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/leadactiv/users/{page} [get]
func (d *Delivery) ReadLeadactivUsersList(c *gin.Context) {
	page, err := strconv.Atoi(c.Param("page"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	fields, err := d.services.Integration.ReadLeadactivUsersList(context.Background(), page)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, fields)
}
