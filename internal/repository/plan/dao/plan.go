package dao

import (
	"time"

	"github.com/google/uuid"
)

type Plan struct {
	Id           uuid.UUID `db:"id"`
	KvizId       uuid.UUID `db:"kviz_id"`
	Title        string    `db:"title"`
	Photo        string    `db:"photo"`
	Rooms        int       `db:"rooms"`
	TotalArea    float32   `db:"total_area"`
	LivingArea   float32   `db:"living_area"`
	KitchenArea  float32   `db:"kitchen_area"`
	BedRoomArea  float32   `db:"bed_room_area"`
	BathRoomArea float32   `db:"bath_room_area"`
	Price        int       `db:"price"`
	CreatedAt    time.Time `db:"created_at"`
	ModifiedAt   time.Time `db:"modified_at"`
}

var ColumnsPlan = []string{
	"id",
	"kviz_id",
	"title",
	"photo",
	"rooms",
	"total_area",
	"living_area",
	"kitchen_area",
	"bed_room_area",
	"bath_room_area",
	"price",
	"created_at",
	"modified_at",
}
