package repository

import (
	"backend/internal/domain/lead"
	"backend/internal/repository/lead/dao"
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "lead"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"lead_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateLead(ctx context.Context, lead *lead.Lead) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsLead...).Values(lead.Id(), lead.Url(), lead.SiteId(), lead.Name(), lead.Phone(), lead.Email(), lead.Comment(), lead.Roistat(), lead.Ip(), lead.UserAgent(), lead.UtmMedium(), lead.UtmContent(), lead.UtmTerm(), lead.UtmSource(), lead.UtmCampaign(), lead.Yclid(), lead.Gclid(), lead.Fbid(), lead.Source(), lead.Spam(), lead.YandexClientId(), lead.CreatedAt(), lead.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateLead(ctx context.Context, id uuid.UUID, updateFn func(lead *lead.Lead) (*lead.Lead, error)) (lead *lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLead, err := r.oneLeadTx(ctx, tx, id)
	if err != nil {
		return
	}

	leadNew, err := updateFn(upLead)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("yandex_client_id", leadNew.YandexClientId()).Set("spam", leadNew.Spam()).Set("url", leadNew.Url()).Set("comment", leadNew.Comment()).Set("site_id", leadNew.SiteId()).Set("name", leadNew.Name()).Set("phone", leadNew.Phone()).Set("email", leadNew.Email()).Set("roistat", leadNew.Roistat()).Set("ip", leadNew.Ip()).Set("user_agent", leadNew.UserAgent()).Set("utm_medium", leadNew.UtmMedium()).Set("utm_term", leadNew.UtmTerm()).Set("utm_content", leadNew.UtmContent()).Set("utm_source", leadNew.UtmSource()).Set("utm_campaign", leadNew.UtmCampaign()).Set("yclid", leadNew.Yclid()).Set("gclid", leadNew.Gclid()).Set("fbid", leadNew.Fbid()).Set("source", leadNew.Source()).Set("modified_at", leadNew.ModifiedAt()).Where("id = ?", leadNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteLead(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadLeadList(ctx context.Context) (leadsList []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLead...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	leadsList = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		leadsList = append(leadsList, leadIns)
	}
	return
}

func (r *Repository) ReadFiltredLeadList(ctx context.Context, filter *lead.LeadsFilter, offset, limit int) (list []*lead.Lead, count int, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From(tableName).LeftJoin("site as site on site.url = lead.url").Where("lead.spam = false").Offset(uint64(offset)).Limit(uint64(limit))
	rawQuery = addFiltersToQuery(rawQuery, filter)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, 0, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, 0, err
	}

	list = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, 0, err
		}

		list = append(list, leadIns)
	}

	rawQuery = r.Builder.Select("lead.id").From(tableName).LeftJoin("site as site on site.url = lead.url").Offset(uint64(offset)).Limit(uint64(limit))
	rawQuery = addFiltersToQuery(rawQuery, filter)
	query, args, _ = rawQuery.ToSql()

	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, 0, err
	}
	for rows.Next() {
		count += 1
	}
	return
}

func (r *Repository) GetLeadsOfProject(ctx context.Context, projectId uuid.UUID, url string, from time.Time, to time.Time) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From(tableName).LeftJoin("site as site on site.url = lead.url").Where("site.project_id = ? and lead.created_at > ? and lead.created_at < ?", projectId, from, to)
	if url != "" {
		rawQuery = rawQuery.Where("lead.url = ?", url)
	}
	query, args, _ := rawQuery.ToSql()

	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(rows, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		leads = append(leads, leadIns)
	}

	return
}

func (r *Repository) ReadSpamFiltredLeadList(ctx context.Context, filter *lead.SpamLeadsFilter) (list []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From(tableName).LeftJoin("site as site on site.url = lead.url").Where("lead.spam = true")
	rawQuery = addSpamFiltersToQuery(rawQuery, filter)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	list = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		list = append(list, leadIns)
	}

	return
}

func (r *Repository) GetLeadsByIpCount(ctx context.Context, phone string, ip string, from time.Time) (count int, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)
	rawQuery := r.Builder.Select("id").From(tableName).Where("phone != ? and ip = ? and created_at > ?", phone, ip, from)
	query, args, _ := rawQuery.ToSql()
	rows, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return 0, err
	}
	defer rows.Close()
	count = 0
	for rows.Next() {
		count += 1
	}

	return
}

func (r *Repository) GetLeadsByYandexClientIdCount(ctx context.Context, yandexClientId int64, from time.Time) (count int, err error) {
	if yandexClientId == 0 {
		return 0, nil
	}
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select("id").From(tableName).Where("yandex_client_id = ? and created_at > ?", yandexClientId, from)

	query, args, _ := rawQuery.ToSql()
	rows, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return 0, err
	}
	defer rows.Close()
	count = 0
	for rows.Next() {
		count += 1
	}

	return
}

func (r *Repository) GetUnsendedLeadsToMailIntegration(ctx context.Context, integrationId uuid.UUID) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From("mail_integration as mail").LeftJoin("site as site on site.id = mail.site_id").LeftJoin("lead as lead on lead.url = site.url").LeftJoin("leads_send_log as sendLog on ((sendLog.integration_id = mail.id) and (sendLog.lead_id = lead.id))").Where("mail.id = ? and lead.id is not null and lead.spam = false and (sendLog.id is null or sendLog.sended = false)", integrationId)

	query, args, _ := rawQuery.ToSql()
	rows, err := tx.Query(ctx, query, args...)

	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(rows, pgx.RowToStructByPos[dao.Lead])

	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		leads = append(leads, leadIns)
	}

	return
}

func (r *Repository) GetUnsendedLeadsToBitrixIntegration(ctx context.Context, integrationId uuid.UUID) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From("bitrix_integration as bitrix").LeftJoin("site as site on site.id = bitrix.site_id").LeftJoin("lead as lead on lead.url = site.url").LeftJoin("leads_send_log as sendLog on ((sendLog.integration_id = bitrix.id) and (sendLog.lead_id = lead.id))").Where("bitrix.id = ? and lead.id is not null and lead.spam = false and (sendLog.id is null or sendLog.sended = false)", integrationId) //and (sendLog.id is null or sendLog.sended = false)

	query, args, _ := rawQuery.ToSql()
	rows, err := tx.Query(ctx, query, args...)

	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(rows, pgx.RowToStructByPos[dao.Lead])

	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		leads = append(leads, leadIns)
	}

	return
}

func (r *Repository) GetUnsendedLeadsToAmocrmIntegration(ctx context.Context, integrationId uuid.UUID) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From("amocrm_integration as amocrm").LeftJoin("site as site on site.id = amocrm.site_id").LeftJoin("lead as lead on lead.url = site.url").LeftJoin("leads_send_log as sendLog on ((sendLog.integration_id = amocrm.id) and (sendLog.lead_id = lead.id))").Where("amocrm.id = ? and lead.id is not null and lead.spam = false and (sendLog.id is null or sendLog.sended = false)", integrationId)

	query, args, _ := rawQuery.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(rows, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		leads = append(leads, leadIns)
	}

	return
}

func (r *Repository) GetUnsendedLeadsToLeadactivIntegration(ctx context.Context, integrationId uuid.UUID) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "lead"
	for _, column := range dao.ColumnsLead {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From("leadactiv_integration as leadactiv").LeftJoin("site as site on site.id = leadactiv.site_id").LeftJoin("lead as lead on lead.url = site.url").LeftJoin("leads_send_log as sendLog on ((sendLog.integration_id = leadactiv.id) and (sendLog.lead_id = lead.id))").Where("leadactiv.id = ? and lead.id is not null and lead.spam = false and (sendLog.id is null or sendLog.sended = false)", integrationId)

	query, args, _ := rawQuery.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(rows, pgx.RowToStructByPos[dao.Lead])

	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, lead := range daoLead {
		leadIns, err := r.toDomainLead(&lead)
		if err != nil {
			return nil, err
		}

		leads = append(leads, leadIns)
	}

	return
}

func (r *Repository) oneLeadTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *lead.Lead, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsLead...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	return r.toDomainLead(&daoLead)
}

func addFiltersToQuery(query squirrel.SelectBuilder, filter *lead.LeadsFilter) squirrel.SelectBuilder {

	if filter.ProjectId() != uuid.Nil {
		query = query.Where("site.project_id = ?", filter.ProjectId())
	}

	if !filter.From().IsZero() {
		query = query.Where("lead.created_at > ?", filter.From())
	}

	if !filter.To().IsZero() {
		query = query.Where("lead.created_at < ?", filter.To())
	}

	if filter.Phone() != "" {
		query = query.Where("lead.phone like ?", "%"+filter.Phone()+"%")
	}

	if filter.Source() != "" {
		query = query.Where("lead.source = ?", filter.Source())
	}

	if filter.Url() != "" {
		query = query.Where("lead.url like ?", "%"+filter.Url()+"%")
	}

	return query
}

func addSpamFiltersToQuery(query squirrel.SelectBuilder, filter *lead.SpamLeadsFilter) squirrel.SelectBuilder {

	if filter.ProjectId() != uuid.Nil {
		query = query.Where("site.project_id = ?", filter.ProjectId())
	}

	if !filter.From().IsZero() {
		query = query.Where("lead.created_at > ?", filter.From())
	}

	if !filter.To().IsZero() {
		query = query.Where("lead.created_at < ?", filter.To())
	}

	if filter.Url() != "" {
		query = query.Where("lead.url like ?", "%"+filter.Url()+"%")
	}

	return query
}
