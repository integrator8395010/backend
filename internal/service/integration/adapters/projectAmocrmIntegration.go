package repository

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

type ProjectAmocrmIntegrationRepository interface {
	CreateProjectAmocrmIntegration(ctx context.Context, integration *integration.ProjectAmoIntegration) (err error)
	UpdateProjectAmocrmIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.ProjectAmoIntegration) (*integration.ProjectAmoIntegration, error)) (integration *integration.ProjectAmoIntegration, err error)
	DeleteProjectAmocrmIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadProjectAmocrmIntegration(ctx context.Context, projectId uuid.UUID) (response *integration.ProjectAmoIntegration, err error)
}
