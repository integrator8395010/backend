package repository

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

type ProjectBitrixIntegrationRepository interface {
	CreateProjectBitrixIntegration(ctx context.Context, integration *integration.ProjectBitrixIntegration) (err error)
	UpdateProjectBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.ProjectBitrixIntegration) (*integration.ProjectBitrixIntegration, error)) (integration *integration.ProjectBitrixIntegration, err error)
	DeleteProjectBitrixIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadProjectBitrixIntegration(ctx context.Context, projectId uuid.UUID) (response *integration.ProjectBitrixIntegration, err error)
	ReadProjectBitrixIntegrationBySiteId(ctx context.Context, siteId uuid.UUID) (projectBitrixIntegrationsList *integration.ProjectBitrixIntegration, err error)
}
