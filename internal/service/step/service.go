package step

import (
	"backend/internal/repository"
	"time"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository *repository.Repository
	logger     logger.Interface
	options    Options
}

type Options struct {
	TokenTTL time.Duration
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repository.Repository, logger logger.Interface, options Options) (*service, error) {
	service := &service{
		repository: repository,
		logger:     logger,
	}

	service.SetOptions(options)
	return service, nil
}
