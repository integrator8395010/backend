CREATE TABLE "user" (
  "id" uuid PRIMARY KEY,
  "name" varchar NOT NULL,
  "login" varchar UNIQUE NOT NULL,
  "pass" varchar NOT NULL,
  "role" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "project" (
  "id" uuid PRIMARY KEY,
  "name" varchar NOT NULL,
  "photo" varchar NOT NULL,
  "created_by" uuid NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "site" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "url" varchar,
  "project_id" uuid,
  "created_by" uuid NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "kviz" (
  "id" uuid PRIMARY KEY,
  "site_id" uuid NOT NULL,
  "template_id" uuid NOT NULL,
  "name" varchar NOT NULL,
  "background" varchar NOT NULL,
  "main_color" varchar NOT NULL,
  "secondary_color" varchar NOT NULL,
  "sub_title" varchar NOT NULL,
  "sub_title_items" varchar NOT NULL,
  "phone_step_title" varchar NOT NULL,
  "footer_title" varchar NOT NULL,
  "phone" varchar,
  "politics" bool NOT NULL,
  "roistat" varchar,
  "advantages_title" varchar NOT NULL,
  "photos_title" varchar NOT NULL,
  "plans_title" varchar,
  "result_step_text" varchar,
  "yandex" varchar,
  "google" varchar,
  "mail" varchar,
  "vk" varchar,
  "qoopler" bool NOT NULL,
  "dmp_one" varchar,
  "validate_phone" bool NOT NULL,
  "created_by" uuid NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "step" (
  "id" uuid PRIMARY KEY,
  "kviz_id" uuid NOT NULL,
  "title" varchar NOT NULL,
  "answers" varchar[] NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "advantage" (
  "id" uuid PRIMARY KEY,
  "kviz_id" uuid NOT NULL,
  "title" varchar NOT NULL,
  "photo" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "plan" (
  "id" uuid PRIMARY KEY,
  "kviz_id" uuid NOT NULL,
  "title" varchar NOT NULL,
  "photo" varchar NOT NULL,
  "rooms" integer,
  "total_area" float,
  "living_area" float,
  "kitchen_area" float,
  "bed_room_area" float,
  "bath_room_area" float,
  "price" integer,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "lead" (
  "id" uuid PRIMARY KEY,
  "url" varchar NOT NULL,
  "site_id" uuid,
  "name" varchar,
  "phone" varchar NOT NULL,
  "email" varchar,
  "comment" varchar,
  "roistat" integer,
  "ip" varchar,
  "user_agent" varchar,
  "utm_medium" varchar,
  "utm_content" varchar,
  "utm_term" varchar,
  "utm_source" varchar,
  "utm_campaign" varchar,
  "yclid" varchar,
  "gclid" varchar,
  "fbid" varchar,
  "source" varchar,
  "spam" boolean NOT NULL,
  "yandex_client_id" bigint,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "leadactiv_amo_secrets" (
  "id" uuid PRIMARY KEY,
  "base_url" varchar NOT NULL,
  "client_id" varchar NOT NULL,
  "client_secret" varchar NOT NULL,
  "redirect_uri" varchar NOT NULL,
  "token" varchar NOT NULL,
  "refresh_token" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "leadactiv_integration" (
  "id" uuid PRIMARY KEY,
  "site_id" uuid NOT NULL UNIQUE,
  "responsible" integer NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "mail_integration" (
  "id" uuid PRIMARY KEY,
  "site_id" uuid NOT NULL UNIQUE,
  "recipients" json NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "project_bitrix_integration" (
  "id" uuid PRIMARY KEY,
  "project_id" uuid NOT NULL UNIQUE,
  "base_url" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "bitrix_integration" (
  "id" uuid PRIMARY KEY,
  "site_id" uuid NOT NULL UNIQUE,
  "responsible" integer,
  "send_type" varchar,
  "associations" json,
  "default_values" json,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "project_amocrm_integration" (
  "id" uuid PRIMARY KEY,
  "project_id" uuid NOT NULL UNIQUE,
  "base_url" varchar NOT NULL,
  "client_id" varchar NOT NULL,
  "client_secret" varchar NOT NULL,
  "redirect_uri" varchar NOT NULL,
  "token" varchar NOT NULL,
  "refresh_token" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "amocrm_integration" (
  "id" uuid PRIMARY KEY,
  "site_id" uuid NOT NULL UNIQUE,
  "pipeline_id" integer,
  "status_id" integer,
  "responsible" integer,
  "unsorted" boolean,
  "associations" json,
  "default_values" json,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "leads_send_log" (
  "id" uuid PRIMARY KEY,
  "lead_id" uuid NOT NULL,
  "integration_id" uuid NOT NULL,
  "integration_type" varchar NOT NULL,
  "response_id" integer,
  "source" varchar,
  "comment" varchar,
  "sended" boolean,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "photo" (
  "id" uuid PRIMARY KEY,
  "kviz_id" uuid NOT NULL,
  "photo" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

ALTER TABLE "leads_send_log" ADD FOREIGN KEY ("lead_id") REFERENCES "lead" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "project_amocrm_integration" ADD FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "project_bitrix_integration" ADD FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "mail_integration" ADD FOREIGN KEY ("site_id") REFERENCES "site" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "bitrix_integration" ADD FOREIGN KEY ("site_id") REFERENCES "site" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "amocrm_integration" ADD FOREIGN KEY ("site_id") REFERENCES "site" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "site" ADD FOREIGN KEY ("project_id") REFERENCES "project" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "kviz" ADD FOREIGN KEY ("site_id") REFERENCES "site" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "advantage" ADD FOREIGN KEY ("kviz_id") REFERENCES "kviz" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "photo" ADD FOREIGN KEY ("kviz_id") REFERENCES "kviz" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "step" ADD FOREIGN KEY ("kviz_id") REFERENCES "kviz" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "plan" ADD FOREIGN KEY ("kviz_id") REFERENCES "kviz" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;