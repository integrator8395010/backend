package repository

import (
	"backend/internal/domain/photo"
	"backend/internal/repository/photo/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "photo"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"photo_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreatePhoto(ctx context.Context, photo *photo.Photo) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsPhoto...).Values(photo.Id(), photo.KvizId(), photo.Photo(), photo.CreatedAt(), photo.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) DeletePhoto(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) ReadPhotoList(ctx context.Context) (photosList []*photo.Photo, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsPhoto...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPhoto, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Photo])
	if err != nil {
		return nil, err
	}

	photosList = []*photo.Photo{}

	for _, photo := range daoPhoto {
		photoIns, err := r.toDomainPhoto(&photo)
		if err != nil {
			return nil, err
		}

		photosList = append(photosList, photoIns)
	}

	return
}

func (r *Repository) ReadPhotoListOfKviz(ctx context.Context, kvizId uuid.UUID) (photosList []*photo.Photo, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsPhoto...).From(tableName).Where("kviz_id = ?", kvizId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPhoto, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Photo])
	if err != nil {
		return nil, err
	}

	photosList = []*photo.Photo{}

	for _, photo := range daoPhoto {
		photoIns, err := r.toDomainPhoto(&photo)
		if err != nil {
			return nil, err
		}

		photosList = append(photosList, photoIns)
	}

	return
}

func (r *Repository) onePhotoTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *photo.Photo, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsPhoto...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPhoto, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Photo])
	if err != nil {
		return nil, err
	}

	return r.toDomainPhoto(&daoPhoto)
}
