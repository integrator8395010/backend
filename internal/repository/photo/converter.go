package repository

import (
	"backend/internal/domain/photo"
	"backend/internal/repository/photo/dao"
)

func (r *Repository) toDomainPhoto(dao *dao.Photo) (*photo.Photo, error) {
	return photo.NewWithId(dao.Id, dao.KvizId, dao.Photo, dao.CreatedAt, dao.ModifiedAt) // pass is empty for security reasons
}
