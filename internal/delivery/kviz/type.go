package kviz

import (
	"time"
)

type CreateKvizRequest struct {
	SiteId          string `json:"siteId"`
	TemplateId      string `json:"templateId"`
	Name            string `json:"name"`
	Background      []byte `json:"background"`
	MainColor       string `json:"mainColor"`
	SecondaryColor  string `json:"secondaryColor"`
	SubTitle        string `json:"subTitle"`
	SubTitleItems   string `json:"subTitleItems"`
	PhoneStepTitle  string `json:"phoneStepTitle"`
	FooterTitle     string `json:"footerTitle"`
	Phone           string `json:"phone"`
	Politics        bool   `json:"politics"`
	Roistat         string `json:"roistat"`
	AdvantagesTitle string `json:"advantagesTitle"`
	PhotosTitle     string `json:"photosTitle"`
	PlansTitle      string `json:"plansTitle"`
	ResultStepText  string `json:"resultStepText"`
	Qoopler         bool   `json:"qoopler"`
	DmpOne          string `json:"dmpOne"`
	ValidatePhone   bool   `json:"validatePhone"`
}

type CreateKvizComplexRequest struct {
	Site       CreateKvizRequest  `json:"site"`
	Advantages []AdvantageComplex `json:"advantages"`
	Plans      []PlanComplex      `json:"plans"`
	Photos     [][]byte           `json:"photos"`
}

type AdvantageComplex struct {
	Title string `json:"title"`
	Photo []byte `json:"photo"`
}

type PlanComplex struct {
	KvizId       string  `json:"kvizId"`
	Title        string  `json:"title"`
	Photo        []byte  `json:"photo"`
	Rooms        int     `json:"rooms"`
	TotalArea    float32 `json:"totalArea"`
	LivingArea   float32 `json:"livingArea"`
	KitchenArea  float32 `json:"kitchenArea"`
	BedRoomArea  float32 `json:"bedRoomArea"`
	BathRoomArea float32 `json:"bathRoomArea"`
	Price        int     `json:"price"`
}

type KvizListResponse struct {
	KvizList []*KvizResponse `json:"kvizList"`
}

type KvizResponse struct {
	ID              string    `json:"id"`
	SiteId          string    `json:"siteId"`
	Name            string    `json:"name"`
	TemplateId      string    `json:"templateId"`
	Background      string    `json:"background"`
	MainColor       string    `json:"mainColor"`
	SecondaryColor  string    `json:"secondaryColor"`
	SubTitle        string    `json:"subTitle"`
	SubTitleItems   string    `json:"subTitleItems"`
	PhoneStepTitle  string    `json:"phoneStepTitle"`
	FooterTitle     string    `json:"footerTitle"`
	Phone           string    `json:"phone"`
	Politics        bool      `json:"politics"`
	Roistat         string    `json:"roistat"`
	AdvantagesTitle string    `json:"advantagesTitle"`
	PhotosTitle     string    `json:"photosTitle"`
	PlansTitle      string    `json:"plansTitle"`
	ResultStepText  string    `json:"resultStepText"`
	Qoopler         bool      `json:"qoopler"`
	DmpOne          string    `json:"dmpOne"`
	ValidatePhone   bool      `json:"validatePhone"`
	CreatedAt       time.Time `json:"createdAt"`
	ModifiedAt      time.Time `json:"modifiedAt"`
}

type UpdateKvizRequest struct {
	ID              string `form:"id"`
	SiteId          string `form:"siteId"`
	Name            string `form:"name"`
	TemplateId      string `form:"templateId"`
	Background      string `form:"background"`
	MainColor       string `form:"mainColor"`
	SecondaryColor  string `form:"secondaryColor"`
	SubTitle        string `form:"subTitle"`
	SubTitleItems   string `form:"subTitleItems"`
	PhoneStepTitle  string `form:"phoneStepTitle"`
	FooterTitle     string `form:"footerTitle"`
	Phone           string `form:"phone"`
	Politics        bool   `form:"politics"`
	Roistat         string `form:"roistat"`
	AdvantagesTitle string `form:"advantagesTitle"`
	PhotosTitle     string `form:"photosTitle"`
	PlansTitle      string `form:"plansTitle"`
	ResultStepText  string `form:"resultStepText"`
	Qoopler         bool   `form:"qoopler"`
	DmpOne          string `form:"dmpOne"`
	ValidatePhone   bool   `form:"validatePhone"`
}
