package repository

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

type MailIntegrationRepository interface {
	CreateMailIntegration(ctx context.Context, integration *integration.MailIntegration) (err error)
	UpdateMailIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.MailIntegration) (*integration.MailIntegration, error)) (integration *integration.MailIntegration, err error)
	DeleteMailIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadAllSitesMailIntegrations(ctx context.Context) (integrationList []*integration.MailIntegration, err error)
	ReadMailIntegrationOfSite(ctx context.Context, siteid uuid.UUID) (integration *integration.MailIntegration, err error)
}
