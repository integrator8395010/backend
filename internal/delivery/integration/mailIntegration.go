package integration

type CreateMailIntegrationRequest struct {
	SiteId     string   `json:"siteId"`
	Recipients []string `json:"recipients"`
}

type UpdateMailIntegrationRequest struct {
	Id         string   `json:"id"`
	SiteId     string   `json:"siteId"`
	Recipients []string `json:"recipients"`
}

type MailIntegrationResponse struct {
	ID         string   `json:"id"`
	SiteId     string   `json:"siteId"`
	Recipients []string `json:"recipients"`
	CreatedAt  string   `json:"createdAt"`
	ModifiedAt string   `json:"modifiedAt"`
}
