package delivery

import (
	jsonPhoto "backend/internal/delivery/photo"
	"backend/internal/domain/photo"
	"context"
	"io"
	"mime/multipart"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CratePhoto
// @Summary Создание плана квартиры для сайта.
// @Description Создание плана квартиры для сайта.
// @Tags photo
// @Accept multipart/form-data
// @Produce json
// @Param data formData jsonPhoto.CreatePhotoRequest true "Body"
// @Param file formData file true "Photo of photo"
// @Success 201			{object} 	jsonPhoto.PhotoResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /photo/ [put]
func (d *Delivery) CreatePhoto(c *gin.Context) {
	request := jsonPhoto.CreatePhotoRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, err := photo.New(kvizId, "")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	file, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var photoReader multipart.File
	photoReader, err = file.Open()
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	defer photoReader.Close()

	photoContent, err := io.ReadAll(photoReader)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photoResp, err := d.services.Photo.CreatePhoto(context.Background(), photo, file.Filename, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonPhoto.ToPhotoResponse(photoResp))
}

// DeletePhoto
// @Summary Удаление плана квартиры.
// @Description Удаление плана квартиры.
// @Tags photo
// @Produce json
// @Param id path string true "Photo ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /photo/{id} [delete]
func (d *Delivery) DeletePhoto(c *gin.Context) {
	id := c.Param("id")
	photoId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Photo.DeletePhoto(context.Background(), photoId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadPhotoList
// @Summary Список фото.
// @Description Список фото.
// @Tags photo
// @Produce json
// @Success 200			{object} 	jsonPhoto.PhotoListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /photo/ [get]
func (d *Delivery) ReadPhotoList(c *gin.Context) {
	photoList, err := d.services.Photo.ReadPhotoList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photoListResponse := make([]*jsonPhoto.PhotoResponse, len(photoList))

	for index, photo := range photoList {
		photoListResponse[index] = jsonPhoto.ToPhotoResponse(photo)
	}

	c.JSON(http.StatusOK, jsonPhoto.PhotoListResponse{Photos: photoListResponse})
}

// ReadPhotosOfKviz
// @Summary Список фото.
// @Description Список фото.
// @Tags photo
// @Produce json
// @Param id path string true "Photo ID"
// @Success 200			{object} 	jsonPhoto.PhotoListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /photo/{id} [get]
func (d *Delivery) ReadPhotosOfKviz(c *gin.Context) {
	id := c.Param("id")
	photoId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photoList, err := d.services.Photo.ReadPhotosOfKviz(context.Background(), photoId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photoListResponse := make([]*jsonPhoto.PhotoResponse, len(photoList))

	for index, photo := range photoList {
		photoListResponse[index] = jsonPhoto.ToPhotoResponse(photo)
	}

	c.JSON(http.StatusOK, jsonPhoto.PhotoListResponse{Photos: photoListResponse})
}
