package repository

import (
	"backend/internal/domain/lead"
	"backend/internal/repository/leadsSendLog/dao"
)

func (r *Repository) toDomainLeadsSendLog(dao *dao.LeadsSendLog) (*lead.LeadsSendLog, error) {
	return lead.NewLeadsSendLogWithId(dao.Id, dao.LeadId, dao.IntegrationId, lead.IntegrationType(dao.IntegrationType), dao.ResponseId, lead.Source(dao.Source), dao.Comment, dao.Sended, dao.CreatedAt, dao.ModifiedAt)
}
