package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/bitrixDefaultValue"
	"backend/internal/domain/integration/bitrixFiledAssociation"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateBitrixIntegration
// @Summary Создать параметры интеграции bitrix для сайта.
// @Description Создать параметры интеграции bitrix для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.CreateBitrixIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.BitrixIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/bitrix [put]
func (d *Delivery) CreateBitrixIntegration(c *gin.Context) {
	request := jsonIntegration.CreateBitrixIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	associationsList := []*bitrixFiledAssociation.BitrixFiledAssociation{}

	for _, val := range request.Associations {
		association := bitrixFiledAssociation.NewBitrixFieldAssociation(val.LeadField, val.BitrixField)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		associationsList = append(associationsList, association)
	}

	defaultValuesList := []*bitrixDefaultValue.BitrixDefaultValue{}

	for _, val := range request.DefaultValues {
		defaultValue := bitrixDefaultValue.NewBitrixDefaultValue(val.Filed, val.Value)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		defaultValuesList = append(defaultValuesList, defaultValue)
	}

	bitrixIntegration, err := integration.NewBitrixIntegration(siteId, request.Responsible, integration.SendType(request.SendType), associationsList, defaultValuesList)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.CreateBitrixIntegration(context.Background(), bitrixIntegration)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToBitrixIntegrationResponse(bitrixIntegration))
}

// UpdateBitrixIntegration
// @Summary Обновить параметры интеграции bitrix для сайта.
// @Description Обновить параметры интеграции bitrix для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.UpdateBitrixIntegrationRequest true "Body"
// @Success 200			{object} 	jsonIntegration.BitrixIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/bitrix [post]
func (d *Delivery) UpdateBitrixIntegration(c *gin.Context) {
	request := jsonIntegration.UpdateBitrixIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	associationsList := []*bitrixFiledAssociation.BitrixFiledAssociation{}

	for _, val := range request.Associations {
		association := bitrixFiledAssociation.NewBitrixFieldAssociation(val.LeadField, val.BitrixField)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		associationsList = append(associationsList, association)
	}

	defaultValuesList := []*bitrixDefaultValue.BitrixDefaultValue{}

	for _, val := range request.DefaultValues {
		defaultValue := bitrixDefaultValue.NewBitrixDefaultValue(val.Filed, val.Value)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		defaultValuesList = append(defaultValuesList, defaultValue)
	}

	updateFn := func(oldIntegration *integration.BitrixIntegration) (*integration.BitrixIntegration, error) {
		return integration.NewBitrixIntegrationWithId(id, siteId, request.Responsible, integration.SendType(request.SendType), oldIntegration.CreatedAt(), time.Now(), associationsList, defaultValuesList)
	}

	mailIntegrationNew, err := d.services.Integration.UpdateBitrixIntegration(context.Background(), id, updateFn)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToBitrixIntegrationResponse(mailIntegrationNew))
}

// DeleteBitrixIntegration
// @Summary Удалить параметры интеграции сайта.
// @Description Удалить параметры интеграции сайта.
// @Tags integration
// @Produce json
// @Param id path string true "BitrixIntegration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/bitrix/{id} [delete]
func (d *Delivery) DeleteBitrixIntegration(c *gin.Context) {
	id := c.Param("id")
	integrationId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.DeleteBitrixIntegration(context.Background(), integrationId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadBitrixIntegrationOfSite
// @Summary Получить параметры интеграции bitrix для сайта.
// @Description Получить параметры интеграции bitrix для сайта.
// @Tags integration
// @Produce json
// @Param id path string true "BitrixIntegration site ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.BitrixIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/bitrix/{id} [get]
func (d *Delivery) ReadBitrixIntegrationOfSite(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	mailIntegration, err := d.services.Integration.ReadBitrixIntegrationOfSite(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToBitrixIntegrationResponse(mailIntegration))
}
