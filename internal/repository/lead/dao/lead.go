package dao

import (
	"time"

	"github.com/google/uuid"
)

type Lead struct {
	Id             uuid.UUID `db:"id"`
	Url            string    `db:"url"`
	SiteId         uuid.UUID `db:"site_id"`
	Name           string    `db:"name"`
	Phone          string    `db:"phone"`
	Email          string    `db:"email"`
	Comment        string    `db:"comment"`
	Rositat        int       `db:"roistat"`
	Ip             string    `db:"ip"`
	UserAgent      string    `db:"user_agent"`
	UtmMedium      string    `db:"utm_medium"`
	UtmContent     string    `db:"utm_content"`
	UtmTerm        string    `db:"utm_term"`
	UtmSource      string    `db:"utm_source"`
	UtmCampaign    string    `db:"utm_campaign"`
	Yclid          string    `db:"yclid"`
	Gclid          string    `db:"gclid"`
	Fbid           string    `db:"fbid"`
	Source         string    `db:"source"`
	Spam           bool      `db:"spam"`
	YandexClientId int64     `db:"yandex_client_id"`
	CreatedAt      time.Time `db:"created_at"`
	ModifiedAt     time.Time `db:"modified_at"`
}

var ColumnsLead = []string{
	"id",
	"url",
	"site_id",
	"name",
	"phone",
	"email",
	"comment",
	"roistat",
	"ip",
	"user_agent",
	"utm_medium",
	"utm_content",
	"utm_term",
	"utm_source",
	"utm_campaign",
	"yclid",
	"gclid",
	"fbid",
	"source",
	"spam",
	"yandex_client_id",
	"created_at",
	"modified_at",
}
