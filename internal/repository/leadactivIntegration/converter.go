package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/leadactivIntegration/dao"
)

func (r *Repository) toDomainLeadactivIntegration(dao *dao.LeadactivIntegration) (*integration.LeadactivIntegration, error) {
	return integration.NewLeadactivIntegrationWithId(dao.ID, dao.SiteId, dao.Responsible, dao.CreatedAt, dao.ModifiedAt)
}
