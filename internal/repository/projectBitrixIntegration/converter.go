package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/projectBitrixIntegration/dao"
)

func (r *Repository) toDomainProjectBitrixIntegration(dao *dao.ProjectBitrixIntegration) (*integration.ProjectBitrixIntegration, error) {
	return integration.NewProjectBitrixIntegrationWithId(dao.Id, dao.ProjectId, dao.BaseUrl, dao.CreatedAt, dao.ModifiedAt)
}
