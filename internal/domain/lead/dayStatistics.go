package lead

type DayStatistic struct {
	leadsCount        uint64
	siteLeadsCount    uint64
	marquizLeadsCount uint64
	tildaLeadsCount   uint64
	flexbeLeadsCount  uint64
	spamLeadsCount    uint64
}

func NewDayStatistics() *DayStatistic {
	return &DayStatistic{}
}

func (l DayStatistic) LeadsCount() uint64 {
	return l.leadsCount
}

func (l *DayStatistic) IncLeadsCount() {
	l.leadsCount += 1
}

func (l DayStatistic) SiteLeadsCount() uint64 {
	return l.siteLeadsCount
}

func (l *DayStatistic) IncSiteLeadsCount() {
	l.siteLeadsCount += 1
}

func (l *DayStatistic) MarquizLeadsCount() uint64 {
	return l.marquizLeadsCount
}

func (l *DayStatistic) IncMarquizLeadsCount() {
	l.marquizLeadsCount += 1
}

func (l *DayStatistic) TildaLeadsCount() uint64 {
	return l.tildaLeadsCount
}

func (l *DayStatistic) IncTildaLeadsCount() {
	l.tildaLeadsCount += 1
}

func (l DayStatistic) FlexbeLeadsCount() uint64 {
	return l.flexbeLeadsCount
}

func (l *DayStatistic) IncFlexbeLeadsCount() {
	l.flexbeLeadsCount += 1
}

func (l DayStatistic) SpamLeadsCount() uint64 {
	return l.spamLeadsCount
}

func (l *DayStatistic) IncSpamLeadsCount() {
	l.spamLeadsCount += 1
}
