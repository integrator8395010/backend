package repository

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

type BitrixIntegrationRepository interface {
	CreateBitrixIntegration(ctx context.Context, integration *integration.BitrixIntegration) (err error)
	UpdateBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.BitrixIntegration) (*integration.BitrixIntegration, error)) (integration *integration.BitrixIntegration, err error)
	DeleteBitrixIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadBitrixIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.AmocrmIntegration, err error)
	ReadAllSitesBitrixIntegrations(ctx context.Context) (integrationList []*integration.BitrixIntegration, err error)
}
