package main

import (
	"backend/internal/config"
	"backend/internal/domain/user"
	"backend/internal/repository"
	"backend/pkg/mail"
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"backend/internal/service"

	lg "gitlab.com/kanya384/gotools/logger"

	"gitlab.com/kanya384/gotools/psql"
)

var (
	name  string
	login string
	pass  string
)

func main() {
	flag.StringVar(&name, "name", "", "new users name")
	flag.StringVar(&login, "login", "", "new login")
	flag.StringVar(&pass, "pass", "", "new password")
	flag.Parse()

	if name == "" {
		fmt.Print("name not provided")
		os.Exit(1)
	}

	if login == "" {
		fmt.Print("login not provided")
		os.Exit(1)
	}
	if pass == "" {
		fmt.Print("pass not provided")
		os.Exit(1)
	}

	services := initApp()
	user, err := user.New(name, login, pass, user.UserRole)
	if err != nil {
		fmt.Print("user error")
		os.Exit(1)
	}

	err = services.Auth.SignUp(context.Background(), user)

	if err != nil {
		fmt.Printf("user create error: %s", err.Error())
	}

	fmt.Println("user successfully created")
}

func initApp() (services *service.Service) {
	//read config
	cfg, err := config.InitConfig("")
	if err != nil {
		panic(fmt.Sprintf("error initializing config %s", err))
	}

	//setup logger
	logger, err := lg.New(cfg.Log.Level, cfg.App.ServiceName)
	if err != nil {
		panic(fmt.Sprintf("error initializing logger %s", err))
	}
	logFile, err := os.OpenFile(cfg.Log.Path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		panic(fmt.Sprintf("error opening log file %s", err.Error()))
	}
	logger.SetOutput(logFile)

	//db init
	pg, err := psql.New(cfg.PG.Host, cfg.PG.Port, cfg.PG.DbName, cfg.PG.User, cfg.PG.Pass, psql.MaxPoolSize(cfg.PG.PoolMax), psql.ConnTimeout(time.Duration(cfg.PG.Timeout)*time.Second))
	if err != nil {
		panic(fmt.Sprintf("postgres connection error: %s", err.Error()))
	}

	//repository
	repository, err := repository.NewRepository(pg)
	if err != nil {
		panic(fmt.Sprintf("storage initialization error: %s", err.Error()))
	}
	mailSender := mail.NewSender(cfg.Email.Host, cfg.Email.Port, cfg.Email.Login, cfg.Email.Pass)

	//services
	services, err = service.NewServices(mailSender, repository, logger, cfg.Token.Secret, cfg.Pass.Salt, cfg.BASE.URL, cfg.WhiteListIp)
	if err != nil {
		panic(fmt.Sprintf("services initialization error: %s", err.Error()))
	}
	return
}
