package repository

import (
	"backend/internal/domain/photo"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreatePhoto(ctx context.Context, photo *photo.Photo) (err error)
	DeletePhoto(ctx context.Context, id uuid.UUID) (err error)
	ReadPhotoList(ctx context.Context) (photosList []*photo.Photo, err error)
	ReadPhotoListOfKviz(ctx context.Context, kvizId uuid.UUID) (photosList []*photo.Photo, err error)
}
