package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/amocrmDefaultValue"
	"backend/internal/domain/integration/amocrmFieldAssociation"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateAmocrmIntegration
// @Summary Создать параметры интеграции amocrm для сайта.
// @Description Создать параметры интеграции amocrm для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.CreateAmocrmIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.AmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/amocrm [put]
func (d *Delivery) CreateAmocrmIntegration(c *gin.Context) {
	request := jsonIntegration.CreateAmocrmIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	associationsList := []*amocrmFieldAssociation.AmocrmFieldAssociation{}

	for _, val := range request.Associations {
		association := amocrmFieldAssociation.NewAmocrmFieldAssociation(val.LeadField, val.AmocrmField)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		associationsList = append(associationsList, association)
	}

	defaultValuesList := []*amocrmDefaultValue.AmocrmDefaultValue{}

	for _, val := range request.DefaultValues {
		defaultValue := amocrmDefaultValue.NewAmocrmDefaultValue(val.Filed, val.Value)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		defaultValuesList = append(defaultValuesList, defaultValue)
	}

	integration, err := integration.NewAmocrmIntegration(siteId, request.PipelineId, request.StatusId, request.Responsible, request.Unsorted, associationsList, defaultValuesList)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.CreateAmocrmIntegration(context.Background(), integration)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToAmocrmIntegrationResponse(integration))
}

// UpdateAmocrmIntegration
// @Summary Обновить параметры интеграции amocrm для сайта.
// @Description Обновить параметры интеграции amocrm для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.UpdateAmocrmIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.AmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/amocrm [post]
func (d *Delivery) UpdateAmocrmIntegration(c *gin.Context) {
	request := jsonIntegration.UpdateAmocrmIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	associationsList := []*amocrmFieldAssociation.AmocrmFieldAssociation{}

	for _, val := range request.Associations {
		association := amocrmFieldAssociation.NewAmocrmFieldAssociation(val.LeadField, val.AmocrmField)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		associationsList = append(associationsList, association)
	}

	defaultValuesList := []*amocrmDefaultValue.AmocrmDefaultValue{}

	for _, val := range request.DefaultValues {
		defaultValue := amocrmDefaultValue.NewAmocrmDefaultValue(val.Filed, val.Value)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		defaultValuesList = append(defaultValuesList, defaultValue)
	}

	updateFn := func(oldIntegration *integration.AmocrmIntegration) (*integration.AmocrmIntegration, error) {
		return integration.NewAmocrmIntegrationWithId(id, siteId, request.PipelineId, request.StatusId, request.Responsible, request.Unsorted, associationsList, defaultValuesList, oldIntegration.CreatedAt(), time.Now())
	}

	mailIntegrationNew, err := d.services.Integration.UpdateAmocrmIntegration(context.Background(), id, updateFn)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToAmocrmIntegrationResponse(mailIntegrationNew))
}

// DeleteAmocrmIntegration
// @Summary Удалить параметры интеграции сайта.
// @Description Удалить параметры интеграции сайта.
// @Tags integration
// @Produce json
// @Param id path string true "AmocrmIntegration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/{id} [delete]
func (d *Delivery) DeleteAmocrmIntegration(c *gin.Context) {
	id := c.Param("id")
	integrationId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.DeleteAmocrmIntegration(context.Background(), integrationId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadAmocrmIntegrationOfSite
// @Summary Получить параметры интеграции amocrm для сайта.
// @Description Получить параметры интеграции amocrm для сайта.
// @Tags integration
// @Produce json
// @Param id path string true "AmocrmIntegration site ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.AmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/{id} [get]
func (d *Delivery) ReadAmocrmIntegrationOfSite(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	mailIntegration, err := d.services.Integration.ReadAmocrmIntegrationOfSite(context.Background(), siteId)
	if err != nil && err.Error() != "no rows in result set" {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToAmocrmIntegrationResponse(mailIntegration))
}
