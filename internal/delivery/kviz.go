package delivery

import (
	jsonKviz "backend/internal/delivery/kviz"
	"backend/internal/domain/kviz"
	"context"
	"io"
	"mime/multipart"
	"net/http"
	"time"

	serviceKviz "backend/internal/service/kviz"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CrateKviz
// @Summary Создание квиза для сайта.
// @Description Создание квиза для сайта.
// @Tags kviz
// @Accept multipart/form-data
// @Produce json
// @Param data formData jsonKviz.CreateKvizRequest true "Body"
// @Param background formData file true "Background of kviz"
// @Success 201			{object} 	jsonKviz.KvizResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /kviz/ [put]
func (d *Delivery) CreateKviz(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonKviz.CreateKvizRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	templateId, err := uuid.Parse(request.TemplateId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kviz, err := kviz.New(siteId, request.Name, templateId, "", request.MainColor, request.SecondaryColor, request.SubTitle, request.SubTitleItems, request.PhoneStepTitle, request.FooterTitle, request.Phone, request.Politics, request.Roistat, request.AdvantagesTitle, request.PhotosTitle, request.PlansTitle, request.ResultStepText, request.Qoopler, request.DmpOne, request.ValidatePhone, auth.UserId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var photoReader multipart.File
	photoReader, err = photo.Open()
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	defer photoReader.Close()

	photoContent, err := io.ReadAll(photoReader)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizResp, err := d.services.Kviz.CreateKviz(context.Background(), kviz, photo.Filename, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonKviz.ToKvizResponse(kvizResp))
}

// UpdateKviz
// @Summary Обновление квиза.
// @Description Обновление квиза.
// @Tags kviz
// @Accept multipart/form-data
// @Produce json
// @Param data formData jsonKviz.UpdateKvizRequest true "Body"
// @Param background formData file false "Background of kviz"
// @Success 200			{object} 	jsonKviz.KvizResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /kviz/ [post]
func (d *Delivery) UpdateKviz(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonKviz.UpdateKvizRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.ID)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	templateId, err := uuid.Parse(request.TemplateId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kviz, err := kviz.NewWithId(id, siteId, request.Name, templateId, "", request.MainColor, request.SecondaryColor, request.SubTitle, request.SubTitleItems, request.PhoneStepTitle, request.FooterTitle, request.Phone, request.Politics, request.Roistat, request.AdvantagesTitle, request.PhotosTitle, request.PlansTitle, request.ResultStepText, request.Qoopler, request.DmpOne, request.ValidatePhone, auth.UserId, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, errFile := c.FormFile("file")
	fileName := ""
	photoContent := []byte{}
	if errFile == nil {
		fileName = photo.Filename
		var photoReader multipart.File
		photoReader, err = photo.Open()
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		defer photoReader.Close()

		photoContent, err = io.ReadAll(photoReader)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
	}

	kvizResp, err := d.services.Kviz.UpdateKviz(context.Background(), kviz, fileName, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonKviz.ToKvizResponse(kvizResp))
}

// DeleteKviz
// @Summary Удаление квиза.
// @Description Удаление квиза.
// @Tags kviz
// @Produce json
// @Param id path string true "Kviz ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /kviz/{id} [delete]
func (d *Delivery) DeleteKviz(c *gin.Context) {
	id := c.Param("id")
	kvizId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Kviz.DeleteKviz(context.Background(), kvizId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadKvizListOfProject
// @Summary Список квизов проекта.
// @Description Список квизов проекта.
// @Tags kviz
// @Param id path string true "Site ID"
// @Produce json
// @Success 200			{object} 	jsonKviz.KvizListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /kviz/{id} [get]
func (d *Delivery) ReadKvizListOfProject(c *gin.Context) {
	id := c.Param("id")
	projectId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizList, err := d.services.Kviz.ReadKvizListOfProject(context.Background(), projectId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizListResponse := make([]*jsonKviz.KvizResponse, len(kvizList))

	for index, kviz := range kvizList {
		kvizListResponse[index] = jsonKviz.ToKvizResponse(kviz)
	}

	c.JSON(http.StatusOK, jsonKviz.KvizListResponse{KvizList: kvizListResponse})
}

// CrateKvizComplex
// @Summary Создание квиза для сайта.
// @Description Создание квиза для сайта.
// @Tags kviz
// @Accept json
// @Produce json
// @Param kviz 	body 		jsonKviz.CreateKvizComplexRequest 		    true  "Данные по квизу, его преимуществам и т.д."
// @Success 201			{object} 	jsonKviz.KvizResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /kviz/complex [put]
func (d *Delivery) CrateKvizComplex(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonKviz.CreateKvizComplexRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kviz := &serviceKviz.Kviz{
		SiteId:          request.Site.SiteId,
		TemplateId:      request.Site.TemplateId,
		Name:            request.Site.Name,
		Background:      request.Site.Background,
		MainColor:       request.Site.MainColor,
		SecondaryColor:  request.Site.SecondaryColor,
		SubTitle:        request.Site.SubTitle,
		SubTitleItems:   request.Site.SubTitleItems,
		PhoneStepTitle:  request.Site.PhoneStepTitle,
		FooterTitle:     request.Site.FooterTitle,
		Phone:           request.Site.Phone,
		Politics:        request.Site.Politics,
		Roistat:         request.Site.Roistat,
		AdvantagesTitle: request.Site.AdvantagesTitle,
		PhotosTitle:     request.Site.PhotosTitle,
		PlansTitle:      request.Site.PlansTitle,
		ResultStepText:  request.Site.ResultStepText,
		Qoopler:         request.Site.Qoopler,
	}

	advantages := make([]*serviceKviz.Advantage, len(request.Advantages))

	for index, advantage := range request.Advantages {
		advantages[index] = &serviceKviz.Advantage{
			Title: advantage.Title,
			Photo: advantage.Photo,
		}
	}

	plans := make([]*serviceKviz.Plan, len(request.Plans))

	for index, plan := range request.Plans {
		plans[index] = &serviceKviz.Plan{
			KvizId:       plan.KvizId,
			Title:        plan.Title,
			Photo:        plan.Photo,
			Rooms:        plan.Rooms,
			TotalArea:    plan.TotalArea,
			LivingArea:   plan.LivingArea,
			KitchenArea:  plan.KitchenArea,
			BedRoomArea:  plan.BathRoomArea,
			BathRoomArea: plan.BathRoomArea,
		}
	}

	_, err = d.services.Kviz.CreateKvizComplex(context.Background(), kviz, advantages, plans, request.Photos, auth.UserId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	//c.JSON(http.StatusOK, jsonKviz.ToKvizResponse(kvizResp))
}
