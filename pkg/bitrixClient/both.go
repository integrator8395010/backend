package bitrixClient

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/goccy/go-json"
)

const (
	sendComment      = "crm.timeline.comment.add"
	sendNotification = "im.notify.personal.add"
)

func (c *Client) sendComment(id int, entityType, comment string) (err error) {
	request := map[string]interface{}{
		"fields": map[string]interface{}{
			"ENTITY_ID":   id,
			"ENTITY_TYPE": entityType,
			"COMMENT":     "Повторный лид:\n" + comment,
		},
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(sendComment, request, &response)
	if err != nil {
		return
	}
	return
}

func (c *Client) sendNotification(userId int, text string) (err error) {
	request := map[string]interface{}{
		"USER_ID":     userId,
		"MESSAGE":     text,
		"MESSAGE_OUT": text,
		"TAG":         "Повторное обращение",
		"SUB_TAG":     "Поступило повторное обращение",
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(sendNotification, request, &response)
	if err != nil {
		return
	}
	return
}

func (c *Client) sendRequest(urlPostfix string, data interface{}, response interface{}) (code int, err error) {
	url := fmt.Sprintf("%s%s", c.baseUrl, urlPostfix)

	jsonLead, err := json.Marshal(data)
	if err != nil {
		return 0, err
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonLead))
	if err != nil {
		return 0, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("no body in response")
	}

	err = json.Unmarshal(body, response)
	if err != nil {
		return 0, fmt.Errorf("error decode response from %s - error %s, body of response: %s", url, err.Error(), string(body))
	}

	return resp.StatusCode, err
}

func (c *Client) getCrmHostFromUrl() (string, error) {
	url, err := url.Parse(c.baseUrl)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("https://%s", url.Hostname()), nil

}
