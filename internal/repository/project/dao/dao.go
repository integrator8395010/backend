package dao

import (
	"time"

	"github.com/google/uuid"
)

type Project struct {
	Id         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	Photo      string    `db:"photo"`
	CreatedBy  uuid.UUID `db:"created_by"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsProject = []string{
	"id",
	"name",
	"photo",
	"created_by",
	"created_at",
	"modified_at",
}
