package integration

type LeadactivAmocrmSecretsIntegrationResponse struct {
	ID           string `json:"id"`
	BaseUrl      string `json:"baseUrl"`
	ClientId     string `json:"clientId"`
	ClientSecret string `json:"clientSecret"`
	RedirectUri  string `json:"redirectUri"`
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
	CreatedAt    string `json:"createdAt"`
	ModifiedAt   string `json:"modifiedAt"`
}

type CreateLeadactivAmocrmIntegrationRequest struct {
	SiteId      string `json:"siteId"`
	Responsible int    `json:"responsible"`
}

type UpdateLeadactivAmocrmIntegrationRequest struct {
	Id          string `json:"id"`
	SiteId      string `json:"siteId"`
	Responsible int    `json:"responsible"`
}

type LeadactivAmocrmIntegrationResponse struct {
	Id          string `json:"id"`
	SiteId      string `json:"siteId"`
	Responsible int    `json:"responsible"`
	CreatedAt   string `json:"createdAt"`
	ModifiedAt  string `json:"modifiedAt"`
}
