package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/amocrmIntegration/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "amocrm_integration"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"amocrm_integration_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateAmocrmIntegration(ctx context.Context, integration *integration.AmocrmIntegration) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	daoIntegration, err := r.toRepositoryDaoAmoCrmIntegration(integration)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsAmocrmIntegration...).Values(daoIntegration.Id, daoIntegration.SiteId, daoIntegration.PipelineId, daoIntegration.StatusId, daoIntegration.Responsible, daoIntegration.Unsorted, daoIntegration.Associations, daoIntegration.DefaultValues, daoIntegration.CreatedAt, daoIntegration.ModifiedAt)
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateAmocrmIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.AmocrmIntegration) (*integration.AmocrmIntegration, error)) (integration *integration.AmocrmIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLead, err := r.oneIntegrationTx(ctx, tx, id)
	if err != nil {
		return
	}

	amocrmIntegrationNew, err := updateFn(upLead)
	if err != nil {
		return
	}

	daoIntegration, err := r.toRepositoryDaoAmoCrmIntegration(amocrmIntegrationNew)
	if err != nil {
		return
	}
	rawQuery := r.Builder.Update(tableName).Set("site_id", amocrmIntegrationNew.SiteId()).Set("pipeline_id", daoIntegration.PipelineId).Set("status_id", daoIntegration.StatusId).Set("responsible", daoIntegration.Responsible).Set("unsorted", daoIntegration.Unsorted).Set("associations", daoIntegration.Associations).Set("default_values", daoIntegration.DefaultValues).Set("modified_at", daoIntegration.ModifiedAt).Where("id = ?", daoIntegration.Id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return amocrmIntegrationNew, err
}

func (r *Repository) DeleteAmocrmIntegration(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadAmocrmIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.AmocrmIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsAmocrmIntegration...).From(tableName).Where("site_id = ?", siteId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.AmocrmIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainAmocrmIntegration(&daoIntegration)
}

func (r *Repository) ReadAmocrmIntegrationById(ctx context.Context, id uuid.UUID) (integration *integration.AmocrmIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsAmocrmIntegration...).From(tableName).Where("id = ?", id)

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.AmocrmIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainAmocrmIntegration(&daoIntegration)
}

func (r *Repository) ReadAllSitesAmocrmIntegrations(ctx context.Context) (integrationList []*integration.AmocrmIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsAmocrmIntegration...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegrations, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.AmocrmIntegration])
	if err != nil {
		return nil, err
	}

	integrationList = make([]*integration.AmocrmIntegration, len(daoIntegrations))

	for index, daoIntegration := range daoIntegrations {
		domainIntegration, err := r.toDomainAmocrmIntegration(&daoIntegration)
		if err != nil {
			return nil, err
		}

		integrationList[index] = domainIntegration
	}

	return
}

func (r *Repository) oneIntegrationTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *integration.AmocrmIntegration, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsAmocrmIntegration...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.AmocrmIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainAmocrmIntegration(&daoLead)
}
