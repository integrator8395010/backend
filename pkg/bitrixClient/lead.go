package bitrixClient

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/bitrixFiledAssociation"
	"backend/internal/domain/lead"
	"fmt"
	"strconv"
)

const (
	sendLead        = "crm.lead.add.json"
	phoneLeadSearch = "crm.lead.list.json"
)

func (c *Client) SendLead(lead *lead.Lead, integration *integration.BitrixIntegration) (responseId int, err error) {
	//TODO get site field from integration, if exists, and check if it's same for contact
	urlField := getSiteUrlFileldOfIntegration(integration.Associations())
	leadId, responsibleId, err := c.searchLeadByPhone(lead.Phone(), lead.Url(), urlField)
	if err == nil {
		//TODO - send comment and task
		err = c.sendComment(leadId, "lead", lead.Comment())
		if err != nil {
			return leadId, fmt.Errorf("error sending comment for lead: %s", err.Error())
		}

		crmUrl, err := c.getCrmHostFromUrl()
		if err != nil {
			return 0, fmt.Errorf("error getting host from url: %s", err.Error())
		}

		err = c.sendNotification(responsibleId, fmt.Sprintf("Повторное обращение: %s/crm/lead/details/%d/", crmUrl, leadId))
		if err != nil {
			return leadId, fmt.Errorf("error sending comment for deal: %s", err.Error())
		}

		return leadId, err
	}
	return c.doSendLead(lead, integration)
}

func (c *Client) doSendLead(lead *lead.Lead, integration *integration.BitrixIntegration) (responseId int, err error) {
	leadParams := map[string]interface{}{
		"fields": map[string]interface{}{
			"TITLE": "Заявка с сайта",
			"PHONE": []map[string]string{{
				"VALUE":      lead.Phone(),
				"VALUE_TYPE": "WORK",
			}},
			"COMMENTS":       lead.Comment(),
			"ASSIGNED_BY_ID": integration.Responsible(),
			"CREATED_BY_ID":  integration.Responsible(),
			"WEB":            []map[string]string{{"VALUE": lead.Url(), "VALUE_TYPE": "WORK"}},
		},
		"params": map[string]string{"REGISTER_SONET_EVENT": "Y"},
	}

	if lead.Name() != "" {
		leadParams["fields"].(map[string]interface{})["NAME"] = lead.Name()
	}

	if lead.Email() != "" {
		leadParams["fields"].(map[string]interface{})["Email"] = map[string]string{
			"VALUE":      lead.Email(),
			"VALUE_TYPE": "WORK",
		}
	}

	for _, value := range integration.DefaultValues() {
		leadParams["fields"].(map[string]interface{})[value.Field()] = value.Value()
	}

	for _, value := range integration.Associations() {
		fieldValue, err := lead.FieldByName(value.LeadField())
		if err != nil {
			return 0, fmt.Errorf("not founded lead field by name %s: %s", value.LeadField(), err.Error())
		}
		leadParams["fields"].(map[string]interface{})[value.BitrixField()] = fieldValue
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(sendLead, leadParams, &response)
	if err != nil {
		return
	}
	if result, ok := response["result"]; ok {
		return int(result.(float64)), nil
	}
	return
}

func (c *Client) searchLeadByPhone(phone string, url string, urlField string) (leadId int, responsibleId int, err error) {

	fitler := map[string]string{"PHONE": phone}
	if urlField != "" {
		fitler[urlField] = url
	}

	request := map[string]interface{}{
		"order":  map[string]string{"DATE_CREATE": "DESC"},
		"filter": fitler,
		"select": []string{"ID", "ASSIGNED_BY_ID"},
	}

	response := map[string]interface{}{}
	_, err = c.sendRequest(phoneLeadSearch, request, &response)
	if err != nil {
		return
	}
	if result, ok := response["result"]; ok {
		list := result.([]interface{})
		if len(list) > 0 {
			stringId := list[0].(map[string]interface{})["ID"].(string)
			id, err := strconv.Atoi(stringId)
			if err != nil {
				return 0, 0, err
			}

			stringResponsibleId := list[0].(map[string]interface{})["ASSIGNED_BY_ID"].(string)
			responsibleId, err := strconv.Atoi(stringResponsibleId)
			if err != nil {
				return 0, 0, err
			}
			return id, responsibleId, nil
		}
		return 0, 0, fmt.Errorf("no leads")
	}
	return
}

func getSiteUrlFileldOfIntegration(fields []*bitrixFiledAssociation.BitrixFiledAssociation) (field string) {
	for _, value := range fields {
		if value.LeadField() == "url" {
			return value.BitrixField()
		}
	}
	return
}
