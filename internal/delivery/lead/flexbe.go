package lead

type CreateLeadFromFlexbeRequest struct {
	Data    CreateLeadFromFlexbeRequestData `json:"data" binding:"required"`
	SiteUrl string                          `json:"site[domain]"`
}

type CreateLeadFromFlexbeRequestData struct {
	Client FlexbeClient `json:"data[client]" binding:"required"`
	Site   FlexbeSite   `json:"data[page]"`
	Utm    FlexbeUtms   `json:"data[utm]"`
}

type FlexbeSite struct {
	Url  string `form:"data[page][url]"`
	Name string `form:"data[page][name]"`
}

type FlexbeClient struct {
	Email string `form:"data[client][email]"`
	Name  string `form:"data[client][name]"`
	Phone string `form:"data[client][phone]"`
}

type FlexbeAnswerStruct struct {
	Name  string
	Value string
}

type FlexbeUtms struct {
	YmClientId string `form:"data[utm][ym_client_id]"`
	Source     string `form:"data[utm][utm_source]"`
	Campaign   string `form:"data[utm][utm_campaign]"`
	Medium     string `form:"data[utm][utm_medium]"`
	Term       string `form:"data[utm][utm_term]"`
	Content    string `form:"data[utm][utm_content]"`
	Url        string `form:"data[utm][url]"`
	Ip         string `form:"data[utm][ip]"`
}
