package amocrmDefaultValue

type AmocrmDefaultValue struct {
	field int
	value interface{}
}

func NewAmocrmDefaultValue(field int, value interface{}) *AmocrmDefaultValue {
	return &AmocrmDefaultValue{
		field: field,
		value: value,
	}
}

func (a AmocrmDefaultValue) Field() int {
	return a.field
}

func (a AmocrmDefaultValue) Value() interface{} {
	return a.value
}
