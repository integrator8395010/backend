package photo

import (
	"backend/internal/domain/photo"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreatePhoto(ctx context.Context, photo *photo.Photo, inputFileName string, inputFileContent []byte) (*photo.Photo, error)
	DeletePhoto(ctx context.Context, id uuid.UUID) (err error)
	ReadPhotoList(ctx context.Context) (photosList []*photo.Photo, err error)
	ReadPhotosOfKviz(ctx context.Context, kvizId uuid.UUID) (photosList []*photo.Photo, err error)
}
