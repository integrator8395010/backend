package lead

import (
	"time"

	"github.com/google/uuid"
)

type SpamLeadsFilter struct {
	projectId uuid.UUID
	url       string
	from      time.Time
	to        time.Time
}

func NewSpamLeadsFilter() *SpamLeadsFilter {
	return &SpamLeadsFilter{}
}

func (f SpamLeadsFilter) ProjectId() uuid.UUID {
	return f.projectId
}

func (f *SpamLeadsFilter) SetProjectId(projectId uuid.UUID) {
	f.projectId = projectId
}

func (f SpamLeadsFilter) Url() string {
	return f.url
}

func (f *SpamLeadsFilter) SetUrl(url string) {
	f.url = url
}

func (f SpamLeadsFilter) From() time.Time {
	return f.from
}

func (f *SpamLeadsFilter) SetFrom(from time.Time) {
	f.from = from
}

func (f SpamLeadsFilter) To() time.Time {
	return f.to
}

func (f *SpamLeadsFilter) SetTo(to time.Time) {
	f.to = to
}
