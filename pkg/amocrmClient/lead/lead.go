package lead

import (
	"backend/pkg/amocrmClient/customField"
	"backend/pkg/amocrmClient/embeeded"
)

type Lead struct {
	Id                int                        `json:"id"`                   //ID сделки
	Name              string                     `json:"name"`                 //Название сделки
	Price             int                        `json:"price"`                //Бюджет сделки
	ResponsibleUserId int                        `json:"responsible_user_id"`  //ID пользователя, ответственного за сделку
	StatusId          int                        `json:"status_id"`            // ID статуса, в который добавляется сделка
	PipelineId        int                        `json:"pipeline_id"`          //ID воронки, в которую добавляется сделка
	CreatedBy         int                        `json:"created_by"`           //ID пользователя, создающий сделку
	UpdatedBy         int                        `json:"updated_by"`           //ID пользователя, создающий сделку
	Embedded          *embeeded.Embedded         `json:"_embedded"`            //Данные вложенных сущностей, tags, contacts сюда
	Text              string                     `json:"text"`                 //Данные вложенных сущностей, tags, contacts сюда
	CustomFileds      []*customField.CustomField `json:"custom_fields_values"` //Доп поля, utm_medium, utm_term и т.п.
}

type LeadUnsorted struct {
	Id                int                        `json:"id"`                   //ID сделки
	Name              string                     `json:"name"`                 //Название сделки
	ResponsibleUserId int                        `json:"responsible_user_id"`  //ID пользователя, ответственного за сделку
	PipelineId        int                        `json:"pipeline_id"`          //ID воронки, в которую добавляется сделка
	CreatedBy         int                        `json:"created_by"`           //ID пользователя, создающий сделку
	UpdatedBy         int                        `json:"updated_by"`           //ID пользователя, создающий сделку
	Embedded          *embeeded.Embedded         `json:"_embedded"`            //Данные вложенных сущностей, tags, contacts сюда
	Text              string                     `json:"text"`                 //Данные вложенных сущностей, tags, contacts сюда
	CustomFileds      []*customField.CustomField `json:"custom_fields_values"` //Доп поля, utm_medium, utm_term и т.п.
}
