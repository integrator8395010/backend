package dao

import (
	"time"

	"github.com/google/uuid"
)

type Advantage struct {
	Id         uuid.UUID `db:"id"`
	KvizId     uuid.UUID `db:"kviz_id"`
	Title      string    `db:"title"`
	Photo      string    `db:"photo"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsAdvantage = []string{
	"id",
	"kviz_id",
	"title",
	"photo",
	"created_at",
	"modified_at",
}
