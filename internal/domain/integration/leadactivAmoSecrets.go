package integration

import (
	"time"

	"github.com/google/uuid"
)

type LeadactivAmoSecrets struct {
	id           uuid.UUID
	baseUrl      string
	clientId     string
	clientSecret string
	redirectUri  string
	token        string
	refreshToken string
	createdAt    time.Time
	modifiedAt   time.Time
}

func (l *LeadactivAmoSecrets) Id() uuid.UUID {
	return l.id
}

func (l *LeadactivAmoSecrets) BaseUrl() string {
	return l.baseUrl
}

func (l *LeadactivAmoSecrets) ClientId() string {
	return l.clientId
}

func (l *LeadactivAmoSecrets) ClientSecret() string {
	return l.clientSecret
}

func (l *LeadactivAmoSecrets) RedirectUri() string {
	return l.redirectUri
}

func (l *LeadactivAmoSecrets) Token() string {
	return l.token
}

func (l *LeadactivAmoSecrets) RefreshToken() string {
	return l.refreshToken
}

func (l *LeadactivAmoSecrets) CreatedAt() time.Time {
	return l.createdAt
}

func (l *LeadactivAmoSecrets) ModifiedAt() time.Time {
	return l.modifiedAt
}

func NewLeadactivAmoSecretsWithId(
	id uuid.UUID,
	baseUrl string,
	clientId string,
	clientSecret string,
	redirectUri string,
	token string,
	refreshToken string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*LeadactivAmoSecrets, error) {
	return &LeadactivAmoSecrets{
		id:           id,
		baseUrl:      baseUrl,
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  redirectUri,
		token:        token,
		refreshToken: refreshToken,
		createdAt:    createdAt,
		modifiedAt:   modifiedAt,
	}, nil
}

func NewLeadactivAmoSecrets(
	baseUrl string,
	clientId string,
	clientSecret string,
	redirectUri string,
	token string,
	refreshToken string,
) (*LeadactivAmoSecrets, error) {
	return &LeadactivAmoSecrets{
		id:           uuid.New(),
		baseUrl:      baseUrl,
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  redirectUri,
		token:        token,
		refreshToken: refreshToken,
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}

func NewLeadactivSecretsOauth(clientId, clientSecret string) (*LeadactivAmoSecrets, error) {
	return &LeadactivAmoSecrets{
		id:           uuid.New(),
		baseUrl:      "",
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  "",
		token:        "",
		refreshToken: "",
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}
