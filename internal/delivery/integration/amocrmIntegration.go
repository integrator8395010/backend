package integration

type CreateAmocrmIntegrationOauthSecretRequest struct {
	ClientId     string `json:"client_id" binding:"required"`
	ClientSecret string `json:"client_secret" binding:"required"`
	State        string `json:"state" binding:"required"`
}

type CreateAmocrmIntegrationOauthCodeRequest struct {
	Code     string `form:"code" binding:"required"`
	Referer  string `form:"referer" binding:"required"`
	ClientId string `form:"client_id" json:"client_id" binding:"required"`
}

type CreateProjectAmocrmIntegrationRequest struct {
	SiteId        string         `json:"siteId"`
	PipelineId    int            `json:"pipelineId"`
	StatusId      int            `json:"statusId"`
	Responsible   int            `json:"responsible"`
	Unsorted      bool           `json:"unsorted"`
	Associations  []Association  `json:"associations"`
	DefaultValues []DefaultValue `json:"defaultValues"`
}

type Association struct {
	LeadField   string `json:"lead_field"`
	AmocrmField int    `json:"amocrm_field"`
}

type DefaultValue struct {
	Filed int         `json:"filed"`
	Value interface{} `json:"value"`
}

type CreateAmocrmIntegrationRequest struct {
	SiteId        string         `json:"siteId"`
	PipelineId    int            `json:"pipeline_id"`
	StatusId      int            `json:"status_id"`
	Responsible   int            `json:"responsible"`
	Unsorted      bool           `json:"unsorted"`
	Associations  []Association  `json:"associations"`
	DefaultValues []DefaultValue `json:"defaultValues"`
}

type UpdateAmocrmIntegrationRequest struct {
	Id            string         `json:"id"`
	SiteId        string         `json:"siteId"`
	PipelineId    int            `json:"pipeline_id"`
	StatusId      int            `json:"status_id"`
	Responsible   int            `json:"responsible"`
	Unsorted      bool           `json:"unsorted"`
	Associations  []Association  `json:"associations"`
	DefaultValues []DefaultValue `json:"defaultValues"`
}

type ProjectAmocrmIntegrationResponse struct {
	ID           string `json:"id"`
	ProjectId    string `json:"projectId"`
	BaseUrl      string `json:"baseUrl"`
	ClientId     string `json:"clientId"`
	ClientSecret string `json:"clientSecret"`
	RedirectUri  string `json:"redirectUri"`
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
	CreatedAt    string `json:"createdAt"`
	ModifiedAt   string `json:"modifiedAt"`
}

type AmocrmIntegrationResponse struct {
	ID            string         `json:"id"`
	SiteId        string         `json:"siteId"`
	PipelineId    int            `json:"pipeline_id"`
	StatusId      int            `json:"status_id"`
	Responsible   int            `json:"responsible"`
	Unsorted      bool           `json:"unsorted"`
	Associations  []Association  `json:"associations"`
	DefaultValues []DefaultValue `json:"defaultValues"`
	CreatedAt     string         `json:"createdAt"`
	ModifiedAt    string         `json:"modifiedAt"`
}
