package delivery

import (
	docs "backend/internal/delivery/swagger/docs"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title leadactiv backend service
// @version 1.0
// @description leadactiv backend service
// @license.name kanya384

// @contact.name API Support
// @contact.email kanya384@mail.ru

// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func (d *Delivery) initRouter() *gin.Engine {

	var router = gin.New()
	router.Use(cors.Default())

	router.Static("/file-store", "./file-store")
	router.GET("/", d.ProxyToQuiz)
	router.GET("/admin/*proxyPath", d.ProxyToAdmin)

	router.Use(d.RequestLeadsLoggerMiddleware())
	router.PUT("/lead/site", (d.CreateLeadFromSite))
	router.POST("/lead/site", d.CreateLeadFromSite)
	router.POST("/lead/tilda", d.CreateLeadFromTilda)
	router.POST("/lead/marquiz", d.CreateLeadFromMarquiz)
	router.POST("/lead/flexbe", d.CreateLeadFromFlexbe)
	router.POST("/integration/amocrm/project/secret/:id", d.CreateProjectAmocrmIntegrationOauthSecret)
	router.GET("/integration/amocrm/project/code/:id", d.CreateProjectAmocrmIntegrationOauthCode)

	router.POST("/integration/amocrm/leadactiv/secret", d.CreateLeadactivAmocrmIntegrationOauthSecret)
	router.GET("/integration/amocrm/leadactiv/code", d.CreateLeadactivAmocrmIntegrationOauthCode)

	d.routerDocs(router.Group("/docs"))
	d.routerAuth(router.Group("/auth"))

	router.Use(d.checkAuth)

	d.routerProject(router.Group("/project"))
	d.routerAdvantage(router.Group("/advantage"))
	d.routerPlan(router.Group("/plan"))
	d.routerSite(router.Group("/site"))
	d.routerStep(router.Group("/step"))
	d.routerKviz(router.Group("/kviz"))
	d.routerPhoto(router.Group("/photo"))
	d.routerLead(router.Group("/lead"))
	d.routerIntegration(router.Group("/integration"))

	return router
}

func (d *Delivery) routerAuth(router *gin.RouterGroup) {
	router.POST("/sign-in", d.SignIn)
}

func (d *Delivery) routerLead(router *gin.RouterGroup) {
	router.POST("/list", d.ReadLeadsFiltredList)
	router.POST("/statistics", d.LeadsStatisticsOfProject)
	router.POST("/spam-list", d.ReadSpamFiltredList)
	router.POST("/spam-list-xls", d.ExcelSpamFiltredList)
}

func (d *Delivery) routerProject(router *gin.RouterGroup) {
	router.GET("/", d.ReadProjectList)
	router.PUT("/", d.CreateProject)
	router.POST("/", d.UpdateProject)
	router.DELETE("/:id", d.DeleteProject)
}

func (d *Delivery) routerAdvantage(router *gin.RouterGroup) {
	router.GET("/", d.ReadAdvantageList)
	router.PUT("/", d.CreateAdvantage)
	router.POST("/", d.UpdateAdvantage)
	router.DELETE("/:id", d.DeleteAdvantage)
	router.GET("/:id", d.ReadAdvantageOfKviz)
}

func (d *Delivery) routerPlan(router *gin.RouterGroup) {
	router.GET("/", d.ReadPlanList)
	router.PUT("/", d.CreatePlan)
	router.POST("/", d.UpdatePlan)
	router.DELETE("/:id", d.DeletePlan)
	router.GET("/:id", d.ReadPlansOfKviz)
}

func (d *Delivery) routerSite(router *gin.RouterGroup) {
	router.GET("/", d.ReadSiteList)
	router.GET("/:id", d.ReadSiteById)
	router.GET("/project/:id", d.ReadSitesOfProjet)
	router.PUT("/", d.CreateSite)
	router.POST("/", d.UpdateSite)
	router.DELETE("/:id", d.DeleteSite)
}

func (d *Delivery) routerStep(router *gin.RouterGroup) {
	router.GET("/", d.ReadStepList)
	router.PUT("/", d.CreateStep)
	router.POST("/", d.UpdateStep)
	router.DELETE("/:id", d.DeleteStep)
}

func (d *Delivery) routerKviz(router *gin.RouterGroup) {
	router.PUT("/", d.CreateKviz)
	router.PUT("/complex", d.CrateKvizComplex)
	router.POST("/", d.UpdateKviz)
	router.DELETE("/:id", d.DeleteKviz)
	router.GET("/:id", d.ReadKvizListOfProject)
}

func (d *Delivery) routerPhoto(router *gin.RouterGroup) {
	router.PUT("/", d.CreatePhoto)
	router.DELETE("/:id", d.DeletePhoto)
	router.GET("/:id", d.ReadPhotosOfKviz)
}

func (d *Delivery) routerIntegration(router *gin.RouterGroup) {
	router.GET("/amocrm/leadactiv", d.ReadLeadactivAmocrmIntegration)

	router.PUT("/bitrix", d.CreateBitrixIntegration)
	router.POST("/bitrix", d.UpdateBitrixIntegration)
	router.DELETE("/bitrix/:id", d.DeleteBitrixIntegration)
	router.GET("/bitrix/:id", d.ReadBitrixIntegrationOfSite)

	router.PUT("/bitrix/project", d.CreateProjectBitrixIntegration)
	router.POST("/bitrix/project", d.UpdateProjectBitrixIntegration)
	router.DELETE("/bitrix/project/:id", d.DeleteProjectBitrixIntegration)
	router.GET("/bitrix/project/:id", d.ReadProjectBitrixIntegration)

	router.PUT("/leadactiv", d.CreateLeadactivAmocrmIntegration)
	router.POST("/leadactiv", d.UpdateLeadactivAmocrmIntegration)
	router.DELETE("/leadactiv/:id", d.DeleteLeadactivAmocrmIntegration)
	router.GET("/leadactiv/:id", d.ReadLeadactivAmocrmIntegrationOfSite)
	router.GET("/leadactiv/users/:page", d.ReadLeadactivUsersList)

	router.PUT("/mail", d.CreateMailIntegration)
	router.POST("/mail", d.UpdateMailIntegration)
	router.POST("/mail/:id", d.DeleteMailIntegration)
	router.GET("/mail/:id", d.ReadMailIntegrationOfSite)

	router.PUT("/amocrm", d.CreateAmocrmIntegration)
	router.POST("/amocrm", d.UpdateAmocrmIntegration)
	router.DELETE("/amocrm/:id", d.DeleteAmocrmIntegration)
	router.GET("/amocrm/:id", d.ReadAmocrmIntegrationOfSite)

	router.GET("/list/:id", d.ReadIntegrationsOfSite)
	router.GET("/sites/true/:id", d.ReadSitesOfProjectWithIntegrations)
	router.GET("/sites/false/:id", d.ReadSitesOfProjectWithoutIntegrations)

	router.GET("/amocrm/project/:id", d.ReadProjectAmocrmIntegration)
	router.GET("/amocrm/fields/:id/:page", d.ReadAmocrmFieldsList)
	router.GET("/amocrm/users/:id", d.ReadAmocrmUsersList)
	router.GET("/amocrm/pipelines/:id", d.ReadAmocrmPipelinesList)
	router.GET("/amocrm/statuses/:id/:pipelineId", d.ReadAmocrmStatusesList)
}

func (d *Delivery) routerDocs(router *gin.RouterGroup) {
	docs.SwaggerInfo.BasePath = "/"

	router.Any("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
