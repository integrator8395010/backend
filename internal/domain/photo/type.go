package photo

import (
	"time"

	"github.com/google/uuid"
)

type Photo struct {
	id         uuid.UUID
	kvizId     uuid.UUID
	photo      string
	createdAt  time.Time
	modifiedAt time.Time
}

func (c Photo) Id() uuid.UUID {
	return c.id
}

func (c Photo) KvizId() uuid.UUID {
	return c.kvizId
}

func (c Photo) Photo() string {
	return c.photo
}

func (c *Photo) SetPhoto(photo string) {
	c.photo = photo
}

func (c Photo) CreatedAt() time.Time {
	return c.createdAt
}

func (c Photo) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	kvizId uuid.UUID,
	photo string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Photo, error) {
	return &Photo{
		id:         id,
		kvizId:     kvizId,
		photo:      photo,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	kvizId uuid.UUID,
	photo string,
) (*Photo, error) {
	return &Photo{
		id:         uuid.New(),
		kvizId:     kvizId,
		photo:      photo,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
