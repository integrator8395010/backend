package project

import (
	"backend/internal/domain/project"
	"backend/pkg/tools"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateProject(ctx context.Context, project *project.Project, inputFileName string, fileContent []byte) (*project.Project, error) {
	fileName, err := tools.SaveFileToFileStorage(inputFileName, fileContent)
	if err != nil {
		return nil, err
	}

	project.SetPhoto(fileName)

	err = s.repository.CreateProject(ctx, project)
	if err != nil {
		s.logger.Error("error creating project: %s", err.Error())
		return nil, err
	}

	return project, nil
}

func (s *service) UpdateProject(ctx context.Context, id uuid.UUID, updateFn func(project *project.Project) (*project.Project, error)) (project *project.Project, err error) {
	project, err = s.repository.UpdateProject(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error updating project: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteProject(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.DeleteProject(ctx, id)
	if err != nil {
		s.logger.Error("error updating project: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadProjectList(ctx context.Context) (projectsList []*project.Project, err error) {
	projectsList, err = s.repository.ReadProjectList(ctx)
	if err != nil {
		s.logger.Error("error updating project: %s", err.Error())
		return
	}
	return
}
