package auth

type SignInRequest struct {
	Login string `json:"login"  binding:"required"`
	Pass  string `json:"pass"  binding:"required"`
}

type SignInResponse struct {
	Token        string `json:"token"  binding:"required"`
	RefreshToken string `json:"refreshToken"  binding:"required"`
}
