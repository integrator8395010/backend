package lead

import (
	"backend/internal/domain/lead"
	"backend/pkg/excel"
	"time"

	"context"
	"fmt"

	"github.com/google/uuid"
)

const (
	emailSender                = "leadactiv-krd@yandex.ru"
	spamSearchIpDuration       = time.Hour * 24 * 3 // 3 days
	spamSearchClientIdDuration = time.Hour * 24 * 7 // 7 days
)

func (s *service) CreateLead(ctx context.Context, lead *lead.Lead) (err error) {
	errSpam := s.checkSpam(ctx, lead)
	if errSpam != nil {
		s.logger.Error("error checking spam: %s", errSpam.Error())
	}

	err = s.repository.Lead.CreateLead(ctx, lead)
	if err != nil {
		s.logger.Error("error creating lead: %s", err.Error())
		return
	}
	return
}

func (s *service) UpdateLead(ctx context.Context, id uuid.UUID, updateFn func(lead *lead.Lead) (*lead.Lead, error)) (lead *lead.Lead, err error) {
	lead, err = s.repository.Lead.UpdateLead(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error creating lead: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteLead(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.Lead.DeleteLead(ctx, id)
	if err != nil {
		s.logger.Error("error creating lead: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadLeadList(ctx context.Context) (leadsList []*lead.Lead, err error) {
	leadsList, err = s.repository.Lead.ReadLeadList(ctx)
	if err != nil {
		s.logger.Error("error creating lead: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadFiltredLeadList(ctx context.Context, filter *lead.LeadsFilter, offset, limit int) (list []*lead.Lead, count int, err error) {
	list, count, err = s.repository.Lead.ReadFiltredLeadList(ctx, filter, offset, limit)
	if err != nil {
		s.logger.Error("error reading filtred list: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadSpamFiltredLeadList(ctx context.Context, filter *lead.SpamLeadsFilter) (list []*lead.Lead, err error) {
	list, err = s.repository.Lead.ReadSpamFiltredLeadList(ctx, filter)
	if err != nil {
		s.logger.Error("error reading spam filtred list: %s", err.Error())
		return
	}
	return
}

func (s *service) GetSpamLeadListExcel(ctx context.Context, filter *lead.SpamLeadsFilter) (fileName string, err error) {
	leads, err := s.repository.Lead.ReadSpamFiltredLeadList(ctx, filter)
	if err != nil {
		s.logger.Error("GetSpamLeadListExcel error reading spam filtred list excel: %s", err.Error())
		return
	}
	data := [][]string{{"Номер телефона", "Имя"}}
	for _, lead := range leads {
		data = append(data, []string{lead.Phone(), lead.Name()})
	}
	uid := uuid.New()
	fileName = "file-store/" + uid.String() + ".xlsx"
	excel := excel.NewExcelService()
	err = excel.CreateExcelFile(fileName, data)
	if err != nil {
		s.logger.Error("GetSpamLeadListExcel error creaeting file with spam list: %s", err.Error())
		return
	}
	return
}

func (s *service) GetLeadsStatisticsOfProject(ctx context.Context, projectId uuid.UUID, url string, from time.Time, to time.Time) (leadsStatistics *lead.LeadsStatistics, err error) {
	leadsStatistics, err = lead.NewLeadsStatistics(projectId, url, from, to)
	if err != nil {
		s.logger.Error("GetLeadsOfProject error: %s", err.Error())
		return
	}

	leadsList, err := s.repository.Lead.GetLeadsOfProject(ctx, projectId, url, from, to)
	if err != nil {
		s.logger.Error("GetLeadsOfProject error reading from repo: %s", err.Error())
		return
	}

	for _, leadInp := range leadsList {
		leadsStatistics.IncLeadsCount()

		day, err := leadsStatistics.GetDay(lead.CreateStartOfTheDay(leadInp.CreatedAt().Year(), int(leadInp.CreatedAt().Month()), leadInp.CreatedAt().Day()))
		if err != nil {
			s.logger.Error("GetLeadsOfProject error getting day from range: %s", err.Error())
			return nil, err
		}

		day.IncLeadsCount()
		if leadInp.Spam() {
			day.IncSpamLeadsCount()
			continue
		}

		switch lead.Source(leadInp.Source()) {
		case lead.Site:
			day.IncSiteLeadsCount()
		case lead.Flexbe:
			day.IncFlexbeLeadsCount()
		case lead.Marquiz:
			day.IncMarquizLeadsCount()
		case lead.Tilda:
			day.IncTildaLeadsCount()
		}
	}

	return
}

func (s *service) checkSpam(ctx context.Context, lead *lead.Lead) (err error) {
	if s.isInWhiteList(lead.Ip()) {
		return
	}
	leadsCountIp, err := s.repository.Lead.GetLeadsByIpCount(ctx, lead.Phone(), lead.Ip(), time.Now().Add(-spamSearchIpDuration))
	if err != nil {
		s.logger.Error(fmt.Sprintf("CheckSpam error get leads by ip: %s", err.Error()))
		return
	}

	leadsYandexClientCount, err := s.repository.Lead.GetLeadsByYandexClientIdCount(ctx, lead.YandexClientId(), time.Now().Add(-spamSearchClientIdDuration))
	if err != nil {
		s.logger.Error(fmt.Sprintf("CheckSpam error get leads by clientId: %s", err.Error()))
		return
	}

	if (leadsCountIp >= 2) || (leadsYandexClientCount >= 2) {
		lead.SetSpam(true)
		if leadsYandexClientCount >= 2 {
			s.logger.Info(fmt.Sprintf("spammer %s, phoneNumber - %s, site - %s by yandex clientId", lead.Ip(), lead.Phone(), lead.Url()))
		} else {
			s.logger.Info(fmt.Sprintf("spammer %s, phoneNumber - %s, site - %s by ip", lead.Ip(), lead.Phone(), lead.Url()))
		}
	}
	return
}

func generateMessageFromLead(lead *lead.Lead) string {
	message := fmt.Sprintf("<!DOCTYPE html><html lang=\"en\"><head>    <meta charset=\"UTF-8\">    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">    <title>Leadactiv</title></head><style>tr:nth-child(even) {background-color: #f2f2f2;} </style><body>    <table><tr><th>Получена заявка с %s </th></tr>", lead.Url())
	if lead.Name() != "" {
		message += fmt.Sprintf("<td><tr><td>Имя: %s</td></tr>", lead.Name())
	}

	if lead.Phone() != "" {
		message += fmt.Sprintf("<tr><td>Номер телефона:%s</td></tr>", lead.Phone())
	}

	if lead.Email() != "" {
		message += fmt.Sprintf("<tr><td>Email: %s</td></tr>", lead.Email())
	}

	if lead.Comment() != "" {
		message += fmt.Sprintf("<tr><td>Комментарий: %s</td></tr>", lead.Comment())
	}

	if lead.Ip() != "" {
		message += fmt.Sprintf("<tr><td>Ip: %s</td></tr>", lead.Ip())
	}

	if lead.UtmMedium() != "" {
		message += fmt.Sprintf("<tr><td>Utm Medium: %s</td></tr>", lead.UtmMedium())
	}

	if lead.UtmSource() != "" {
		message += fmt.Sprintf("<tr><td>Utm Source: %s</td></tr>", lead.UtmSource())
	}

	if lead.UtmCampaign() != "" {
		message += fmt.Sprintf("<tr><td>Utm Campaign: %s</td></tr>", lead.UtmCampaign())
	}

	if lead.UtmContent() != "" {
		message += fmt.Sprintf("<tr><td>Utm Content: %s</td></tr>", lead.UtmContent())
	}

	if lead.UtmTerm() != "" {
		message += fmt.Sprintf("<tr><td>Utm Term: %s</td></tr>", lead.UtmTerm())
	}

	message += fmt.Sprintf("<tr><td>Дата получения заявки: %s %s</td></tr>", lead.CreatedAt().Format("15:04"), lead.CreatedAt().Format("02.01.2006"))

	message += "</table></body></html>"
	return message
}

func (s *service) isInWhiteList(ip string) bool {
	flag := false
	for _, item := range s.whiteListIp {
		if item == ip {
			flag = true
			break
		}
	}
	return flag
}
