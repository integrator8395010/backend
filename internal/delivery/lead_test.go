package delivery

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseCookiesFromTildaRequest(t *testing.T) {
	req := require.New(t)
	tests := map[string]struct {
		inputString string
		want        map[string]string
		err         error
	}{
		"success": {inputString: "_ga%3DGA1.2.1861016115.1519204131%3B+_ym_uid%3D2021810468765220932%3BTILDAUTM%3Dutm_source%253Dyandex%257C%257C%257Cutm_medium%253Dcpc%257C%257C%257Cutm_campaign%253Dpromo%257C%257C%257Cutm_content%253Dblocktext%257C%257C%257Cutm_term%253Dpoisk%257C%257C%257C", want: map[string]string{" _ym_uid": "2021810468765220932", "_ga": "GA1.2.1861016115.1519204131", "utm_campaign": "promo", "utm_content": "blocktext", "utm_medium": "cpc", "utm_source": "yandex", "utm_term": "poisk"}, err: nil},
		"wrong":   {inputString: "_ga%3DGA1.2.1861016115.1519204131%3B+_ym_uid%3", want: map[string]string{}, err: errors.New("error string escaping")},
	}

	for name, testCase := range tests {
		t.Run(name, func(t *testing.T) {
			res, err := parseCookiesFromTildaRequest(testCase.inputString)
			if err != nil {
				req.ErrorContains(err, testCase.err.Error())
			} else {
				req.Empty(err)
			}
			req.Equal(testCase.want, res)
		})
	}
}
