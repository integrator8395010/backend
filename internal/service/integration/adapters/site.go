package repository

import (
	"backend/internal/domain/site"
	"context"

	"github.com/google/uuid"
)

type SiteRepository interface {
	ReadSitesOfProjectWithIntegrations(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error)
	ReadSitesOfProjectWithoutIntegrations(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error)
}
