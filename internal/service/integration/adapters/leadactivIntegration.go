package repository

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

type LeadactivIntegrationRepository interface {
	CreateLeadactivIntegration(ctx context.Context, integration *integration.LeadactivIntegration) (err error)
	UpdateLeadactivIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.LeadactivIntegration) (*integration.LeadactivIntegration, error)) (integration *integration.LeadactivIntegration, err error)
	DeleteLeadactivIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadLeadactivIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (leadactivIntegration *integration.LeadactivIntegration, err error)
}
