package integration

type CreateBitrixIntegrationRequest struct {
	SiteId        string               `json:"siteId"`
	Responsible   int                  `json:"responsible"`
	SendType      string               `json:"sendType"`
	Associations  []BitrixAssociation  `json:"associations"`
	DefaultValues []BitrixDefaultValue `json:"defaultValues"`
}

type BitrixAssociation struct {
	LeadField   string `json:"lead_field"`
	BitrixField string `json:"bitrix_field"`
}

type BitrixDefaultValue struct {
	Filed string      `json:"filed"`
	Value interface{} `json:"value"`
}

type UpdateBitrixIntegrationRequest struct {
	Id            string               `json:"id"`
	SiteId        string               `json:"siteId"`
	Responsible   int                  `json:"responsible"`
	SendType      string               `json:"sendType"`
	Associations  []BitrixAssociation  `json:"associations"`
	DefaultValues []BitrixDefaultValue `json:"defaultValues"`
}

type CreateProjectBitrixIntegration struct {
	ProjectId string `json:"projectId"`
	BaseUrl   string `json:"baseUrl"`
}

type UpdateProjectBitrixIntegrationRequest struct {
	Id        string `json:"id"`
	ProjectId string `json:"projectId"`
	BaseUrl   string `json:"baseUrl"`
}

type ProjectBitrixIntegrationResponse struct {
	ID         string `json:"id"`
	ProjectId  string `json:"projectId"`
	BaseUrl    string `json:"baseUrl"`
	CreatedAt  string `json:"createdAt"`
	ModifiedAt string `json:"modifiedAt"`
}

type BitrixIntegrationResponse struct {
	ID            string               `json:"id"`
	SiteId        string               `json:"siteId"`
	Responsible   int                  `json:"responsible"`
	SendType      string               `json:"sendType"`
	Associations  []BitrixAssociation  `json:"associations"`
	DefaultValues []BitrixDefaultValue `json:"defaultValues"`
	CreatedAt     string               `json:"createdAt"`
	ModifiedAt    string               `json:"modifiedAt"`
}
