package dao

import (
	"time"

	"github.com/google/uuid"
)

type AmocrmIntegration struct {
	Id            uuid.UUID             `db:"id"`
	SiteId        uuid.UUID             `db:"site_id"`
	PipelineId    int                   `db:"pipeline_id"`
	StatusId      int                   `db:"status_id"`
	Responsible   int                   `db:"responsible"`
	Unsorted      bool                  `db:"unsorted"`
	Associations  []map[string]int      `db:"associations"`
	DefaultValues []map[int]interface{} `db:"default_values"`
	CreatedAt     time.Time             `db:"created_at"`
	ModifiedAt    time.Time             `db:"modified_at"`
}

var ColumnsAmocrmIntegration = []string{
	"id",
	"site_id",
	"pipeline_id",
	"status_id",
	"responsible",
	"unsorted",
	"associations",
	"default_values",
	"created_at",
	"modified_at",
}
