package plan

import (
	"time"

	"github.com/google/uuid"
)

type Plan struct {
	id           uuid.UUID
	kvizId       uuid.UUID
	title        string
	photo        string
	rooms        int
	totalArea    float32
	livingArea   float32
	kitchenArea  float32
	bedRoomArea  float32
	bathRoomArea float32
	price        int
	createdAt    time.Time
	modifiedAt   time.Time
}

func (c Plan) Id() uuid.UUID {
	return c.id
}

func (c Plan) KvizId() uuid.UUID {
	return c.kvizId
}

func (c Plan) Title() string {
	return c.title
}

func (c Plan) Photo() string {
	return c.photo
}

func (c *Plan) SetPhoto(photo string) {
	c.photo = photo
}

func (c Plan) Rooms() int {
	return c.rooms
}

func (c Plan) TotalArea() float32 {
	return c.totalArea
}

func (c Plan) LivingArea() float32 {
	return c.livingArea
}

func (c Plan) KitchenArea() float32 {
	return c.kitchenArea
}

func (c Plan) BedRoomArea() float32 {
	return c.bedRoomArea
}

func (c Plan) BathRoomArea() float32 {
	return c.bathRoomArea
}

func (c Plan) Price() int {
	return c.price
}

func (c Plan) CreatedAt() time.Time {
	return c.createdAt
}

func (c Plan) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	kvizId uuid.UUID,
	title string,
	photo string,
	rooms int,
	totalArea float32,
	livingArea float32,
	kitchenArea float32,
	bedRoomArea float32,
	bathRoomArea float32,
	price int,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Plan, error) {
	return &Plan{
		id:           id,
		kvizId:       kvizId,
		title:        title,
		photo:        photo,
		rooms:        rooms,
		totalArea:    totalArea,
		livingArea:   livingArea,
		kitchenArea:  kitchenArea,
		bedRoomArea:  bedRoomArea,
		bathRoomArea: bathRoomArea,
		price:        price,
		createdAt:    createdAt,
		modifiedAt:   modifiedAt,
	}, nil
}

func New(
	kvizId uuid.UUID,
	title string,
	photo string,
	rooms int,
	totalArea float32,
	livingArea float32,
	kitchenArea float32,
	bedRoomArea float32,
	bathRoomArea float32,
	price int,
) (*Plan, error) {
	return &Plan{
		id:           uuid.New(),
		kvizId:       kvizId,
		title:        title,
		photo:        photo,
		rooms:        rooms,
		totalArea:    totalArea,
		livingArea:   livingArea,
		kitchenArea:  kitchenArea,
		bedRoomArea:  bedRoomArea,
		bathRoomArea: bathRoomArea,
		price:        price,
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}
