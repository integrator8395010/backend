package integration

import (
	repository "backend/internal/repository"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository *repository.Repository
	logger     logger.Interface
	baseUrl    string
	options    Options
}

type Options struct {
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repository.Repository, baseUrl string, logger logger.Interface, options Options) (*service, error) {
	service := &service{
		repository: repository,
		logger:     logger,
		baseUrl:    baseUrl,
	}

	service.SetOptions(options)
	return service, nil
}
