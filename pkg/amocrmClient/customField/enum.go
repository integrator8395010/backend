package customField

type Enum struct {
	Value interface{} `json:"value"`
}

type EnumWithId struct {
	EnumId int         `json:"enum_id"`
	Value  interface{} `json:"value"`
}
