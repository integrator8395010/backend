package bitrixDefaultValue

type BitrixDefaultValue struct {
	field string
	value interface{}
}

func NewBitrixDefaultValue(field string, value interface{}) *BitrixDefaultValue {
	return &BitrixDefaultValue{
		field: field,
		value: value,
	}
}

func (b BitrixDefaultValue) Field() string {
	return b.field
}

func (b BitrixDefaultValue) Value() interface{} {
	return b.value
}
