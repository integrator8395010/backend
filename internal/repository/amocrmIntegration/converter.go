package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/amocrmDefaultValue"
	"backend/internal/domain/integration/amocrmFieldAssociation"
	"backend/internal/repository/amocrmIntegration/dao"
)

func (r *Repository) toDomainAmocrmIntegration(dao *dao.AmocrmIntegration) (*integration.AmocrmIntegration, error) {
	associationsList := []*amocrmFieldAssociation.AmocrmFieldAssociation{}
	for _, item := range dao.Associations {
		for key, value := range item {
			association := amocrmFieldAssociation.NewAmocrmFieldAssociation(key, value)
			associationsList = append(associationsList, association)
		}
	}

	defaultValuesList := []*amocrmDefaultValue.AmocrmDefaultValue{}
	for _, item := range dao.DefaultValues {
		for key, value := range item {
			defaultValue := amocrmDefaultValue.NewAmocrmDefaultValue(key, value)
			defaultValuesList = append(defaultValuesList, defaultValue)
		}
	}
	return integration.NewAmocrmIntegrationWithId(dao.Id, dao.SiteId, dao.PipelineId, dao.StatusId, dao.Responsible, dao.Unsorted, associationsList, defaultValuesList, dao.CreatedAt, dao.ModifiedAt)
}

func (r *Repository) toRepositoryDaoAmoCrmIntegration(integration *integration.AmocrmIntegration) (*dao.AmocrmIntegration, error) {
	associationsList := []map[string]int{}
	for _, association := range integration.Associations() {
		associationsList = append(associationsList, map[string]int{association.LeadField(): association.AmocrmField()})
	}

	defaultValuesList := []map[int]interface{}{}
	for _, item := range integration.DefaultValues() {
		defaultValuesList = append(defaultValuesList, map[int]interface{}{item.Field(): item.Value()})
	}
	return &dao.AmocrmIntegration{
		Id:            integration.Id(),
		SiteId:        integration.SiteId(),
		PipelineId:    integration.PipelineId(),
		StatusId:      integration.StatusId(),
		Responsible:   integration.Responsible(),
		Unsorted:      integration.IsUnsorted(),
		Associations:  associationsList,
		DefaultValues: defaultValuesList,
		CreatedAt:     integration.CreatedAt(),
		ModifiedAt:    integration.ModifiedAt(),
	}, nil
}
