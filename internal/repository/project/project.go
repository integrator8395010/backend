package repository

import (
	"backend/internal/domain/project"
	"backend/internal/repository/project/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "project"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"project_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateProject(ctx context.Context, project *project.Project) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsProject...).Values(project.Id(), project.Name(), project.Photo(), project.CreatedBy(), project.CreatedAt(), project.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateProject(ctx context.Context, id uuid.UUID, updateFn func(project *project.Project) (*project.Project, error)) (project *project.Project, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upProject, err := r.oneProjectTx(ctx, tx, id)
	if err != nil {
		return
	}

	projectNew, err := updateFn(upProject)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", projectNew.Name()).Set("photo", projectNew.Photo()).Set("created_by", projectNew.CreatedBy()).Set("modified_at", projectNew.ModifiedAt()).Where("id = ?", projectNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return projectNew, nil
}

func (r *Repository) DeleteProject(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) ReadProjectList(ctx context.Context) (projectsList []*project.Project, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsProject...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProject, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Project])
	if err != nil {
		return nil, err
	}

	projectsList = []*project.Project{}

	for _, project := range daoProject {
		projectIns, err := r.toDomainProject(&project)
		if err != nil {
			return nil, err
		}

		projectsList = append(projectsList, projectIns)
	}

	return
}

func (r *Repository) oneProjectTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *project.Project, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsProject...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProject, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Project])
	if err != nil {
		return nil, err
	}

	return r.toDomainProject(&daoProject)
}
