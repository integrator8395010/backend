package lead

import (
	"time"

	"github.com/google/uuid"
)

type CreateLeadFromSiteRequest struct {
	Name           string `json:"name" form:"name"`
	Phone          string `json:"phone" form:"phone" example:"+7 (900) 000-00-00" binding:"required"`
	Email          string `json:"email" form:"email"`
	UtmMedium      string `json:"utm_medium" form:"utm_medium"`
	UtmContent     string `json:"utm_content" form:"utm_content"`
	UtmTerm        string `json:"utm_term" form:"utm_term"`
	UtmSource      string `json:"utm_source" form:"utm_source"`
	UtmCampaign    string `json:"utm_campaign" form:"utm_campaign"`
	Comment        string `json:"text" form:"text"`
	Yclid          string `json:"yclid" form:"yclid"`
	Gclid          string `json:"gclid" form:"gclid"`
	Fbid           string `json:"fbid" form:"fbid"`
	YandexClientId int64  `json:"yandexClientId" form:"yandexClientId"`
}

type CreateLeadFromTildaRequest struct {
	Name        string `form:"Name"`
	Email       string `form:"Email" example:"Иван"`
	Phone       string `form:"Phone" example:"+7 (900) 000-00-00" binding:"required"`
	Comments    string `form:"Comments"`
	Cookies     string `form:"Cookies"`
	UtmMedium   string `json:"utm_medium" form:"utm_medium"`
	UtmContent  string `json:"utm_content" form:"utm_content"`
	UtmTerm     string `json:"utm_term" form:"utm_term"`
	UtmSource   string `json:"utm_source" form:"utm_source"`
	UtmCampaign string `json:"utm_campaign" form:"utm_campaign"`
}

type CreateLeadFromSiteResponse struct {
	Message string `json:"message"`
}

type LeadResponse struct {
	Id             uuid.UUID `json:"id"`
	Url            string    `json:"url"`
	SiteId         string    `json:"siteId"`
	Name           string    `json:"name"`
	Phone          string    `json:"phone"`
	Email          string    `json:"email"`
	Comment        string    `json:"comment"`
	Roistat        int       `json:"roistat"`
	Ip             string    `json:"ip"`
	UserAgent      string    `json:"userAgent"`
	UtmMedium      string    `json:"utmMedium"`
	UtmContent     string    `json:"utmContent"`
	UtmTerm        string    `json:"utmTerm"`
	UtmSource      string    `json:"utmSource"`
	UtmCampaign    string    `json:"utmCampaign"`
	Yclid          string    `json:"yclid"`
	Gclid          string    `json:"gclid"`
	FbId           string    `json:"fbid"`
	Source         string    `json:"source"`
	Spam           bool      `json:"spam"`
	YandexClientId int64     `json:"yandexCLientId"`
	CreatedAt      string    `json:"createdAt"`
	ModifiedAt     string    `json:"modifiedAt"`
}

type ReadLeadsFiltersListRequest struct {
	Filter LeadsFilter `json:"filter"`
	Offset int         `json:"offset"`
	Limit  int         `json:"limit"`
}

type ReadLeadsFiltredListResponse struct {
	Leads      []*LeadResponse `json:"leads"`
	TotalItems int             `json:"totalItems"`
}

type LeadsFilter struct {
	ProjectId uuid.UUID `json:"projectId"`
	Url       string    `json:"url"`
	Phone     string    `json:"phone"`
	Source    string    `json:"source"`
	From      time.Time `json:"from" binding:"required" example:"2023-01-17T00:00:00Z"`
	To        time.Time `json:"to" binding:"required" example:"2023-01-17T00:00:00Z"`
}

type ReadSpamLeadsFiltredList struct {
	Filter LeadsSpamFilter `json:"filter" binding:"required"`
}

type ReadSpamLeadsFiltredListResponse struct {
	Leads []*LeadResponse `json:"leads"`
}

type LeadsSpamFilter struct {
	ProjectId uuid.UUID `json:"projectId" binding:"required"`
	Url       string    `json:"url"`
	From      time.Time `json:"from" binding:"required" example:"2023-01-17T00:00:00Z"`
	To        time.Time `json:"to" binding:"required" example:"2023-01-17T00:00:00Z"`
}
