package scheduler

import (
	"backend/internal/service"
	"context"
	"fmt"
	"time"

	"github.com/go-co-op/gocron"
	"gitlab.com/kanya384/gotools/logger"
)

const (
	dailyUpdateInterval         = time.Hour * 24
	defaultSleepInterval        = time.Second * 10
	sleepIntervalOnError        = time.Minute * 10
	sendLeadsPauseOnEmpty       = time.Second * 30
	emailFrom                   = "leadactiv-krd@yandex.ru"
	amoProjectTokensUpdateTime  = "23:10"
	amoLeadactivTokenUpdateTime = "23:20"
)

type scheduler struct {
	service *service.Service
	logger  logger.Interface
}

func NewScheduler(service *service.Service, logger logger.Interface) (*scheduler, error) {
	return &scheduler{
		logger:  logger,
		service: service,
	}, nil
}

func (s *scheduler) Run() {
	s.mailSender(context.Background())
	s.bitrixSender(context.Background())
	s.amoSender(context.Background())
	s.leadactivSender(context.Background())

	cron := gocron.NewScheduler(time.Local)

	cron.Every(1).Day().At(amoProjectTokensUpdateTime).Do(func() {
		s.amoTokensRefresh(context.Background())
	})

	cron.Every(1).Day().At(amoLeadactivTokenUpdateTime).Do(func() {
		s.leadactivTokensRefresh(context.Background())
	})
	cron.StartAsync()
	fmt.Println(time.Now())
}

func (s *scheduler) mailSender(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.service.Lead.ProcessLeadsToEmail(ctx)
			if err != nil {
				s.logger.Error("error sending mail in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(defaultSleepInterval)
		}
	}(ctx, s)
}

func (s *scheduler) bitrixSender(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.service.Lead.ProcessLeadsToBitrix(ctx)
			if err != nil {
				s.logger.Error("error sending lead to bitrix in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(defaultSleepInterval)
		}
	}(ctx, s)
}

func (s *scheduler) amoSender(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.service.Lead.ProcessLeadsToAmocrm(ctx)
			if err != nil {
				s.logger.Error("error sending leads to amocrm in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(defaultSleepInterval)
		}
	}(ctx, s)
}

func (s *scheduler) leadactivSender(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.service.Lead.ProcessLeadsToLeadactiv(ctx)
			if err != nil {
				s.logger.Error("error sending leads to leadactiv in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(defaultSleepInterval)
		}
	}(ctx, s)
}

func (s *scheduler) amoTokensRefresh(ctx context.Context) {
	err := s.service.Integration.RefreshProjectIntegrationsTokens(ctx)
	if err != nil {
		s.logger.Error("error refreshing project integration tokens in scheduler: %s", err.Error())
	}
}

func (s *scheduler) leadactivTokensRefresh(ctx context.Context) {
	err := s.service.Integration.RefreshLeadactivTokens(ctx)
	if err != nil {
		s.logger.Error("error refreshing leadactiv integration tokens in scheduler: %s", err.Error())
	}
}
