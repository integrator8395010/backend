package delivery

import (
	jsonPlan "backend/internal/delivery/plan"
	"backend/internal/domain/plan"
	"context"
	"io"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CratePlan
// @Summary Создание плана квартиры для сайта.
// @Description Создание плана квартиры для сайта.
// @Tags plan
// @Accept multipart/form-data
// @Produce json
// @Param data formData jsonPlan.CreatePlanRequest true "Body"
// @Param file formData file true "Photo of plan"
// @Success 201			{object} 	jsonPlan.PlanResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /plan/ [put]
func (d *Delivery) CreatePlan(c *gin.Context) {
	request := jsonPlan.CreatePlanRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	plan, err := plan.New(kvizId, request.Title, "", request.Rooms, request.TotalArea, request.LivingArea, request.KitchenArea, request.BedRoomArea, request.BathRoomArea, request.Price)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var photoReader multipart.File
	photoReader, err = photo.Open()
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	defer photoReader.Close()

	photoContent, err := io.ReadAll(photoReader)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	planResp, err := d.services.Plan.CreatePlan(context.Background(), plan, photo.Filename, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonPlan.ToPlanResponse(planResp))
}

// UpdatePlan
// @Summary Обновление плана квартиры.
// @Description Обновление плана квартиры.
// @Tags plan
// @Accept json
// @Produce json
// @Param data body jsonPlan.UpdatePlanRequest true "Body"
// @Success 200			{object} 	jsonPlan.PlanResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /plan/ [post]
func (d *Delivery) UpdatePlan(c *gin.Context) {
	request := jsonPlan.UpdatePlanRequest{}
	if err := c.ShouldBindJSON(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.ID)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	planNew, err := plan.NewWithId(id, kvizId, request.Title, "", request.Rooms, request.TotalArea, request.LivingArea, request.KitchenArea, request.BedRoomArea, request.BathRoomArea, request.Price, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var photoReader multipart.File
	photoReader, err = photo.Open()
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	defer photoReader.Close()

	photoContent, err := io.ReadAll(photoReader)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	planResp, err := d.services.Plan.UpdatePlan(context.Background(), planNew, photo.Filename, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonPlan.ToPlanResponse(planResp))
}

// DeletePlan
// @Summary Удаление плана квартиры.
// @Description Удаление плана квартиры.
// @Tags plan
// @Produce json
// @Param id path string true "Plan ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /plan/{id} [delete]
func (d *Delivery) DeletePlan(c *gin.Context) {
	id := c.Param("id")
	planId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Plan.DeletePlan(context.Background(), planId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadPlanList
// @Summary Список планировок.
// @Description Список планировок.
// @Tags plan
// @Produce json
// @Success 200			{object} 	jsonPlan.PlanListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /plan/ [get]
func (d *Delivery) ReadPlanList(c *gin.Context) {
	planList, err := d.services.Plan.ReadPlanList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	planListResponse := make([]*jsonPlan.PlanResponse, len(planList))

	for index, plan := range planList {
		planListResponse[index] = jsonPlan.ToPlanResponse(plan)
	}

	c.JSON(http.StatusOK, jsonPlan.PlanListResponse{Plans: planListResponse})
}

// ReadPlansOfKviz
// @Summary Список планировок.
// @Description Список планировок.
// @Tags plan
// @Produce json
// @Param id path string true "Plan ID"
// @Success 200			{object} 	jsonPlan.PlanListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /plan/{id} [get]
func (d *Delivery) ReadPlansOfKviz(c *gin.Context) {
	id := c.Param("id")
	planId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	planList, err := d.services.Plan.ReadPlanListOfKviz(context.Background(), planId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	planListResponse := make([]*jsonPlan.PlanResponse, len(planList))

	for index, plan := range planList {
		planListResponse[index] = jsonPlan.ToPlanResponse(plan)
	}

	c.JSON(http.StatusOK, jsonPlan.PlanListResponse{Plans: planListResponse})
}
