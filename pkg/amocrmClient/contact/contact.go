package contact

type Contact struct {
	FirstName         string        `json:"first_name"`
	CustomFieldValues []interface{} `json:"custom_fields_values"`
}
