package repository

import (
	"backend/internal/domain/user"
	"backend/internal/repository/user/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "public.user"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"user_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateUser(ctx context.Context, user *user.User) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsUser...).Values(user.Id(), user.Name(), user.Login(), user.Pass(), user.Role(), user.CreatedAt(), user.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) FindUserByCredetinals(ctx context.Context, login, pass string) (user *user.User, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsUser...).From(tableName).Where("login = ? and pass = ?", login, pass)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	daoUser, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.User])
	if err != nil {
		return nil, err
	}

	return r.toDomainUser(&daoUser)
}

func (r *Repository) UpdateUser(ctx context.Context, Id uuid.UUID, updateFn func(user *user.User) (*user.User, error)) (user *user.User, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upUser, err := r.oneUserTx(ctx, tx, Id)
	if err != nil {
		return
	}

	userNew, err := updateFn(upUser)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", userNew.Name()).Set("login", userNew.Login()).Set("pass", userNew.Pass()).Set("role", userNew.Role()).Set("modified_at", userNew.ModifiedAt()).Where("id = ?", userNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return

}

func (r *Repository) DeleteUser(ctx context.Context, ID uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) ReadUserList(ctx context.Context) (statuses []*user.User, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsUser...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoUser, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.User])
	if err != nil {
		return nil, err
	}

	statuses = []*user.User{}

	for _, status := range daoUser {
		statusIns, err := r.toDomainUser(&status)
		if err != nil {
			return nil, err
		}

		statuses = append(statuses, statusIns)
	}

	return

}

func (r *Repository) oneUserTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *user.User, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsUser...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoUser, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.User])
	if err != nil {
		return nil, err
	}

	return r.toDomainUser(&daoUser)
}
