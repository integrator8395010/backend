package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/leadactivAmoSecrets/dao"
)

func (r *Repository) toDomainAmocrmIntegration(dao *dao.LeadactivAmoSecrets) (*integration.LeadactivAmoSecrets, error) {
	return integration.NewLeadactivAmoSecretsWithId(dao.Id, dao.BaseUrl, dao.ClientId, dao.ClientSecret, dao.RedirectUri, dao.Token, dao.RefreshToken, dao.CreatedAt, dao.ModifiedAt)
}

func (r *Repository) toRepositoryDaoAmoSecrets(integration *integration.LeadactivAmoSecrets) (*dao.LeadactivAmoSecrets, error) {
	return &dao.LeadactivAmoSecrets{
		Id:           integration.Id(),
		BaseUrl:      integration.BaseUrl(),
		ClientId:     integration.ClientId(),
		ClientSecret: integration.ClientSecret(),
		RedirectUri:  integration.RedirectUri(),
		Token:        integration.Token(),
		RefreshToken: integration.RefreshToken(),
		CreatedAt:    integration.CreatedAt(),
		ModifiedAt:   integration.ModifiedAt(),
	}, nil
}
