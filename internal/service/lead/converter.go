package lead

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/lead"
	"backend/pkg/amocrmClient"
	amoContact "backend/pkg/amocrmClient/contact"
	amoContactCustomField "backend/pkg/amocrmClient/contact/customField"
	"backend/pkg/amocrmClient/customField"
	amoEmbeeded "backend/pkg/amocrmClient/embeeded"
	amoLead "backend/pkg/amocrmClient/lead"
	"fmt"
)

const (
	leadactivIntegrationStatusId   = 46697913
	leadactivIntegrationPipelineId = 1188
)

func (ls *service) toAmoApiLead(leadIndex int, lead *lead.Lead, integration *integration.AmocrmIntegration) (resLead *amoLead.Lead, err error) {
	contact := &amoContact.Contact{
		FirstName: lead.Name(),
	}
	contactPhone := amoContactCustomField.CustomField{
		FieldCode: "PHONE",
		Values: []amoContactCustomField.Enum{
			{
				EnumCode: "WORK",
				Value:    lead.Phone(),
			},
		},
	}
	contact.CustomFieldValues = append(contact.CustomFieldValues, contactPhone)

	leadsEmbeeded := &amoEmbeeded.Embedded{
		Contacts: []*amoContact.Contact{contact},
	}

	leadsCustomFields := []*customField.CustomField{}

	for _, association := range integration.Associations() {

		value, err := lead.FieldByName(association.LeadField())
		if err != nil {
			return nil, fmt.Errorf("error getting field by name (%s): %s", association.LeadField(), err.Error())
		}

		if value != "" && value != 0 && value != nil {
			customField := &customField.CustomField{
				FieldId: association.AmocrmField(),
				Values: []customField.Enum{
					{
						Value: value,
					},
				},
			}
			leadsCustomFields = append(leadsCustomFields, customField)
		}
	}

	for _, defaultValue := range integration.DefaultValues() {

		resField := &customField.CustomField{
			FieldId: defaultValue.Field(),
		}
		switch defaultValue.Value().(type) {
		case map[string]interface{}:
			item := defaultValue.Value().(map[string]interface{})
			resField.Values = []customField.EnumWithId{{
				EnumId: int(item["enum_id"].(float64)),
				Value:  item["value"],
			},
			}
		default:
			resField.Values = []customField.Enum{{
				Value: defaultValue.Value(),
			},
			}
		}
		leadsCustomFields = append(leadsCustomFields, resField)
	}

	comment := lead.Comment()

	leadR := &amoLead.Lead{
		Id:                leadIndex,
		Name:              "Сделка",
		ResponsibleUserId: integration.Responsible(),
		StatusId:          integration.StatusId(),
		PipelineId:        integration.PipelineId(),
		CreatedBy:         integration.Responsible(),
		Embedded:          leadsEmbeeded,
		Text:              comment,
		CustomFileds:      leadsCustomFields,
	}
	return leadR, nil
}

func (ls *service) toAmoApiUnsorted(leadIndex int, lead *lead.Lead, integration *integration.AmocrmIntegration) (unsortedRes *amocrmClient.UnsortedEmbeeded, err error) {
	contact := &amoContact.Contact{
		FirstName: lead.Name(),
	}
	contactPhone := amoContactCustomField.CustomField{
		FieldCode: "PHONE",
		Values: []amoContactCustomField.Enum{
			{
				EnumCode: "WORK",
				Value:    lead.Phone(),
			},
		},
	}
	contact.CustomFieldValues = append(contact.CustomFieldValues, contactPhone)

	leadsEmbeeded := &amoEmbeeded.Embedded{}

	leadsCustomFields := []*customField.CustomField{}

	for _, association := range integration.Associations() {

		value, err := lead.FieldByName(association.LeadField())
		if err != nil {
			return nil, fmt.Errorf("error getting field by name (%s): %s", association.LeadField(), err.Error())
		}

		if value != "" && value != 0 && value != nil {
			customField := &customField.CustomField{
				FieldId: association.AmocrmField(),
				Values: []customField.Enum{
					{
						Value: value,
					},
				},
			}
			leadsCustomFields = append(leadsCustomFields, customField)
		}
	}

	for _, defaultValue := range integration.DefaultValues() {
		resField := &customField.CustomField{
			FieldId: defaultValue.Field(),
		}
		switch defaultValue.Value().(type) {
		case map[string]interface{}:
			item := defaultValue.Value().(map[string]interface{})
			resField.Values = []customField.EnumWithId{{
				EnumId: int(item["enum_id"].(float64)),
				Value:  item["value"],
			},
			}
		default:
			resField.Values = []customField.Enum{{
				Value: defaultValue.Value(),
			},
			}
		}
		leadsCustomFields = append(leadsCustomFields, resField)
	}

	comment := lead.Comment()

	leadUnsorted := &amoLead.LeadUnsorted{
		Id:                leadIndex,
		Name:              "Сделка",
		ResponsibleUserId: integration.Responsible(),
		PipelineId:        integration.PipelineId(),
		CreatedBy:         integration.Responsible(),
		Embedded:          leadsEmbeeded,
		Text:              comment,
		CustomFileds:      leadsCustomFields,
	}

	unsortedRes = &amocrmClient.UnsortedEmbeeded{
		Leads: []*amoLead.LeadUnsorted{leadUnsorted},
		Contacts: []*amoContact.Contact{
			contact,
		},
	}
	return unsortedRes, nil
}

func (ls *service) toAmoApiLeadactiv(leadIndex int, lead *lead.Lead, integration *integration.LeadactivIntegration) (resLead *amoLead.Lead, err error) {
	contact := &amoContact.Contact{
		FirstName: lead.Name(),
	}
	contactPhone := amoContactCustomField.CustomField{
		FieldCode: "PHONE",
		Values: []amoContactCustomField.Enum{
			{
				EnumCode: "WORK",
				Value:    lead.Phone(),
			},
		},
	}

	landing := amoContactCustomField.CustomFieldValue{
		FieldId: 982894,
		Values: []amoContactCustomField.EnumWithoutCode{
			{
				Value: lead.Url(),
			},
		},
	}
	contact.CustomFieldValues = append(contact.CustomFieldValues, contactPhone, landing)

	leadsEmbeeded := &amoEmbeeded.Embedded{
		Contacts: []*amoContact.Contact{contact},
	}

	leadsCustomFields := []*customField.CustomField{}

	if lead.UtmSource() != "" {
		customField := &customField.CustomField{
			FieldId: 1306894,
			Values: []customField.Enum{
				{
					Value: lead.UtmSource(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, customField)
	}

	if lead.UtmCampaign() != "" {
		customField := &customField.CustomField{
			FieldId: 1306896,
			Values: []customField.Enum{
				{
					Value: lead.UtmCampaign(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, customField)
	}

	if lead.UtmTerm() != "" {
		customField := &customField.CustomField{
			FieldId: 1306898,
			Values: []customField.Enum{
				{
					Value: lead.UtmTerm(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, customField)
	}

	if lead.UtmMedium() != "" {
		customField := &customField.CustomField{
			FieldId: 1306900,
			Values: []customField.Enum{
				{
					Value: lead.UtmMedium(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, customField)
	}

	if lead.Roistat() != 0 {
		customField := &customField.CustomField{
			FieldId: 1283206,
			Values: []customField.Enum{
				{
					Value: lead.UtmMedium(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, customField)
	}

	if len(leadsCustomFields) == 0 {
		leadsCustomFields = nil
	}

	comment := lead.Comment()

	leadR := &amoLead.Lead{
		Id:                leadIndex,
		Name:              "Сделка",
		ResponsibleUserId: integration.Responsible(),
		StatusId:          leadactivIntegrationStatusId,
		PipelineId:        leadactivIntegrationPipelineId,
		CreatedBy:         integration.Responsible(),
		Embedded:          leadsEmbeeded,
		Text:              comment,
		CustomFileds:      leadsCustomFields,
	}
	return leadR, nil
}
