package lead

import (
	"time"

	"github.com/google/uuid"
)

type LeadsSendLog struct {
	id              uuid.UUID
	leadId          uuid.UUID
	integrationId   uuid.UUID
	integrationType IntegrationType
	responseId      int
	source          Source
	comment         string
	sended          bool
	createdAt       time.Time
	modifiedAt      time.Time
}

func NewLeadsSendLogWithId(
	id uuid.UUID,
	leadId uuid.UUID,
	integrationId uuid.UUID,
	integrationType IntegrationType,
	responseId int,
	source Source,
	comment string,
	sended bool,
	createdAt time.Time,
	modifiedAt time.Time,
) (*LeadsSendLog, error) {
	return &LeadsSendLog{
		id:              id,
		leadId:          leadId,
		integrationId:   integrationId,
		integrationType: integrationType,
		responseId:      responseId,
		source:          source,
		comment:         comment,
		sended:          sended,
		createdAt:       createdAt,
		modifiedAt:      modifiedAt,
	}, nil
}

func NewLeadsSendLog(
	leadId uuid.UUID,
	integrationId uuid.UUID,
	integrationType IntegrationType,
	responseId int,
	source Source,
	comment string,
	sended bool,
) (*LeadsSendLog, error) {
	return &LeadsSendLog{
		id:              uuid.New(),
		leadId:          leadId,
		integrationId:   integrationId,
		integrationType: integrationType,
		responseId:      responseId,
		source:          source,
		comment:         comment,
		sended:          sended,
		createdAt:       time.Now(),
		modifiedAt:      time.Now(),
	}, nil
}

func (l LeadsSendLog) Id() uuid.UUID {
	return l.id
}

func (l LeadsSendLog) LeadId() uuid.UUID {
	return l.leadId
}

func (l LeadsSendLog) IntegrationId() uuid.UUID {
	return l.integrationId
}

func (l LeadsSendLog) IntegrationType() IntegrationType {
	return l.integrationType
}

func (l LeadsSendLog) ResponseId() int {
	return l.responseId
}

func (l LeadsSendLog) Source() Source {
	return l.source
}

func (l LeadsSendLog) Comment() string {
	return l.comment
}

func (l LeadsSendLog) Sended() bool {
	return l.sended
}

func (l LeadsSendLog) CreatedAt() time.Time {
	return l.createdAt
}

func (l LeadsSendLog) ModifiedAt() time.Time {
	return l.modifiedAt
}
