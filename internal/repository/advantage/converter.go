package repository

import (
	"backend/internal/domain/advantage"
	"backend/internal/repository/advantage/dao"
)

func (r *Repository) toDomainAdvantage(dao *dao.Advantage) (*advantage.Advantage, error) {
	return advantage.NewWithId(dao.Id, dao.KvizId, dao.Title, dao.Photo, dao.CreatedAt, dao.ModifiedAt)
}
