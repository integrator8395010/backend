package integration

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/site"
	"context"

	"github.com/google/uuid"
)

type Integration interface {
	CreateMailIntegration(ctx context.Context, integration *integration.MailIntegration) (err error)
	UpdateMailIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.MailIntegration) (*integration.MailIntegration, error)) (integration *integration.MailIntegration, err error)
	DeleteMailIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadMailIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.MailIntegration, err error)

	ReadProjectAmocrmIntegration(ctx context.Context, projectId uuid.UUID) (*integration.ProjectAmoIntegration, error)
	ProcessAmocrmSecrets(ctx context.Context, projectId uuid.UUID, clientId, clientSecret string) (*integration.ProjectAmoIntegration, error)
	ProcessAmocrmTokenRequestWithAuthorizationCode(ctx context.Context, siteId uuid.UUID, authCode, referer string) (*integration.ProjectAmoIntegration, error)
	CreateAmocrmIntegration(ctx context.Context, integration *integration.AmocrmIntegration) (err error)
	UpdateAmocrmIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.AmocrmIntegration) (*integration.AmocrmIntegration, error)) (integration *integration.AmocrmIntegration, err error)
	DeleteAmocrmIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadAmocrmIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integratiom *integration.AmocrmIntegration, err error)
	ReadAmocrmFieldsList(ctx context.Context, id uuid.UUID, page int) (list interface{}, err error)
	ReadAmocrmUsersList(ctx context.Context, id uuid.UUID) (list interface{}, err error)
	ReadAmocrmPipelinesList(ctx context.Context, id uuid.UUID) (list interface{}, err error)
	ReadAmocrmStatusesOfPipelineList(ctx context.Context, id uuid.UUID, pipelineId int) (list interface{}, err error)
	RefreshProjectIntegrationsTokens(ctx context.Context) error

	CreateProjectBitrixIntegration(ctx context.Context, integration *integration.ProjectBitrixIntegration) (err error)
	UpdateProjectBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.ProjectBitrixIntegration) (*integration.ProjectBitrixIntegration, error)) (integration *integration.ProjectBitrixIntegration, err error)
	ReadProjectBitrixIntegration(ctx context.Context, projectId uuid.UUID) (integration *integration.ProjectBitrixIntegration, err error)
	DeleteProjectBitrixIntegration(ctx context.Context, id uuid.UUID) (err error)

	CreateBitrixIntegration(ctx context.Context, integration *integration.BitrixIntegration) (err error)
	UpdateBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.BitrixIntegration) (*integration.BitrixIntegration, error)) (integration *integration.BitrixIntegration, err error)
	DeleteBitrixIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadBitrixIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.BitrixIntegration, err error)

	ReadIntegrationsOfSite(ctx context.Context, siteId uuid.UUID) (amocrm *integration.AmocrmIntegration, bitrix *integration.BitrixIntegration, mail *integration.MailIntegration, leadactiv *integration.LeadactivIntegration, err error)
	ReadSitesOfProjectWithIntegrations(ctx context.Context, projectId uuid.UUID) ([]*site.Site, error)
	ReadSitesOfProjectWithoutIntegrations(ctx context.Context, projectId uuid.UUID) ([]*site.Site, error)

	ReadLeadactivAmoSecrets(ctx context.Context) (*integration.LeadactivAmoSecrets, error)
	ProcessLeadactivAmocrmSecrets(ctx context.Context, clientId, clientSecret string) (*integration.LeadactivAmoSecrets, error)
	ProcessLeadactivAmocrmTokenRequestWithAuthorizationCode(ctx context.Context, authCode, referer string) (*integration.LeadactivAmoSecrets, error)
	ReadLeadactivUsersList(ctx context.Context, page int) (list interface{}, err error)
	RefreshLeadactivTokens(ctx context.Context) error

	CreateLeadactivIntegration(ctx context.Context, integration *integration.LeadactivIntegration) (err error)
	UpdateLeadactivIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.LeadactivIntegration) (*integration.LeadactivIntegration, error)) (integration *integration.LeadactivIntegration, err error)
	DeleteLeadactivIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadLeadactivIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (leadactivIntegration *integration.LeadactivIntegration, err error)
}
