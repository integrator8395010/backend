package repository

import (
	"backend/internal/domain/plan"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreatePlan(ctx context.Context, plan *plan.Plan) (err error)
	UpdatePlan(ctx context.Context, id uuid.UUID, updateFn func(plan *plan.Plan) (*plan.Plan, error)) (plan *plan.Plan, err error)
	DeletePlan(ctx context.Context, id uuid.UUID) (err error)
	ReadPlanList(ctx context.Context) (plansList []*plan.Plan, err error)
	ReadPlanListOfKviz(ctx context.Context, kvizId uuid.UUID) (plansList []*plan.Plan, err error)
}
