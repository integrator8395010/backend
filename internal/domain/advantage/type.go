package advantage

import (
	"time"

	"github.com/google/uuid"
)

type Advantage struct {
	id         uuid.UUID
	kvizId     uuid.UUID
	title      string
	photo      string
	createdAt  time.Time
	modifiedAt time.Time
}

func (c Advantage) Id() uuid.UUID {
	return c.id
}

func (c Advantage) KvizId() uuid.UUID {
	return c.kvizId
}

func (c Advantage) Title() string {
	return c.title
}

func (c Advantage) Photo() string {
	return c.photo
}

func (c *Advantage) SetPhoto(photo string) {
	c.photo = photo
}

func (c Advantage) CreatedAt() time.Time {
	return c.createdAt
}

func (c Advantage) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	kvizId uuid.UUID,
	title string,
	photo string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Advantage, error) {
	return &Advantage{
		id:         id,
		kvizId:     kvizId,
		title:      title,
		photo:      photo,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	kvizId uuid.UUID,
	title string,
	photo string,
) (*Advantage, error) {
	return &Advantage{
		id:         uuid.New(),
		kvizId:     kvizId,
		title:      title,
		photo:      photo,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
