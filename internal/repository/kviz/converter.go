package repository

import (
	"backend/internal/domain/kviz"
	"backend/internal/repository/kviz/dao"
)

func (r *Repository) toDomainKviz(dao *dao.Kviz) (*kviz.Kviz, error) {
	return kviz.NewWithId(dao.Id, dao.SiteId, dao.Name, dao.TemplateId, dao.Background, dao.MainColor, dao.SecondaryColor, dao.SubTitle, dao.SubTitleItems, dao.PhoneStepTitle, dao.FooterTitle, dao.Phone, dao.Politics, dao.Roistat, dao.AdvantageTitle, dao.PhotosTitle, dao.PlansTitle, dao.ResultStepText, dao.Qoopler, dao.DmpOne, dao.ValidatePhone, dao.CreatedBy, dao.CreatedAt, dao.ModifiedAt) // pass is empty for security reasons
}
