package repository

import (
	"backend/internal/domain/site"
	"backend/internal/repository/site/dao"
)

func (r *Repository) toDomainSite(dao *dao.Site) (*site.Site, error) {
	return site.NewWithId(dao.Id, dao.Name, dao.Url, dao.ProjectId, dao.CreatedBy, dao.CreatedAt, dao.ModifiedAt)
}
