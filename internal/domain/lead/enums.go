package lead

type Source string

const (
	Site    Source = "site"
	Marquiz Source = "marquiz"
	Tilda   Source = "tilda"
	Flexbe  Source = "flexbe"
)

type IntegrationType string

const (
	Mail      IntegrationType = "mail"
	Amo       IntegrationType = "amo"
	Bitrix    IntegrationType = "bitrix"
	Leadactiv IntegrationType = "leadactiv"
)
