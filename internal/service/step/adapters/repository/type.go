package repository

import (
	"backend/internal/domain/step"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreateStep(ctx context.Context, step *step.Step) (err error)
	UpdateStep(ctx context.Context, id uuid.UUID, updateFn func(step *step.Step) (*step.Step, error)) (step *step.Step, err error)
	DeleteStep(ctx context.Context, id uuid.UUID) (err error)
	ReadStepList(ctx context.Context) (stepsList []*step.Step, err error)
}
