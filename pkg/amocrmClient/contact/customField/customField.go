package customField

type CustomField struct {
	//FieldId   int    `json:"field_id"`
	FieldCode string `json:"field_code"`
	Values    []Enum `json:"values"`
}

type CustomFieldValue struct {
	FieldId int               `json:"field_id"`
	Values  []EnumWithoutCode `json:"values"`
}
