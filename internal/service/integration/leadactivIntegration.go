package integration

import (
	"backend/internal/domain/integration"
	"backend/pkg/amocrmClient"
	"context"
	"time"

	"github.com/google/uuid"
)

func (s *service) RefreshLeadactivTokens(ctx context.Context) error {
	amoSecrets, err := s.repository.LeadactivAmoSecrets.ReadLeadactivAmoSecrets(ctx)
	if err != nil {
		if err.Error() != "no rows in result set" {
			s.logger.Error("RefreshLeadactivTokens - error reading project amo integrations list %s: %s", err.Error())
		}
		return err
	}

	client := amocrmClient.NewAmoClient(amoSecrets.BaseUrl(), amoSecrets.ClientId(), amoSecrets.ClientSecret(), amoSecrets.RedirectUri(), amoSecrets.Token(), amoSecrets.RefreshToken(), s.logger)
	token, refreshToken, err := client.RefreshToken()
	if err != nil {
		s.logger.Error("RefreshLeadactivTokens error refreshing: %s", err)
		return err
	}

	upFn := func(old *integration.LeadactivAmoSecrets) (*integration.LeadactivAmoSecrets, error) {
		return integration.NewLeadactivAmoSecretsWithId(old.Id(), old.BaseUrl(), old.ClientId(), old.ClientSecret(), old.RedirectUri(), token, refreshToken, old.CreatedAt(), time.Now())
	}

	_, err = s.repository.LeadactivAmoSecrets.UpdateLeadactivAmoSecrets(ctx, upFn)
	if err != nil {
		s.logger.Error("error updating project amocrm, after token refresh: %s", err)
		return err
	}

	s.logger.Info("leadactiv token refresh succes")

	return nil
}

func (s *service) CreateLeadactivIntegration(ctx context.Context, integration *integration.LeadactivIntegration) (err error) {
	err = s.repository.LeadactivIntegration.CreateLeadactivIntegration(ctx, integration)
	if err != nil {
		s.logger.Error("error CreateLeadactivIntegration: %s", err.Error())
		return err
	}

	return err
}

func (s *service) UpdateLeadactivIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.LeadactivIntegration) (*integration.LeadactivIntegration, error)) (integration *integration.LeadactivIntegration, err error) {
	integration, err = s.repository.LeadactivIntegration.UpdateLeadactivIntegration(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error UpdateLeadactivIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteLeadactivIntegration(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.LeadactivIntegration.DeleteLeadactivIntegration(ctx, id)
	if err != nil {
		s.logger.Error("error DeleteLeadactivIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadLeadactivIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (leadactivIntegration *integration.LeadactivIntegration, err error) {
	leadactivIntegration, err = s.repository.LeadactivIntegration.ReadLeadactivIntegrationOfSite(ctx, siteId)
	if err != nil {
		if err.Error() != "no rows in result set" {
			s.logger.Error("error ReadLeadactivIntegrationOfSite: %s", err.Error())
		}
		return
	}
	return
}

func (s *service) ReadLeadactivUsersList(ctx context.Context, page int) (list interface{}, err error) {
	integration, err := s.repository.LeadactivAmoSecrets.ReadLeadactivAmoSecrets(ctx)
	if err != nil {
		return
	}
	client := amocrmClient.NewAmoClient(integration.BaseUrl(), integration.ClientId(), integration.ClientSecret(), integration.RedirectUri(), integration.Token(), integration.RefreshToken(), s.logger)
	list, err = client.GetUsersList(page)
	return
}
