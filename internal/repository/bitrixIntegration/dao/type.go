package dao

import (
	"time"

	"github.com/google/uuid"
)

type BitrixIntegration struct {
	Id            uuid.UUID                `db:"id"`
	SiteId        uuid.UUID                `db:"site_id"`
	Responsible   int                      `db:"responsible"`
	SendType      string                   `db:"send_type"`
	Associations  []map[string]string      `db:"associations"`
	DefaultValues []map[string]interface{} `db:"default_values"`
	CreatedAt     time.Time                `db:"created_at"`
	ModifiedAt    time.Time                `db:"modified_at"`
}

var ColumnsBitrixIntegration = []string{
	"id",
	"site_id",
	"responsible",
	"send_type",
	"associations",
	"default_values",
	"created_at",
	"modified_at",
}
