package kviz

import (
	repository "backend/internal/repository"

	"backend/internal/service/kviz/adapters/resize"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository *repository.Repository
	logger     logger.Interface
	options    Options
	resizer    resize.Resize
}

type Options struct {
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repository.Repository, logger logger.Interface, resizer resize.Resize, options Options) (*service, error) {
	service := &service{
		repository: repository,
		logger:     logger,
		resizer:    resizer,
	}

	service.SetOptions(options)
	return service, nil
}
