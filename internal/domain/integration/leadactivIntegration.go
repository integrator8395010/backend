package integration

import (
	"time"

	"github.com/google/uuid"
)

type LeadactivIntegration struct {
	id          uuid.UUID
	siteId      uuid.UUID
	responsible int
	createdAt   time.Time
	modifiedAt  time.Time
}

func (l LeadactivIntegration) Id() uuid.UUID {
	return l.id
}

func (l LeadactivIntegration) SiteId() uuid.UUID {
	return l.siteId
}

func (l LeadactivIntegration) Responsible() int {
	return l.responsible
}

func (l LeadactivIntegration) CreatedAt() time.Time {
	return l.createdAt
}

func (l LeadactivIntegration) ModifiedAt() time.Time {
	return l.modifiedAt
}

func NewLeadactivIntegrationWithId(
	id uuid.UUID,
	siteId uuid.UUID,
	responsible int,
	createdAt time.Time,
	modifiedAt time.Time,
) (*LeadactivIntegration, error) {
	return &LeadactivIntegration{
		id:          id,
		siteId:      siteId,
		responsible: responsible,
		createdAt:   createdAt,
		modifiedAt:  modifiedAt,
	}, nil
}

func NewLeadactivIntegration(
	siteId uuid.UUID,
	responsible int,
) (*LeadactivIntegration, error) {
	return &LeadactivIntegration{
		id:          uuid.New(),
		siteId:      siteId,
		responsible: responsible,
		createdAt:   time.Now(),
		modifiedAt:  time.Now(),
	}, nil
}
