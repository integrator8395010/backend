package amocrmFieldAssociation

type AmocrmFieldAssociation struct {
	leadField   string
	amocrmField int
}

func NewAmocrmFieldAssociation(leadField string, amocrmField int) *AmocrmFieldAssociation {
	return &AmocrmFieldAssociation{
		leadField:   leadField,
		amocrmField: amocrmField,
	}
}

func (b AmocrmFieldAssociation) LeadField() string {
	return b.leadField
}

func (b AmocrmFieldAssociation) AmocrmField() int {
	return b.amocrmField
}
