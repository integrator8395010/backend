package site

import "backend/internal/domain/site"

func ToSiteResponse(site *site.Site) *SiteResponse {
	return &SiteResponse{
		ID:         site.Id().String(),
		Name:       site.Name(),
		Url:        site.Url(),
		CreatedBy:  site.CreatedBy(),
		CreatedAt:  site.CreatedAt(),
		ModifiedAt: site.ModifiedAt(),
	}
}
