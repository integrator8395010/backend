package delivery

import (
	"bytes"
	"io"
	"strings"

	"github.com/gin-gonic/gin"
)

func (d *Delivery) RequestLeadsLoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Method == "POST" && (strings.Contains(c.FullPath(), "/lead/site") || strings.Contains(c.FullPath(), "/lead/flexbe") || strings.Contains(c.FullPath(), "/lead/tilda") || strings.Contains(c.FullPath(), "/lead/marquiz")) {
			var buf bytes.Buffer
			tee := io.TeeReader(c.Request.Body, &buf)
			body, _ := io.ReadAll(tee)
			if c.ContentType() == "application/x-www-form-urlencoded" && strings.Contains(c.FullPath(), "/lead/site") {
				newBody := strings.ReplaceAll(string(body), "%", "проц.")
				newBody = strings.ReplaceAll(newBody, ";", ",")
				c.Request.Body = io.NopCloser(strings.NewReader(newBody))
			} else {
				c.Request.Body = io.NopCloser(&buf)
			}
			d.logger.Debug(string(body))
		}
		c.Next()
	}
}

/*
body, _ := io.ReadAll(tee)

*/
