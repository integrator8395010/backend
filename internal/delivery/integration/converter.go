package integration

import (
	"backend/internal/domain/integration"
)

func ToMailIntegrationResponse(integration *integration.MailIntegration) *MailIntegrationResponse {
	recipients := []string{}
	for _, val := range integration.Recipient() {
		recipients = append(recipients, val.String())
	}
	return &MailIntegrationResponse{
		ID:         integration.Id().String(),
		SiteId:     integration.SiteId().String(),
		Recipients: recipients,
		CreatedAt:  integration.CreatedAt().String(),
		ModifiedAt: integration.ModifiedAt().String(),
	}
}

func ToProjectAmocrmIntegration(integration *integration.ProjectAmoIntegration) *ProjectAmocrmIntegrationResponse {
	return &ProjectAmocrmIntegrationResponse{
		ID:           integration.Id().String(),
		ProjectId:    integration.ProjectId().String(),
		BaseUrl:      integration.BaseUrl(),
		ClientId:     integration.ClientId(),
		ClientSecret: integration.ClientSecret(),
		RedirectUri:  integration.RedirectUri(),
		Token:        integration.Token(),
		RefreshToken: integration.RefreshToken(),
		CreatedAt:    integration.CreatedAt().String(),
		ModifiedAt:   integration.ModifiedAt().String(),
	}
}

func ToLeadactivSecretsAmocrmIntegration(integration *integration.LeadactivAmoSecrets) *LeadactivAmocrmSecretsIntegrationResponse {
	return &LeadactivAmocrmSecretsIntegrationResponse{
		ID:           integration.Id().String(),
		BaseUrl:      integration.BaseUrl(),
		ClientId:     integration.ClientId(),
		ClientSecret: integration.ClientSecret(),
		RedirectUri:  integration.RedirectUri(),
		Token:        integration.Token(),
		RefreshToken: integration.RefreshToken(),
		CreatedAt:    integration.CreatedAt().String(),
		ModifiedAt:   integration.ModifiedAt().String(),
	}
}

func ToLeadactivAmocrmIntegartionResponse(integration *integration.LeadactivIntegration) *LeadactivAmocrmIntegrationResponse {
	return &LeadactivAmocrmIntegrationResponse{
		Id:          integration.Id().String(),
		SiteId:      integration.SiteId().String(),
		Responsible: integration.Responsible(),
		CreatedAt:   integration.CreatedAt().String(),
		ModifiedAt:  integration.ModifiedAt().String(),
	}
}

func ToAmocrmIntegrationResponse(integration *integration.AmocrmIntegration) *AmocrmIntegrationResponse {
	associations := []Association{}
	for _, association := range integration.Associations() {
		associations = append(associations, Association{LeadField: association.LeadField(), AmocrmField: association.AmocrmField()})
	}

	defaultValues := []DefaultValue{}
	for _, defaultValue := range integration.DefaultValues() {
		defaultValues = append(defaultValues, DefaultValue{Filed: defaultValue.Field(), Value: defaultValue.Value()})
	}

	return &AmocrmIntegrationResponse{
		ID:            integration.Id().String(),
		SiteId:        integration.SiteId().String(),
		PipelineId:    integration.PipelineId(),
		StatusId:      integration.StatusId(),
		Responsible:   integration.Responsible(),
		Unsorted:      integration.IsUnsorted(),
		Associations:  associations,
		DefaultValues: defaultValues,
		CreatedAt:     integration.CreatedAt().String(),
		ModifiedAt:    integration.ModifiedAt().String(),
	}
}

func ToProjectBitrixIntegration(integration *integration.ProjectBitrixIntegration) *ProjectBitrixIntegrationResponse {
	return &ProjectBitrixIntegrationResponse{
		ID:         integration.Id().String(),
		ProjectId:  integration.ProjectId().String(),
		BaseUrl:    integration.BaseUrl(),
		CreatedAt:  integration.CreatedAt().String(),
		ModifiedAt: integration.ModifiedAt().String(),
	}
}

func ToBitrixIntegrationResponse(integration *integration.BitrixIntegration) *BitrixIntegrationResponse {
	associations := []BitrixAssociation{}
	for _, association := range integration.Associations() {
		associations = append(associations, BitrixAssociation{LeadField: association.LeadField(), BitrixField: association.BitrixField()})
	}

	defaultValues := []BitrixDefaultValue{}
	for _, defaultValue := range integration.DefaultValues() {
		defaultValues = append(defaultValues, BitrixDefaultValue{Filed: defaultValue.Field(), Value: defaultValue.Value()})
	}

	return &BitrixIntegrationResponse{
		ID:            integration.Id().String(),
		SiteId:        integration.SiteId().String(),
		Responsible:   integration.Responsible(),
		SendType:      string(integration.SendType()),
		Associations:  associations,
		DefaultValues: defaultValues,
		CreatedAt:     integration.CreatedAt().String(),
		ModifiedAt:    integration.ModifiedAt().String(),
	}
}
