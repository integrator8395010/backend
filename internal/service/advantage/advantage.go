package advantage

import (
	"backend/internal/domain/advantage"
	"backend/pkg/tools"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateAdvantage(ctx context.Context, advantage *advantage.Advantage, inputFileName string, inputFileContent []byte) (*advantage.Advantage, error) {
	fileName, err := tools.SaveFileToFileStorage(inputFileName, inputFileContent)
	if err != nil {
		return nil, err
	}

	advantage.SetPhoto(fileName)

	err = s.repository.CreateAdvantage(ctx, advantage)
	if err != nil {
		s.logger.Error("error creating advantage: %s", err.Error())
		return nil, err
	}
	return advantage, nil
}

func (s *service) UpdateAdvantage(ctx context.Context, id uuid.UUID, updateFn func(advantage *advantage.Advantage) (*advantage.Advantage, error)) (advantage *advantage.Advantage, err error) {
	advantage, err = s.repository.UpdateAdvantage(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error creating advantage: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteAdvantage(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.DeleteAdvantage(ctx, id)
	if err != nil {
		s.logger.Error("error deleting advantage: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadAdvantageList(ctx context.Context) (advantagesList []*advantage.Advantage, err error) {
	advantagesList, err = s.repository.ReadAdvantageList(ctx)
	if err != nil {
		s.logger.Error("error deleting advantage: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadAdvantagesOfKviz(ctx context.Context, kvizId uuid.UUID) (advantagesList []*advantage.Advantage, err error) {
	advantagesList, err = s.repository.ReadAdvantagesOfKviz(ctx, kvizId)
	if err != nil {
		s.logger.Error("error deleting advantage: %s", err.Error())
		return
	}
	return
}
