package project

import (
	"backend/internal/domain/project"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreateProject(ctx context.Context, project *project.Project, inputFileName string, inputFileContent []byte) (*project.Project, error)
	UpdateProject(ctx context.Context, id uuid.UUID, updateFn func(project *project.Project) (*project.Project, error)) (project *project.Project, err error)
	DeleteProject(ctx context.Context, id uuid.UUID) (err error)
	ReadProjectList(ctx context.Context) (projectsList []*project.Project, err error)
}
