package advantage

import (
	"backend/internal/domain/photo"
)

func ToPhotoResponse(photo *photo.Photo) *PhotoResponse {
	return &PhotoResponse{
		ID:         photo.Id().String(),
		KvizId:     photo.KvizId().String(),
		Photo:      photo.Photo(),
		CreatedAt:  photo.CreatedAt(),
		ModifiedAt: photo.ModifiedAt(),
	}
}
