package customField

type Enum struct {
	EnumCode string `json:"enum_code"`
	Value    string `json:"value"`
}

type EnumWithoutCode struct {
	Value string `json:"value"`
}
