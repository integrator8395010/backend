package site

import (
	"time"

	"github.com/google/uuid"
)

type CreateSiteRequest struct {
	Name      string `json:"name" example:"Атлант"`
	Url       string `json:"url" example:"leadactiv.ru"`
	ProjectId string `json:"projectId" example:"00000000-0000-0000-0000-000000000000" format:"uuid"`
}

type SiteListResponse struct {
	Sites []*SiteResponse `json:"siteList"`
}

type SiteResponse struct {
	ID         string    `json:"id"`
	Name       string    `json:"name"`
	Url        string    `json:"url"`
	ProjectId  uuid.UUID `json:"projectId"`
	CreatedBy  uuid.UUID `json:"createdBy"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type UpdateSiteRequest struct {
	ID        string `json:"id"`
	Name      string `json:"name" example:"Атлант"`
	Url       string `json:"url" example:"leadactiv.ru"`
	ProjectId string `json:"projectId" example:"00000000-0000-0000-0000-000000000000" format:"uuid"`
}
