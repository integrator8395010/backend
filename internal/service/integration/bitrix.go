package integration

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateBitrixIntegration(ctx context.Context, integration *integration.BitrixIntegration) (err error) {
	err = s.repository.BitrixIntegration.CreateBitrixIntegration(ctx, integration)
	if err != nil {
		s.logger.Error("error create bitrixIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) UpdateBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.BitrixIntegration) (*integration.BitrixIntegration, error)) (integration *integration.BitrixIntegration, err error) {
	integration, err = s.repository.BitrixIntegration.UpdateBitrixIntegration(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error update bitrixIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteBitrixIntegration(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.BitrixIntegration.DeleteBitrixIntegration(ctx, id)
	if err != nil {
		s.logger.Error("error create bitrixIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadBitrixIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.BitrixIntegration, err error) {
	integration, err = s.repository.BitrixIntegration.ReadBitrixIntegrationOfSite(ctx, siteId)
	if err != nil {
		s.logger.Error("error read bitrixIntegration of site: %s", err.Error())
		return
	}
	return
}
