package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/projectAmocrmIntegration/dao"
)

func (r *Repository) toDomainProjectAmocrmIntegration(dao *dao.ProjectAmocrmIntegration) (*integration.ProjectAmoIntegration, error) {
	return integration.NewProjectAmoIntegrationWithId(dao.Id, dao.ProjectId, dao.BaseUrl, dao.ClientId, dao.ClientSecret, dao.RedirectUri, dao.Token, dao.RefreshToken, dao.CreatedAt, dao.ModifiedAt)
}
