package project

import (
	"time"

	"github.com/google/uuid"
)

type Project struct {
	id         uuid.UUID
	name       string
	photo      string
	createdBy  uuid.UUID
	createdAt  time.Time
	modifiedAt time.Time
}

func (c Project) Id() uuid.UUID {
	return c.id
}

func (c Project) Name() string {
	return c.name
}

func (c Project) Photo() string {
	return c.photo
}

func (c *Project) SetPhoto(photo string) {
	c.photo = photo
}

func (c Project) CreatedBy() uuid.UUID {
	return c.createdBy
}

func (c Project) CreatedAt() time.Time {
	return c.createdAt
}

func (c Project) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	name string,
	photo string,
	createdBy uuid.UUID,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Project, error) {
	return &Project{
		id:         id,
		name:       name,
		photo:      photo,
		createdBy:  createdBy,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	name string,
	photo string,
	createdBy uuid.UUID,
) (*Project, error) {
	return &Project{
		id:         uuid.New(),
		name:       name,
		photo:      photo,
		createdBy:  createdBy,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
