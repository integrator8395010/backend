package lead

import (
	integra "backend/internal/domain/integration"
	"backend/internal/domain/lead"
	"backend/pkg/amocrmClient"
	amocrmLead "backend/pkg/amocrmClient/lead"
	"backend/pkg/bitrixClient"
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
)

// ProcessLeadsToEmail - get all mails integrations list, and send unsended leads for all of them.
func (s *service) ProcessLeadsToEmail(ctx context.Context) (err error) {
	integrationsList, err := s.repository.MailIntegration.ReadAllSitesMailIntegrations(ctx)
	for _, integration := range integrationsList {
		//read leads for integration
		leads, err := s.repository.Lead.GetUnsendedLeadsToMailIntegration(ctx, integration.Id())
		if err != nil {
			s.logger.Error(fmt.Sprintf("ProcessLeadsToEmail - error getting unsended leads from repo: %s", err.Error()))
			continue
		}
		emails := make([]string, len(integration.Recipient()))
		for index, email := range integration.Recipient() {
			emails[index] = email.String()
		}

		for _, leadInput := range leads {
			err = s.mailSender.SendEmailText(emailSender, emails, "Заявка с сайта", generateMessageFromLead(leadInput))
			if err != nil {
				s.logger.Error(fmt.Sprintf("ProcessLeadsToEmail - error sending lead to email %v: %s", emails, err.Error()))
				err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Mail, 0, false, fmt.Sprintf("error sending: %s", err.Error()))
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToEmail - error store sendLog with error: %s", err.Error()))
				}
			}
			err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Mail, 0, true, "sended")
			if err != nil {
				s.logger.Error(fmt.Sprintf("ProcessLeadsToEmail - error store sendLog: %s", err.Error()))
				continue
			}
		}
	}
	return
}

// ProcessLeadsToBitrix - get all bitrix integrations list, and send unsended leads for all of them.
func (s *service) ProcessLeadsToBitrix(ctx context.Context) (err error) {
	integrationsList, err := s.repository.BitrixIntegration.ReadAllSitesBitrixIntegrations(ctx)
	for _, integration := range integrationsList {
		//read leads for integration
		leads, err := s.repository.Lead.GetUnsendedLeadsToBitrixIntegration(ctx, integration.Id())
		if err != nil {
			s.logger.Error(fmt.Sprintf("ProcessLeadsToBitrix - error getting unsended leads from repo: %s", err.Error()))
			continue
		}
		if len(leads) > 0 {
			projectBitrixIntegration, err := s.repository.ProjectBitrixIntegartion.ReadProjectBitrixIntegrationBySiteId(ctx, integration.SiteId())
			if err != nil {
				s.logger.Error(fmt.Sprintf("ProcessLeadsToBitrix - integration to send leads not founded: %s", err.Error()))
				continue
			}
			for _, leadInput := range leads {
				bitrixClient := bitrixClient.NewBitrixClient(projectBitrixIntegration.BaseUrl())

				responseId := 0
				if integration.SendType() == integra.Lead {
					responseId, err = bitrixClient.SendLead(leadInput, integration)
				} else {
					responseId, err = bitrixClient.SendDeal(leadInput, integration)
				}
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToBitrix - error sending lead to bitrix: %s", err.Error()))
					err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Bitrix, responseId, false, fmt.Sprintf("error sending: %s", err.Error()))
					if err != nil {
						s.logger.Error(fmt.Sprintf("ProcessLeadsToBitrix - error store sendLog with error: %s", err.Error()))
					}
					continue
				}

				err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Bitrix, responseId, true, "sended")
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToBitrix - error store sendLog: %s", err.Error()))
					continue
				}
			}
		}
	}
	return
}

// ProcessLeadsToAmocrm - get all amo integrations list, and send unsended leads for all of them.
func (s *service) ProcessLeadsToAmocrm(ctx context.Context) (err error) {
	integrationsList, err := s.repository.AmoCrmIntegration.ReadAllSitesAmocrmIntegrations(ctx)
	for _, integration := range integrationsList {
		//read leads for integration
		leads, err := s.repository.Lead.GetUnsendedLeadsToAmocrmIntegration(ctx, integration.Id())
		if err != nil {
			s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error getting unsended leads from repo: %s", err.Error()))
			continue
		}
		if len(leads) > 0 {
			projectAmocrmIntegration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmoIntegrationBySiteId(ctx, integration.SiteId())
			if err != nil {
				s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - integration to send leads not founded: %s", err.Error()))
				continue
			}
		L:
			for index, leadInput := range leads {
				client := amocrmClient.NewAmoClient(projectAmocrmIntegration.BaseUrl(), projectAmocrmIntegration.ClientId(), projectAmocrmIntegration.ClientSecret(), projectAmocrmIntegration.RedirectUri(), projectAmocrmIntegration.Token(), projectAmocrmIntegration.RefreshToken(), s.logger)

				responseId := 0

				if integration.IsUnsorted() {
					leadToSend, err := s.toAmoApiUnsorted(index, leadInput, integration)
					if err != nil {
						s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error converting lead to amoApiLead: %s", err.Error()))
						continue L
					}
					result, err := client.SendUnsorted(leadToSend, integration.PipelineId())
					if err != nil {
						s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error sendind unsorted lead to crm: %s", err.Error()))
						err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Amo, responseId, false, err.Error())
						if err != nil {
							s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error store sendLog: %s", err.Error()))
							continue
						}
						continue
					}
					responseId = result
				} else {
					leadToSend, err := s.toAmoApiLead(index, leadInput, integration)
					if err != nil {
						s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error converting lead to amoApiLead: %s", err.Error()))
						continue L
					}
					result, err := client.SendLead(amocrmClient.SendLeadComplexRequest{Leads: []*amocrmLead.Lead{leadToSend}, DontCheckRepeat: false})
					if err != nil {
						s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error sendind lead to crm: %s", err.Error()))
						err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Amo, responseId, false, err.Error())
						if err != nil {
							s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error store sendLog: %s", err.Error()))
							continue
						}
						continue
					}
					responseId = result
				}

				err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Amo, responseId, true, "sended")
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToAmocrm - error store sendLog: %s", err.Error()))
					continue
				}

			}
		}
	}
	return
}

// ProcessLeadsToLeadactiv - get all amo leadactiv integration list, and send unsended leads for all of them.
func (s *service) ProcessLeadsToLeadactiv(ctx context.Context) (err error) {
	integrationsList, err := s.repository.LeadactivIntegration.ReadAllSitesLeadactivIntegrations(ctx)
	for _, integration := range integrationsList {
		//read leads for integration
		leads, err := s.repository.Lead.GetUnsendedLeadsToLeadactivIntegration(ctx, integration.Id())
		if err != nil {
			s.logger.Error(fmt.Sprintf("ProcessLeadsToLeadactiv - error getting unsended leads from repo: %s", err.Error()))
			continue
		}
		if len(leads) > 0 {
			leadactivIntegration, err := s.repository.LeadactivAmoSecrets.ReadLeadactivAmoSecrets(ctx)
			if err != nil {
				s.logger.Error(fmt.Sprintf("ProcessLeadsToLeadactiv - integration to send leads not founded: %s", err.Error()))
				continue
			}
		L:
			for index, leadInput := range leads {
				client := amocrmClient.NewAmoClient(leadactivIntegration.BaseUrl(), leadactivIntegration.ClientId(), leadactivIntegration.ClientSecret(), leadactivIntegration.RedirectUri(), leadactivIntegration.Token(), leadactivIntegration.RefreshToken(), s.logger)

				responseId := 0

				leadToSend, err := s.toAmoApiLeadactiv(index, leadInput, integration)
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToLeadactiv - error converting lead to amoApiLead: %s", err.Error()))
					continue L
				}
				result, err := client.SendLead(amocrmClient.SendLeadComplexRequest{Leads: []*amocrmLead.Lead{leadToSend}, DontCheckRepeat: true})
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToLeadactiv - error sendind lead to crm: %s", err.Error()))
					err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Leadactiv, responseId, false, err.Error())
					if err != nil {
						s.logger.Error(fmt.Sprintf("ProcessLeadsToLeadactiv - error store sendLog: %s", err.Error()))
						continue
					}
					continue
				}
				responseId = result

				err = s.processLeadSendLog(ctx, leadInput, integration.Id(), lead.Leadactiv, responseId, true, "sended")
				if err != nil {
					s.logger.Error(fmt.Sprintf("ProcessLeadsToLeadactiv - error store sendLog: %s", err.Error()))
					continue
				}

			}
		}
	}
	return
}

func (s *service) processLeadSendLog(ctx context.Context, leadInput *lead.Lead, integrationId uuid.UUID, integrationType lead.IntegrationType, responseId int, success bool, comment string) (err error) {
	sendLog, err := s.repository.LeadsSendLog.ReadLeadsSendLogByLeadIdAndTypeList(context.Background(), leadInput.Id(), integrationType)
	if err != nil {
		err = s.createNewSendLog(ctx, leadInput, integrationId, integrationType, responseId, success, comment)
		return
	}
	upFn := func(oldSendLog *lead.LeadsSendLog) (*lead.LeadsSendLog, error) {
		return lead.NewLeadsSendLogWithId(oldSendLog.Id(), oldSendLog.LeadId(), oldSendLog.IntegrationId(), oldSendLog.IntegrationType(), responseId, oldSendLog.Source(), comment, success, oldSendLog.CreatedAt(), time.Now())
	}
	_, err = s.repository.LeadsSendLog.UpdateLeadSendLog(ctx, sendLog.Id(), upFn)

	return
}

func (s *service) createNewSendLog(ctx context.Context, leadInput *lead.Lead, integrationId uuid.UUID, integrationType lead.IntegrationType, responseId int, success bool, comment string) (err error) {
	leadSendLog, err := lead.NewLeadsSendLog(leadInput.Id(), integrationId, integrationType, responseId, leadInput.Source(), comment, success)
	if err != nil {
		return
	}
	err = s.repository.LeadsSendLog.CreateLeadSendLog(ctx, leadSendLog)
	return
}
