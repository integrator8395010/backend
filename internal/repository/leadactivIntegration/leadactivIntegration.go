package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/leadactivIntegration/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "leadactiv_integration"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"leadactiv_integration_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateLeadactivIntegration(ctx context.Context, integration *integration.LeadactivIntegration) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsLeadactivIntegration...).Values(integration.Id(), integration.SiteId(), integration.Responsible(), integration.CreatedAt(), integration.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateLeadactivIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.LeadactivIntegration) (*integration.LeadactivIntegration, error)) (integration *integration.LeadactivIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLeadactivAmoSecrets, err := r.oneLeadactivIntegrationTx(ctx, id, tx)
	if err != nil {
		return
	}

	leadactivAmoSecrets, err := updateFn(upLeadactivAmoSecrets)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("site_id", leadactivAmoSecrets.SiteId()).Set("responsible", leadactivAmoSecrets.Responsible()).Set("modified_at", leadactivAmoSecrets.ModifiedAt()).Where("id = ?", leadactivAmoSecrets.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return leadactivAmoSecrets, nil
}

func (r *Repository) DeleteLeadactivIntegration(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadLeadactivIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (leadactivIntegration *integration.LeadactivIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLeadactivIntegration...).From(tableName).Where("site_id = ?", siteId)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.LeadactivIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainLeadactivIntegration(&daoIntegration)
}

func (r *Repository) ReadAllSitesLeadactivIntegrations(ctx context.Context) (integrationList []*integration.LeadactivIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLeadactivIntegration...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegrations, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.LeadactivIntegration])
	if err != nil {
		return nil, err
	}

	integrationList = make([]*integration.LeadactivIntegration, len(daoIntegrations))

	for index, daoIntegration := range daoIntegrations {
		domainIntegration, err := r.toDomainLeadactivIntegration(&daoIntegration)
		if err != nil {
			return nil, err
		}

		integrationList[index] = domainIntegration
	}

	return
}

func (r *Repository) oneLeadactivIntegrationTx(ctx context.Context, id uuid.UUID, tx pgx.Tx) (response *integration.LeadactivIntegration, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsLeadactivIntegration...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.LeadactivIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainLeadactivIntegration(&daoIntegration)
}
