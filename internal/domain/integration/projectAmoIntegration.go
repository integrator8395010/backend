package integration

import (
	"time"

	"github.com/google/uuid"
)

type ProjectAmoIntegration struct {
	id           uuid.UUID
	projectId    uuid.UUID
	baseUrl      string
	clientId     string
	clientSecret string
	redirectUri  string
	token        string
	refreshToken string
	createdAt    time.Time
	modifiedAt   time.Time
}

func NewProjectAmoIntegrationWithId(
	id uuid.UUID,
	projectId uuid.UUID,
	baseUrl string,
	clientId string,
	clientSecret string,
	redirectUri string,
	token,
	refreshToken string,
	createdAt time.Time,
	modifiedAt time.Time) (*ProjectAmoIntegration, error) {
	return &ProjectAmoIntegration{
		id:           id,
		projectId:    projectId,
		baseUrl:      baseUrl,
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  redirectUri,
		token:        token,
		refreshToken: refreshToken,
		createdAt:    createdAt,
		modifiedAt:   modifiedAt,
	}, nil
}

func NewProjectAmoIntegration(
	projectId uuid.UUID,
	baseUrl string,
	clientId string,
	clientSecret string,
	redirectUri string,
	token,
	refreshToken string,
) (*ProjectAmoIntegration, error) {
	return &ProjectAmoIntegration{
		id:           uuid.New(),
		projectId:    projectId,
		baseUrl:      baseUrl,
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  redirectUri,
		token:        token,
		refreshToken: refreshToken,
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}

func NewSecretsOauth(projectId uuid.UUID, clientId, clientSecret string) (*ProjectAmoIntegration, error) {
	return &ProjectAmoIntegration{
		id:           uuid.New(),
		projectId:    projectId,
		baseUrl:      "",
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  "",
		token:        "",
		refreshToken: "",
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}

func (m ProjectAmoIntegration) Id() uuid.UUID {
	return m.id
}

func (m ProjectAmoIntegration) ProjectId() uuid.UUID {
	return m.projectId
}

func (m ProjectAmoIntegration) BaseUrl() string {
	return m.baseUrl
}

func (m ProjectAmoIntegration) ClientId() string {
	return m.clientId
}

func (m ProjectAmoIntegration) ClientSecret() string {
	return m.clientSecret
}

func (m ProjectAmoIntegration) RedirectUri() string {
	return m.redirectUri
}

func (m ProjectAmoIntegration) Token() string {
	return m.token
}

func (m *ProjectAmoIntegration) SetToken(token string) {
	m.token = token
}

func (m ProjectAmoIntegration) RefreshToken() string {
	return m.refreshToken
}

func (m *ProjectAmoIntegration) SeRefreshToken(refreshToken string) {
	m.refreshToken = refreshToken
}

func (m ProjectAmoIntegration) CreatedAt() time.Time {
	return m.createdAt
}

func (m ProjectAmoIntegration) ModifiedAt() time.Time {
	return m.modifiedAt
}
