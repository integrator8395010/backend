package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/mailIntegration/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "mail_integration"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"mail_integration_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateMailIntegration(ctx context.Context, integration *integration.MailIntegration) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsMailIntegration...).Values(integration.Id(), integration.SiteId(), integration.Recipient(), integration.CreatedAt(), integration.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateMailIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.MailIntegration) (*integration.MailIntegration, error)) (integration *integration.MailIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLead, err := r.oneIntegrationTx(ctx, tx, id)
	if err != nil {
		return
	}

	mailIntegrationNew, err := updateFn(upLead)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("site_id", mailIntegrationNew.SiteId()).Set("recipients", mailIntegrationNew.Recipient()).Set("modified_at", mailIntegrationNew.ModifiedAt()).Where("id = ?", mailIntegrationNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return mailIntegrationNew, nil
}

func (r *Repository) DeleteMailIntegration(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadMailIntegrationOfSite(ctx context.Context, siteid uuid.UUID) (integration *integration.MailIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsMailIntegration...).Where("site_id = ?", siteid).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.MailIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainIntegration(&daoIntegration)
}

func (r *Repository) ReadAllSitesMailIntegrations(ctx context.Context) (integrationList []*integration.MailIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsMailIntegration...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegrations, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.MailIntegration])
	if err != nil {
		return nil, err
	}

	integrationList = make([]*integration.MailIntegration, len(daoIntegrations))

	for index, daoIntegration := range daoIntegrations {
		domainIntegration, err := r.toDomainIntegration(&daoIntegration)
		if err != nil {
			return nil, err
		}

		integrationList[index] = domainIntegration
	}

	return
}

func (r *Repository) oneIntegrationTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *integration.MailIntegration, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsMailIntegration...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.MailIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainIntegration(&daoIntegration)
}
