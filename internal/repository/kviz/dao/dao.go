package dao

import (
	"time"

	"github.com/google/uuid"
)

type Kviz struct {
	Id             uuid.UUID `db:"id"`
	SiteId         uuid.UUID `db:"site_id"`
	Name           string    `db:"name"`
	TemplateId     uuid.UUID `db:"template_id"`
	Background     string    `db:"background"`
	MainColor      string    `db:"main_color"`
	SecondaryColor string    `db:"secondary_color"`
	SubTitle       string    `db:"sub_title"`
	SubTitleItems  string    `db:"sub_title_items"`
	PhoneStepTitle string    `db:"phone_step_title"`
	FooterTitle    string    `db:"footer_title"`
	Phone          string    `db:"phone"`
	Politics       bool      `db:"politics"`
	Roistat        string    `db:"roistat"`
	AdvantageTitle string    `db:"advantages_title"`
	PhotosTitle    string    `db:"photos_title"`
	PlansTitle     string    `db:"plans_title"`
	ResultStepText string    `db:"result_step_text"`
	Qoopler        bool      `db:"qoopler"`
	DmpOne         string    `db:"dmp_one"`
	ValidatePhone  bool      `db:"validate_phone"`
	CreatedBy      uuid.UUID `db:"created_by"`
	CreatedAt      time.Time `db:"created_at"`
	ModifiedAt     time.Time `db:"modified_at"`
}

var ColumnsKviz = []string{
	"id",
	"site_id",
	"name",
	"template_id",
	"background",
	"main_color",
	"secondary_color",
	"sub_title",
	"sub_title_items",
	"phone_step_title",
	"footer_title",
	"phone",
	"politics",
	"roistat",
	"advantages_title",
	"photos_title",
	"plans_title",
	"result_step_text",
	"qoopler",
	"dmp_one",
	"validate_phone",
	"created_by",
	"created_at",
	"modified_at",
}
