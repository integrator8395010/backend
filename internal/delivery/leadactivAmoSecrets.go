package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateLeadactivAmocrmIntegrationOauthSecret
// @Summary Получаем секреты из oauth и обновляем или создаем интеграцию leadactiv.
// @Description Получаем секреты из oauth и обновляем или создаем интеграцию leadactiv.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.CreateAmocrmIntegrationOauthSecretRequest true "Body"
// @Success 200			{object} 	jsonIntegration.LeadactivAmocrmSecretsIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/amocrm/leadactiv/secret [post]
func (d *Delivery) CreateLeadactivAmocrmIntegrationOauthSecret(c *gin.Context) {
	request := jsonIntegration.CreateAmocrmIntegrationOauthSecretRequest{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthSecret json parse error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	integration, err := d.services.Integration.ProcessLeadactivAmocrmSecrets(context.Background(), request.ClientId, request.ClientSecret)
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthSecret store error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToLeadactivSecretsAmocrmIntegration(integration))
}

// CreateLeadactivAmocrmIntegrationOauthCode
// @Summary Получаем код авторизации из oauth и обновляем или создаем интеграцию.
// @Description Получаем код авторизации из oauth и обновляем или создаем интеграцию.
// @Tags integration
// @Accept json
// @Produce json
// @Param id path string true "Project ID"
// @Param data query jsonIntegration.LeadactivAmocrmSecretsIntegrationResponse true "Body"
// @Success 200			{object} 	jsonIntegration.LeadactivAmocrmSecretsIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/amocrm/leadactiv/code [get]
func (d *Delivery) CreateLeadactivAmocrmIntegrationOauthCode(c *gin.Context) {
	request := jsonIntegration.CreateAmocrmIntegrationOauthCodeRequest{}
	if err := c.BindQuery(&request); err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthCode bind query error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	integration, err := d.services.Integration.ProcessLeadactivAmocrmTokenRequestWithAuthorizationCode(context.Background(), request.Code, request.Referer)
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthCode error processing: %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToLeadactivSecretsAmocrmIntegration(integration))
}

// ReadLeadactivAmocrmIntegration
// @Summary Получаем параметры интеграции лидактива.
// @Description Получаем параметры интеграции лидактива.
// @Tags integration
// @Accept json
// @Produce json
// @Success 200			{object} 	jsonIntegration.LeadactivAmocrmSecretsIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/leadactiv [get]
func (d *Delivery) ReadLeadactivAmocrmIntegration(c *gin.Context) {
	integration, err := d.services.Integration.ReadLeadactivAmoSecrets(context.Background())
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthCode error processing: %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToLeadactivSecretsAmocrmIntegration(integration))
}
