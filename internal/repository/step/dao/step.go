package dao

import (
	"time"

	"github.com/google/uuid"
)

type Step struct {
	Id         uuid.UUID `db:"id"`
	KvizId     uuid.UUID `db:"kviz_id"`
	Title      string    `db:"title"`
	Answers    []*string `db:"answers"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsStep = []string{
	"id",
	"kviz_id",
	"title",
	"answers",
	"created_at",
	"modified_at",
}
