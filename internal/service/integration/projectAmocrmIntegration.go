package integration

import (
	"backend/internal/domain/integration"
	"backend/pkg/amocrmClient"
	"context"
	"time"

	"github.com/google/uuid"
)

func (s *service) RefreshProjectIntegrationsTokens(ctx context.Context) error {
	projectIntegrationsList, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegrationList(ctx)
	if err != nil {
		s.logger.Error("RefreshProjectIntegrationsTokens - error reading project amo integrations list %s: %s", err.Error())
		return err
	}

	for _, projectIntegration := range projectIntegrationsList {
		client := amocrmClient.NewAmoClient(projectIntegration.BaseUrl(), projectIntegration.ClientId(), projectIntegration.ClientSecret(), projectIntegration.RedirectUri(), projectIntegration.Token(), projectIntegration.RefreshToken(), s.logger)
		token, refreshToken, err := client.RefreshToken()
		if err != nil {
			s.logger.Error("error refreshing token of project amocrm: %s", err)
			continue
		}

		upFn := func(old *integration.ProjectAmoIntegration) (*integration.ProjectAmoIntegration, error) {
			return integration.NewProjectAmoIntegrationWithId(old.Id(), old.ProjectId(), old.BaseUrl(), old.ClientId(), old.ClientSecret(), old.RedirectUri(), token, refreshToken, old.CreatedAt(), time.Now())
		}

		_, err = s.repository.ProjectAmoCrmIntegartion.UpdateProjectAmocrmIntegration(ctx, projectIntegration.Id(), upFn)
		if err != nil {
			s.logger.Error("error updating project amocrm, after token refresh: %s", err)
			continue
		}

		s.logger.Info("token refresh succes for: %s", projectIntegration.Id())
	}

	return nil
}

func (s *service) ReadProjectAmocrmIntegration(ctx context.Context, projectId uuid.UUID) (*integration.ProjectAmoIntegration, error) {
	projectIntegration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, projectId)
	if err != nil {
		if err.Error() != "no rows in result set" {
			s.logger.Error("error reading project amo integration %s: %s", projectId.String(), err.Error())
		}
		return nil, err
	}

	return projectIntegration, err
}

func (s *service) ProcessAmocrmTokenRequestWithAuthorizationCode(ctx context.Context, projectId uuid.UUID, authCode, referer string) (*integration.ProjectAmoIntegration, error) {
	projectIntegration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, projectId)
	if err != nil {
		s.logger.Error("error reading project amo integration %s: %s", projectId.String(), err.Error())
		return nil, err
	}

	redirectUri := "https://" + s.baseUrl + "/integration/amocrm/project/code/" + projectId.String()

	amocrmClient := amocrmClient.NewAmoClient(referer, projectIntegration.ClientId(), projectIntegration.ClientSecret(), redirectUri, "", "", s.logger)
	token, refreshToken, err := amocrmClient.RequestTokenWithAuthorizationCode(authCode)
	if err != nil {
		s.logger.Error("error ProcessAmocrmTokenRequestWithAuthorizationCode get token and refresh token with authorization code: %s", err.Error())
		return nil, err
	}

	updateFn := func(oldProjectIntegration *integration.ProjectAmoIntegration) (*integration.ProjectAmoIntegration, error) {
		return integration.NewProjectAmoIntegrationWithId(oldProjectIntegration.Id(), oldProjectIntegration.ProjectId(), referer, oldProjectIntegration.ClientId(), oldProjectIntegration.ClientSecret(), redirectUri, token, refreshToken, oldProjectIntegration.CreatedAt(), time.Now())
	}

	res, err := s.repository.ProjectAmoCrmIntegartion.UpdateProjectAmocrmIntegration(ctx, projectIntegration.Id(), updateFn)
	if err != nil {
		s.logger.Error("error ProcessAmocrmTokenRequestWithAuthorizationCode update amocrmIntegration: %s", err.Error())
		return res, err
	}
	return res, nil
}

func (s *service) createProjectAmocrmIntegration(ctx context.Context, integration *integration.ProjectAmoIntegration) (err error) {
	err = s.repository.ProjectAmoCrmIntegartion.CreateProjectAmocrmIntegration(ctx, integration)
	if err != nil {
		s.logger.Error("error create ProjectamocrmIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) ProcessAmocrmSecrets(ctx context.Context, projectId uuid.UUID, clientId, clientSecret string) (*integration.ProjectAmoIntegration, error) {
	oldIntegration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, projectId)
	if err != nil {
		amocrmIntegration, err := integration.NewSecretsOauth(projectId, clientId, clientSecret)
		if err != nil {
			s.logger.Error("error create amocrmIntegration: %s", err.Error())
			return nil, err
		}
		err = s.createProjectAmocrmIntegration(ctx, amocrmIntegration)
		return amocrmIntegration, err
	}

	updateFn := func(oldIntegration *integration.ProjectAmoIntegration) (*integration.ProjectAmoIntegration, error) {
		return integration.NewProjectAmoIntegrationWithId(oldIntegration.Id(), projectId, "", clientId, clientSecret, "", "", "", oldIntegration.CreatedAt(), time.Now())
	}

	res, err := s.repository.ProjectAmoCrmIntegartion.UpdateProjectAmocrmIntegration(ctx, oldIntegration.Id(), updateFn)
	if err != nil {
		s.logger.Error("error update amocrmIntegration: %s", err.Error())
		return res, err
	}
	return res, nil
}

func (s *service) ReadAmocrmUsersList(ctx context.Context, projectId uuid.UUID) (list interface{}, err error) {
	integration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, projectId)
	if err != nil {
		return
	}
	client := amocrmClient.NewAmoClient(integration.BaseUrl(), integration.ClientId(), integration.ClientSecret(), integration.RedirectUri(), integration.Token(), integration.RefreshToken(), s.logger)
	list, err = client.GetUsersList(0)
	return
}

func (s *service) ReadAmocrmPipelinesList(ctx context.Context, projectId uuid.UUID) (list interface{}, err error) {
	integration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, projectId)
	if err != nil {
		return
	}
	client := amocrmClient.NewAmoClient(integration.BaseUrl(), integration.ClientId(), integration.ClientSecret(), integration.RedirectUri(), integration.Token(), integration.RefreshToken(), s.logger)
	list, err = client.GetPipelinesList()
	return
}

func (s *service) ReadAmocrmStatusesOfPipelineList(ctx context.Context, projectId uuid.UUID, pipelineId int) (list interface{}, err error) {
	integration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, projectId)
	if err != nil {
		return
	}
	client := amocrmClient.NewAmoClient(integration.BaseUrl(), integration.ClientId(), integration.ClientSecret(), integration.RedirectUri(), integration.Token(), integration.RefreshToken(), s.logger)
	list, err = client.GetStatusesOfPipeline(pipelineId)
	return
}

func (s *service) ReadAmocrmFieldsList(ctx context.Context, id uuid.UUID, page int) (list interface{}, err error) {
	integration, err := s.repository.ProjectAmoCrmIntegartion.ReadProjectAmocrmIntegration(ctx, id)
	if err != nil {
		return
	}
	client := amocrmClient.NewAmoClient(integration.BaseUrl(), integration.ClientId(), integration.ClientSecret(), integration.RedirectUri(), integration.Token(), integration.RefreshToken(), s.logger)
	list, err = client.GetLeadFields(page)
	return
}
