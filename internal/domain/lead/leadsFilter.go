package lead

import (
	"time"

	"github.com/google/uuid"
)

type LeadsFilter struct {
	projectId uuid.UUID
	url       string
	phone     string
	source    Source
	from      time.Time
	to        time.Time
}

func NewLeadsFilter() *LeadsFilter {
	return &LeadsFilter{}
}

func (f LeadsFilter) ProjectId() uuid.UUID {
	return f.projectId
}

func (f *LeadsFilter) SetProjectId(projectId uuid.UUID) {
	f.projectId = projectId
}

func (f LeadsFilter) Url() string {
	return f.url
}

func (f *LeadsFilter) SetUrl(url string) {
	f.url = url
}

func (f LeadsFilter) Phone() string {
	return f.phone
}

func (f *LeadsFilter) SetPhone(phone string) {
	f.phone = phone
}

func (f LeadsFilter) Source() Source {
	return f.source
}

func (f *LeadsFilter) SetSource(source Source) {
	f.source = source
}

func (f LeadsFilter) From() time.Time {
	return f.from
}

func (f *LeadsFilter) SetFrom(from time.Time) {
	f.from = from
}

func (f LeadsFilter) To() time.Time {
	return f.to
}

func (f *LeadsFilter) SetTo(to time.Time) {
	f.to = to
}
