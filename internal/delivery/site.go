package delivery

import (
	jsonSite "backend/internal/delivery/site"
	"backend/internal/domain/site"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// ReadSiteById
// @Summary Получить домен по id.
// @Description Получить домен по id.
// @Tags site
// @Produce json
// @Param id path string true "Site ID"
// @Produce json
// @Success 200 		{object} 	jsonSite.SiteResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /site/{id} [get]
func (d *Delivery) ReadSiteById(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	site, err := d.services.Site.ReadSiteById(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, jsonSite.ToSiteResponse(site))
}

// CrateSite
// @Summary Создание сайта.
// @Description Создание сайта.
// @Tags site
// @Accept json
// @Produce json
// @Param data formData jsonSite.CreateSiteRequest true "Body"
// @Success 201			{object} 	jsonSite.SiteResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /site/ [put]
func (d *Delivery) CreateSite(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonSite.CreateSiteRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectId, err := uuid.Parse(request.ProjectId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	site, err := site.New(request.Name, request.Url, projectId, auth.UserId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Site.CreateSite(context.Background(), site)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonSite.ToSiteResponse(site))
}

// UpdateSite
// @Summary Обновление сайта.
// @Description Обновление сайта.
// @Tags site
// @Accept json
// @Produce json
// @Param data body jsonSite.UpdateSiteRequest true "Body"
// @Success 200			{object} 	jsonSite.SiteResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /site/ [post]
func (d *Delivery) UpdateSite(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonSite.UpdateSiteRequest{}
	if err := c.ShouldBindJSON(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.ID)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectId, err := uuid.Parse(request.ProjectId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteNew, err := site.NewWithId(id, request.Name, request.Url, projectId, auth.UserId, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldSite *site.Site) (*site.Site, error) {
		return site.NewWithId(oldSite.Id(), siteNew.Name(), siteNew.Url(), siteNew.ProjectId(), oldSite.CreatedBy(), oldSite.CreatedAt(), siteNew.ModifiedAt())
	}

	siteResp, err := d.services.Site.UpdateSite(context.Background(), siteNew.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonSite.ToSiteResponse(siteResp))
}

// DeleteSite
// @Summary Удаление сайта.
// @Description Удаление сайта.
// @Tags site
// @Produce json
// @Param id path string true "Site ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /site/{id} [delete]
func (d *Delivery) DeleteSite(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Site.DeleteSite(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadSiteList
// @Summary Список сайтов.
// @Description Список сайтов.
// @Tags site
// @Produce json
// @Success 200			{object} 	jsonSite.SiteListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /site/ [get]
func (d *Delivery) ReadSiteList(c *gin.Context) {
	siteList, err := d.services.Site.ReadSiteList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteListResponse := make([]*jsonSite.SiteResponse, len(siteList))

	for index, site := range siteList {
		siteListResponse[index] = jsonSite.ToSiteResponse(site)
	}

	c.JSON(http.StatusOK, jsonSite.SiteListResponse{Sites: siteListResponse})
}

// ReadSitesOfProject
// @Summary Список сайтов проекта.
// @Description Список сайтов проекта.
// @Tags site
// @Produce json
// @Param id path string true "Site ID"
// @Success 200			{object} 	jsonSite.SiteListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /site/project/{id} [get]
func (d *Delivery) ReadSitesOfProjet(c *gin.Context) {
	id := c.Param("id")
	projectId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	siteList, err := d.services.Site.ReadSitesOfProject(context.Background(), projectId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteListResponse := make([]*jsonSite.SiteResponse, len(siteList))

	for index, site := range siteList {
		siteListResponse[index] = jsonSite.ToSiteResponse(site)
	}

	c.JSON(http.StatusOK, jsonSite.SiteListResponse{Sites: siteListResponse})
}
