package kviz

import (
	"time"

	"github.com/google/uuid"
)

type Kviz struct {
	id              uuid.UUID
	siteId          uuid.UUID
	name            string
	templateId      uuid.UUID
	background      string
	mainColor       string
	secondaryColor  string
	subTitle        string
	subTitleItems   string
	phoneStepTitle  string
	footerTitle     string
	phone           string
	politics        bool
	roistat         string
	advantagesTitle string
	photosTitle     string
	plansTitle      string
	resultStepText  string
	qoopler         bool
	dmpOne          string
	validatePhone   bool
	createdBy       uuid.UUID
	createdAt       time.Time
	modifiedAt      time.Time
}

func (c Kviz) Id() uuid.UUID {
	return c.id
}

func (c Kviz) SiteId() uuid.UUID {
	return c.siteId
}

func (c Kviz) Name() string {
	return c.name
}

func (c Kviz) TemplateId() uuid.UUID {
	return c.templateId
}

func (c Kviz) Background() string {
	return c.background
}

func (c *Kviz) SetBackground(background string) {
	c.background = background
}

func (c Kviz) MainColor() string {
	return c.mainColor
}

func (c Kviz) SecondaryColor() string {
	return c.secondaryColor
}

func (c Kviz) SubTitle() string {
	return c.subTitle
}

func (c Kviz) SubTitleItems() string {
	return c.subTitleItems
}

func (c Kviz) PhoneStepTitle() string {
	return c.phoneStepTitle
}

func (c Kviz) FooterTitle() string {
	return c.footerTitle
}

func (c Kviz) Phone() string {
	return c.phone
}

func (c Kviz) Politics() bool {
	return c.politics
}

func (c Kviz) Roistat() string {
	return c.roistat
}

func (c Kviz) AdvantageTitle() string {
	return c.advantagesTitle
}

func (c Kviz) PhotosTitle() string {
	return c.photosTitle
}

func (c Kviz) PlansTitle() string {
	return c.plansTitle
}

func (c Kviz) ResultStepText() string {
	return c.resultStepText
}

func (c Kviz) Qoopler() bool {
	return c.qoopler
}

func (c Kviz) DmpOne() string {
	return c.dmpOne
}

func (c Kviz) ValidatePhone() bool {
	return c.validatePhone
}

func (c Kviz) CreatedBy() uuid.UUID {
	return c.createdBy
}

func (c Kviz) CreatedAt() time.Time {
	return c.createdAt
}

func (c Kviz) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	siteId uuid.UUID,
	name string,
	templateId uuid.UUID,
	background string,
	mainColor string,
	secondaryColor string,
	subTitle string,
	subTitleItems string,
	phoneStepTitle string,
	footerTitle string,
	phone string,
	politics bool,
	roistat string,
	advantagesTitle string,
	photosTitle string,
	plansTitle string,
	resultStepText string,
	qoopler bool,
	dmpOne string,
	validatePhone bool,
	createdBy uuid.UUID,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Kviz, error) {
	return &Kviz{
		id:              id,
		siteId:          siteId,
		name:            name,
		templateId:      templateId,
		background:      background,
		mainColor:       mainColor,
		secondaryColor:  secondaryColor,
		subTitle:        subTitle,
		subTitleItems:   subTitleItems,
		phoneStepTitle:  phoneStepTitle,
		footerTitle:     footerTitle,
		phone:           phone,
		politics:        politics,
		roistat:         roistat,
		advantagesTitle: advantagesTitle,
		photosTitle:     photosTitle,
		plansTitle:      plansTitle,
		resultStepText:  resultStepText,
		qoopler:         qoopler,
		dmpOne:          dmpOne,
		validatePhone:   validatePhone,
		createdBy:       createdBy,
		createdAt:       createdAt,
		modifiedAt:      modifiedAt,
	}, nil
}

func New(
	siteId uuid.UUID,
	name string,
	templateId uuid.UUID,
	background string,
	mainColor string,
	secondaryColor string,
	subTitle string,
	subTitleItems string,
	phoneStepTitle string,
	footerTitle string,
	phone string,
	politics bool,
	roistat string,
	advantagesTitle string,
	photosTitle string,
	plansTitle string,
	resultStepText string,
	qoopler bool,
	dmpOne string,
	validatePhone bool,
	createdBy uuid.UUID,
) (*Kviz, error) {
	return &Kviz{
		id:              uuid.New(),
		siteId:          siteId,
		name:            name,
		templateId:      templateId,
		background:      background,
		mainColor:       mainColor,
		secondaryColor:  secondaryColor,
		subTitle:        subTitle,
		subTitleItems:   subTitleItems,
		phoneStepTitle:  phoneStepTitle,
		footerTitle:     footerTitle,
		phone:           phone,
		politics:        politics,
		roistat:         roistat,
		advantagesTitle: advantagesTitle,
		photosTitle:     photosTitle,
		plansTitle:      plansTitle,
		resultStepText:  resultStepText,
		qoopler:         qoopler,
		dmpOne:          dmpOne,
		validatePhone:   validatePhone,
		createdBy:       createdBy,
		createdAt:       time.Now(),
		modifiedAt:      time.Now(),
	}, nil
}
