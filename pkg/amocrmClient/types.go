package amocrmClient

import (
	"backend/pkg/amocrmClient/contact"
	"backend/pkg/amocrmClient/lead"
	"time"
)

type SendToUnsortedRequest struct {
	SourceUuid string           `json:"source_uid"`
	SourceName string           `json:"source_name"`
	PipelineId int              `json:"pipeline_id"`
	Embeded    UnsortedEmbeeded `json:"_embedded"`
	Metadata   UnsortedMetadata `json:"metadata"`
}

type UnsortedEmbeeded struct {
	Leads    []*lead.LeadUnsorted `json:"leads"`
	Contacts []*contact.Contact   `json:"contacts"`
}

type SendLeadComplexRequest struct {
	Leads           []*lead.Lead
	DontCheckRepeat bool
}

type UnsortedMetadata struct {
	FormId     string `json:"form_id"`
	FormName   string `json:"form_name"`
	FormPage   string `json:"form_page"`
	FormSentAt int    `json:"form_sent_at"`
}

type SendLeadComplexResponse struct {
	Code     int
	Result   []*AmoLeadComplexResponse
	Duration time.Duration
}

type AmoLeadComplexResponse struct {
	LeadId    int      `json:"id"`
	ContactId int      `json:"contact_id"`
	RequestId []string `json:"request_id"`
	Merged    bool     `json:"merged"`
}

type PipelinesListAmoResponse struct {
	Embeeded PipelinesListAmoResponseEmbeeded `json:"_embedded"`
}

type PipelinesListAmoResponseEmbeeded struct {
	Pipelines []*Pipeline `json:"pipelines"`
}

type Pipeline struct {
	Id        int              `json:"id"`
	Name      string           `json:"name"`
	IsMain    bool             `json:"is_main"`
	IsArchive bool             `json:"is_archive"`
	Embeeded  PipelineEmbedded `json:"_embedded"`
}

type PipelineEmbedded struct {
	Statuses []Status `json:"statuses"`
}

type Status struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	PipelineId int    `json:"pipeline_id"`
}

type RefreshTokenRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
	RefreshToken string `json:"refresh_token"`
	RedirectUri  string `json:"redirect_uri"`
}

type TokenWithAuthCodeRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
	Code         string `json:"code"`
	RedirectUri  string `json:"redirect_uri"`
}

type RefreshTokenResponse struct {
	TokenType    string `json:"token_type" binding:"required"`
	ExpiresIn    int    `json:"expires_in" binding:"required"`
	AccessToken  string `json:"access_token" binding:"required"`
	RefreshToken string `json:"refresh_token" binding:"required"`
}

/*contact search struct*/

type FilterResponse struct {
	Embedded FilterEmbedded `json:"_embedded" binding:"required"`
}

type FilterLink struct {
	EntityId int    `json:"to_entity_id" binding:"required"`
	Type     string `json:"to_entity_type" binding:"required"`
}

type FilterEmbedded struct {
	Contacts []FilterContactResponse `json:"contacts" binding:"required"`
	Links    []FilterLink            `json:"links" binding:"required"`
}

type UnsortedLeadSendResponse struct {
	Embeeded UnsortedResponse `json:"_embedded" binding:"required"`
}

type UnsortedResponse struct {
	Unsorted []UnsortedEmbeede `json:"unsorted" binding:"required"`
}

type UnsortedEmbeede struct {
	Embeeded UnsortedEmbeeded `json:"_embedded" binding:"required"`
}

type UnsortedResponseEmbeeded struct {
	Leads []UnsortedResponseLead `json:"leads" binding:"required"`
}

type UnsortedResponseLead struct {
	Id int `json:"id" binding:"required"`
}

type FilterContactResponse struct {
	Id int `json:"id" binding:"required"`
}
