package embeeded

import (
	"backend/pkg/amocrmClient/contact"
	"backend/pkg/amocrmClient/tag"
)

type Embedded struct {
	Contacts []*contact.Contact `json:"contacts"`
	Tags     []*tag.Tag         `json:"tags"`
}
