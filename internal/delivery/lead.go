package delivery

import (
	jsonLead "backend/internal/delivery/lead"
	"backend/internal/domain/lead"
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateLeadFromSite
// @Summary Создание лида по запросу с сайта.
// @Description Создание лида по запросу с сайта.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.CreateLeadFromSiteRequest true "Body"
// @Success 201			{object} 	jsonLead.LeadResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /lead/site [put]
func (d *Delivery) CreateLeadFromSite(c *gin.Context) {
	request := jsonLead.CreateLeadFromSiteRequest{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error("error parsing lead from site %s, host: %s, origin: %s : %s", c.Request.RemoteAddr, c.Request.Host, c.Request.Header.Get("Origin"), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	roistatVisitId, _ := getRoistatFromRequest(c)

	ip := c.ClientIP()
	userAgent := c.GetHeader("User-Agent")

	lead, err := lead.New(c.Request.Header.Get("Origin"), uuid.Nil, request.Name, request.Phone, request.Email, strings.Replace(request.Comment, ";", "", 100), roistatVisitId, ip, userAgent, request.UtmMedium, request.UtmContent, request.UtmTerm, request.UtmSource, request.UtmCampaign, request.Yclid, request.Gclid, request.Fbid, lead.Site, false, request.YandexClientId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Lead.CreateLead(context.Background(), lead)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonLead.ToLeadResponse(lead))
}

// ReadLeadsFiltredList
// @Summary Возвращает отфильтрованный список лидов.
// @Description Возвращает отфильтрованный список лидов.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.ReadLeadsFiltersListRequest true "Body"
// @Success 201			{object} 	jsonLead.ReadLeadsFiltredListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /lead/list [post]
func (d *Delivery) ReadLeadsFiltredList(c *gin.Context) {
	request := jsonLead.ReadLeadsFiltersListRequest{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error("error reading leads list %s : %s", c.Request.Header.Get("Origin"), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	filter := jsonLead.ToDomainFilter(&request.Filter)

	leads, count, err := d.services.Lead.ReadFiltredLeadList(context.Background(), filter, request.Offset, request.Limit)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	respList := make([]*jsonLead.LeadResponse, len(leads))

	for index, lead := range leads {
		respList[index] = jsonLead.ToLeadResponse(lead)
	}

	c.JSON(http.StatusCreated, jsonLead.ReadLeadsFiltredListResponse{TotalItems: count, Leads: respList})
}

// CreateLeadFromTilda
// @Summary Создание лида c тильды.
// @Description Создание лида c тильды.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.CreateLeadFromSiteRequest true "Body"
// @Success 201			{object} 	jsonLead.LeadResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /lead/tilda [put]
func (d *Delivery) CreateLeadFromTilda(c *gin.Context) {
	err := c.Request.ParseForm()
	if err != nil {
		d.logger.Error("error parsing lead from tilda %s : %s", c.Request.Header.Get("Origin"), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := &jsonLead.CreateLeadFromTildaRequest{}

	comment := ""
	//d.logger.Info("request parsed form tilda %v", c.Request.PostForm)
	for key, value := range c.Request.PostForm {
		switch key {
		case "Phone":
			request.Phone = value[0]
		case "Email":
			request.Email = value[0]
		case "Name":
			request.Name = value[0]
		case "COOKIES":
			request.Cookies = value[0]
		case "utm_medium":
			request.UtmMedium = value[0]
		case "utm_campaign":
			request.UtmCampaign = value[0]
		case "utm_term":
			request.UtmTerm = value[0]
		case "utm_content":
			request.UtmContent = value[0]
		case "utm_source":
			request.UtmSource = value[0]
		default:
			if key != "tranid" && key != "formid" {
				comment += fmt.Sprintf("%s:%v;", key, value)
			}
		}
	}

	cookies, err := parseCookiesFromTildaRequest(request.Cookies)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	roistatVisitId := 0
	if cookies["roistat_visit"] != "" {
		roistatVisitId, _ = strconv.Atoi(cookies["roistat_visit"])
	}

	d.logger.Info("tilda parsed cookies %v", cookies)
	lead, err := lead.New(c.Request.Referer(), uuid.Nil, request.Name, request.Phone, request.Email, comment, roistatVisitId, "", "", request.UtmMedium, request.UtmContent, request.UtmTerm, request.UtmSource, request.UtmCampaign, cookies["_ym_uid"], cookies["_ga"], cookies["fb_id"], lead.Tilda, false, 0)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Lead.CreateLead(context.Background(), lead)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonLead.ToLeadResponse(lead))
}

// LeadsStatisticsOfProject
// @Summary Получает статистику лидов по проекту.
// @Description Получает статистику лидов по проекту.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.LeadsStatisticsRequest true "Body"
// @Success 200			{object} 	jsonLead.LeadsStatisticsResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /lead/statistics [post]
func (d *Delivery) LeadsStatisticsOfProject(c *gin.Context) {
	request := jsonLead.LeadsStatisticsRequest{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error("error parsing leadsStatistics request : %s", err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	statistics, err := d.services.Lead.GetLeadsStatisticsOfProject(context.Background(), request.ProjectId, request.SiteUrl, request.From, request.To)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, jsonLead.ToStatisticsResponse(statistics))
}

// CreateLeadFromMarquiz
// @Summary Создание лида по запросу с марквиза.
// @Description Создание лида по запросу с марквиза.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.CreateLeadFromSiteRequest true "Body"
// @Success 201			{object} 	jsonLead.LeadResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /lead/marquiz [put]
func (d *Delivery) CreateLeadFromMarquiz(c *gin.Context) {
	request := jsonLead.CreateLeadFromMarquizRequqest{}

	if err := c.ShouldBind(&request); err != nil {
		//body, _ := io.ReadAll(c.Request.Body)
		d.logger.Error("error parsing lead from marquiz %s : %s", c.Request.Header.Get("Origin"), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	d.logger.Info("lead from marquiz %s", request.Extra.Site)

	comment := ""

	for _, answer := range request.Answers {
		comment += fmt.Sprintf("%s - %s;", answer.Question, answer.Answer)
	}

	roistatVisitId := 0
	if request.Extra.Cookies.RoistatVist != "" {
		roistatVisitId, _ = strconv.Atoi(request.Extra.Cookies.RoistatVist)
	}

	var yclientId int64
	yclientId, errC := strconv.ParseInt(request.Extra.Cookies.YandexClientId, 10, 64)
	if errC != nil {
		d.logger.Error("error parsing yclientId from marquiz val - %s : %s", request.Extra.Cookies.YandexClientId, errC.Error())
	}

	lead, err := lead.New(request.Extra.Site, uuid.Nil, request.Contacts.Name, request.Contacts.Phone, request.Contacts.Email, comment, roistatVisitId, request.Extra.Ip, "", request.Extra.Utm.Medium, request.Extra.Utm.Content, request.Extra.Utm.Term, request.Extra.Utm.Source, request.Extra.Utm.Campaign, request.Extra.Cookies.Yclid, request.Extra.Cookies.Gclid, request.Extra.Cookies.Fbid, lead.Marquiz, false, yclientId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Lead.CreateLead(context.Background(), lead)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonLead.ToLeadResponse(lead))
}

// CreateLeadFromFlexbe
// @Summary Создание лида по запросу с flexbe.
// @Description Создание лида по запросу с flexbe.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.CreateLeadFromSiteRequest true "Body"
// @Success 201			{object} 	jsonLead.LeadResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /lead/flexbe [post]
func (d *Delivery) CreateLeadFromFlexbe(c *gin.Context) {
	err := c.Request.ParseForm()
	if err != nil {
		d.logger.Error("error parsing form lead from flexbe : %s", err.Error())
	}
	answers := map[string]*jsonLead.FlexbeAnswerStruct{}
	for key, value := range c.Request.Form {
		if strings.Contains(key, "data[form_data]") && (strings.Contains(key, "[value]") || strings.Contains(key, "[name]")) {
			fieldId := strings.Replace(key, "data[form_data][", "", 1)

			if strings.Contains(key, "[name]") {
				fieldId = strings.Replace(key, "][name]", "", 1)
				if _, ok := answers[fieldId]; !ok {
					answers[fieldId] = &jsonLead.FlexbeAnswerStruct{}
				}
				answers[fieldId].Name = strings.Join(value, ",")
			}

			if strings.Contains(key, "[value]") {
				fieldId = strings.Replace(key, "][value]", "", 1)
				if _, ok := answers[fieldId]; !ok {
					answers[fieldId] = &jsonLead.FlexbeAnswerStruct{}
				}
				answers[fieldId].Value = strings.Join(value, ",")
			}

		}
	}
	comment := ""
	for _, value := range answers {
		if value.Name != "Name" && value.Name != "Phone" {
			comment += fmt.Sprintf("%s - %s; ", value.Name, value.Value)
		}
	}

	request := jsonLead.CreateLeadFromFlexbeRequest{}
	if err := c.Bind(&request); err != nil {
		d.logger.Error("error parsing lead from flexbe %s : %s", c.Request.Referer(), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var yclientId int64
	yclientId, errC := strconv.ParseInt(request.Data.Utm.YmClientId, 10, 64)
	if errC != nil {
		d.logger.Error("error parsing yclientId from flexbe val - %s : %s", request.Data.Utm.YmClientId, errC.Error())
	}

	d.logger.Info("parsed data from flexbe %v", request)
	url := request.SiteUrl
	if url == "" {
		url = request.Data.Site.Url
	}

	d.logger.Info("site %s", url)

	lead, err := lead.New(url, uuid.Nil, request.Data.Client.Name, request.Data.Client.Phone, request.Data.Client.Name, comment, 0, request.Data.Utm.Ip, "", request.Data.Utm.Medium, request.Data.Utm.Content, request.Data.Utm.Term, request.Data.Utm.Source, request.Data.Utm.Campaign, "", "", "", lead.Flexbe, false, yclientId)
	if err != nil {
		d.logger.Error("error creating entity lead from flexbe : %s", err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Lead.CreateLead(context.Background(), lead)
	if err != nil {
		d.logger.Error("error storing lead from flexbe : %s", err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonLead.ToLeadResponse(lead))
}

// ReadSpamFiltredList
// @Summary Возвращает отфильтрованный список спам лидов.
// @Description Возвращает отфильтрованный список спам лидов.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.ReadSpamLeadsFiltredList true "Body"
// @Success 200			{object} 	jsonLead.ReadSpamLeadsFiltredListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /lead/spam-list [post]
func (d *Delivery) ReadSpamFiltredList(c *gin.Context) {
	request := jsonLead.ReadSpamLeadsFiltredList{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error("error reading leads list %s : %s", c.Request.Header.Get("Origin"), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	filter := jsonLead.ToDomainSpamFilter(&request.Filter)

	leads, err := d.services.Lead.ReadSpamFiltredLeadList(context.Background(), filter)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	respList := make([]*jsonLead.LeadResponse, len(leads))

	for index, lead := range leads {
		respList[index] = jsonLead.ToLeadResponse(lead)
	}

	c.JSON(http.StatusCreated, jsonLead.ReadSpamLeadsFiltredListResponse{Leads: respList})
}

// ExcelSpamFiltredList
// @Summary Возвращает excel файл со списком спам лидов.
// @Description Возвращает excel файл со списком спам лидов.
// @Tags lead
// @Accept json
// @Produce json
// @Param data body jsonLead.ReadSpamLeadsFiltredList true "Body"
// @Success 200			file 		"ExcelFile"
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /lead/spam-list-xls [post]
func (d *Delivery) ExcelSpamFiltredList(c *gin.Context) {
	request := jsonLead.ReadSpamLeadsFiltredList{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error("error reading leads list %s : %s", c.Request.Header.Get("Origin"), err.Error())
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	filter := jsonLead.ToDomainSpamFilter(&request.Filter)

	fileName, err := d.services.Lead.GetSpamLeadListExcel(context.Background(), filter)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	defer os.Remove(fileName)

	c.File(fileName)
}

func getRoistatFromRequest(c *gin.Context) (visitId int, err error) {
	roistatCookie, err := c.Request.Cookie("roistat_visit")
	if err != nil {
		return
	}

	visitId, err = strconv.Atoi(roistatCookie.Value)
	return
}

func parseCookiesFromTildaRequest(cookies string) (result map[string]string, err error) {
	result = map[string]string{}
	if cookies == "" {
		return
	}

	cookieQuery, err := url.QueryUnescape(cookies)
	if err != nil {
		return result, fmt.Errorf("error string escaping: %s", err.Error())
	}

	cookieValuesList := strings.Split(cookieQuery, ";")

	for _, cookie := range cookieValuesList {
		split := strings.Split(cookie, "=")
		result[split[0]] = split[1]
	}

	return result, err
}
