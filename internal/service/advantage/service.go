package advantage

import (
	repo "backend/internal/repository"
	"backend/internal/service/advantage/adapters/repository"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository repository.Repository
	logger     logger.Interface
	options    Options
}

type Options struct {
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repo.Repository, logger logger.Interface, options Options) (*service, error) {
	service := &service{
		repository: repository.Advantage,
		logger:     logger,
	}

	service.SetOptions(options)
	return service, nil
}
