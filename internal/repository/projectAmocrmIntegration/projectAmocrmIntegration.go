package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/projectAmocrmIntegration/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "project_amocrm_integration"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"project_amocrm_integration_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateProjectAmocrmIntegration(ctx context.Context, projectAmocrmIntegration *integration.ProjectAmoIntegration) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsProjectAmocrmIntegration...).Values(projectAmocrmIntegration.Id(), projectAmocrmIntegration.ProjectId(), projectAmocrmIntegration.BaseUrl(), projectAmocrmIntegration.ClientId(), projectAmocrmIntegration.ClientSecret(), projectAmocrmIntegration.RedirectUri(), projectAmocrmIntegration.Token(), projectAmocrmIntegration.RefreshToken(), projectAmocrmIntegration.CreatedAt(), projectAmocrmIntegration.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateProjectAmocrmIntegration(ctx context.Context, id uuid.UUID, updateFn func(projectAmocrmIntegration *integration.ProjectAmoIntegration) (*integration.ProjectAmoIntegration, error)) (projectAmocrmIntegration *integration.ProjectAmoIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upProjectAmocrmIntegration, err := r.oneProjectAmocrmIntegrationTx(ctx, tx, id)
	if err != nil {
		return
	}

	projectAmocrmIntegrationNew, err := updateFn(upProjectAmocrmIntegration)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("project_id", projectAmocrmIntegrationNew.ProjectId()).Set("base_url", projectAmocrmIntegrationNew.BaseUrl()).Set("client_id", projectAmocrmIntegrationNew.ClientId()).Set("client_secret", projectAmocrmIntegrationNew.ClientSecret()).Set("redirect_uri", projectAmocrmIntegrationNew.RedirectUri()).Set("token", projectAmocrmIntegrationNew.Token()).Set("refresh_token", projectAmocrmIntegrationNew.RefreshToken()).Set("modified_at", projectAmocrmIntegrationNew.ModifiedAt()).Where("id = ?", projectAmocrmIntegrationNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return projectAmocrmIntegrationNew, nil
}

func (r *Repository) DeleteProjectAmocrmIntegration(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadProjectAmocrmIntegrationList(ctx context.Context) (projectAmocrmIntegrationsList []*integration.ProjectAmoIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsProjectAmocrmIntegration...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectAmocrmIntegration, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.ProjectAmocrmIntegration])
	if err != nil {
		return nil, err
	}

	projectAmocrmIntegrationsList = []*integration.ProjectAmoIntegration{}

	for _, projectAmocrmIntegration := range daoProjectAmocrmIntegration {
		projectAmocrmIntegrationIns, err := r.toDomainProjectAmocrmIntegration(&projectAmocrmIntegration)
		if err != nil {
			return nil, err
		}

		projectAmocrmIntegrationsList = append(projectAmocrmIntegrationsList, projectAmocrmIntegrationIns)
	}
	return
}

func (r *Repository) ReadProjectAmocrmIntegration(ctx context.Context, projectId uuid.UUID) (response *integration.ProjectAmoIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsProjectAmocrmIntegration...).From(tableName).Where("project_id = ?", projectId)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectAmocrmIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectAmocrmIntegration])
	if err != nil {
		return nil, err
	}
	return r.toDomainProjectAmocrmIntegration(&daoProjectAmocrmIntegration)
}

func (r *Repository) ReadProjectAmocrmIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (response *integration.ProjectAmoIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "project_integration"
	for _, column := range dao.ColumnsProjectAmocrmIntegration {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).LeftJoin(fmt.Sprintf("project as project on %s.project_id = project.id", prefix)).LeftJoin("site as site on site.project_id = project.id").From(fmt.Sprintf("%s as %s", tableName, prefix)).Where("site.id = ?", siteId)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectAmocrmIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectAmocrmIntegration])
	if err != nil {
		return nil, err
	}
	return r.toDomainProjectAmocrmIntegration(&daoProjectAmocrmIntegration)
}

func (r *Repository) ReadProjectAmoIntegrationBySiteId(ctx context.Context, siteId uuid.UUID) (projectAmoIntegration *integration.ProjectAmoIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "integration"
	for _, column := range dao.ColumnsProjectAmocrmIntegration {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From("site").LeftJoin("project as project on project.id = site.project_id").LeftJoin("project_amocrm_integration as integration on integration.project_id = project.id").Where("site.id = ?", siteId)

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectAmoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectAmocrmIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainProjectAmocrmIntegration(&daoProjectAmoIntegration)
}

func (r *Repository) oneProjectAmocrmIntegrationTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *integration.ProjectAmoIntegration, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsProjectAmocrmIntegration...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectAmocrmIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectAmocrmIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainProjectAmocrmIntegration(&daoProjectAmocrmIntegration)
}
