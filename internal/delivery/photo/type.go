package advantage

import (
	"time"
)

type CreatePhotoRequest struct {
	KvizId string `form:"kvizId"`
}

type PhotoListResponse struct {
	Photos []*PhotoResponse `json:"photosList"`
}

type PhotoResponse struct {
	ID         string    `json:"id"`
	KvizId     string    `json:"kvizId"`
	Photo      string    `json:"photo"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}
