package lead

import (
	"backend/internal/domain/lead"
	"time"

	"github.com/google/uuid"
)

type LeadsStatisticsRequest struct {
	ProjectId uuid.UUID `json:"projectId"`
	SiteUrl   string    `json:"siteUrl"`
	From      time.Time `json:"from"`
	To        time.Time `json:"to"`
}

type LeadsStatisticsResponse struct {
	ProjectId uuid.UUID                   `json:"projectId"`
	SiteUrl   string                      `json:"siteUrl"`
	Days      map[time.Time]*DayStatistic `json:"days"`
	From      time.Time                   `json:"from"`
	To        time.Time                   `json:"to"`
}

type DayStatistic struct {
	LeadsCount        uint64 `json:"leadsCount"`
	SiteLeadsCount    uint64 `json:"siteLeadsCount"`
	MarquizLeadsCount uint64 `json:"marquizLeadsCount"`
	TildaLeadsCount   uint64 `json:"tildaLeadsCount"`
	FlexbeLeadsCount  uint64 `json:"flexbeLeadsCount"`
	SpamLeadsCount    uint64 `json:"spamLeadsCount"`
}

func ToStatisticsResponse(leadsStatistics *lead.LeadsStatistics) *LeadsStatisticsResponse {
	days := map[time.Time]*DayStatistic{}
	for date, dayInp := range leadsStatistics.Days() {
		days[date] = ToDayResponse(dayInp)
	}
	return &LeadsStatisticsResponse{
		ProjectId: leadsStatistics.ProjectId(),
		SiteUrl:   leadsStatistics.SiteUrl(),
		Days:      days,
		From:      leadsStatistics.From(),
		To:        leadsStatistics.To(),
	}
}

func ToDayResponse(dayStatistic *lead.DayStatistic) *DayStatistic {
	return &DayStatistic{
		LeadsCount:        dayStatistic.LeadsCount(),
		SiteLeadsCount:    dayStatistic.SiteLeadsCount(),
		MarquizLeadsCount: dayStatistic.MarquizLeadsCount(),
		TildaLeadsCount:   dayStatistic.TildaLeadsCount(),
		FlexbeLeadsCount:  dayStatistic.FlexbeLeadsCount(),
		SpamLeadsCount:    dayStatistic.SpamLeadsCount(),
	}
}
