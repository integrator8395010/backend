package advantage

import (
	"time"
)

type CreateAdvantageRequest struct {
	KvizId string `form:"kvizId"`
	Title  string `form:"title"`
}

type AdvantageListResponse struct {
	Advantages []*AdvantageResponse `json:"advantageList"`
}

type AdvantageResponse struct {
	ID         string    `json:"id"`
	KvizId     string    `json:"kvizId"`
	Title      string    `json:"title"`
	Photo      string    `json:"photo"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type UpdateAdvantageRequest struct {
	ID     string `json:"id"`
	KvizId string `json:"url"`
	Title  string `json:"title"`
	Photo  string `json:"photo"`
}
