package integration

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateProjectBitrixIntegration(ctx context.Context, integration *integration.ProjectBitrixIntegration) (err error) {
	err = s.repository.ProjectBitrixIntegartion.CreateProjectBitrixIntegration(ctx, integration)
	if err != nil {
		s.logger.Error("error creating project bitrix integration: %s", err.Error())
		return err
	}

	return err
}

func (s *service) UpdateProjectBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.ProjectBitrixIntegration) (*integration.ProjectBitrixIntegration, error)) (integration *integration.ProjectBitrixIntegration, err error) {
	integration, err = s.repository.ProjectBitrixIntegartion.UpdateProjectBitrixIntegration(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error update project bitrix integration: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteProjectBitrixIntegration(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.ProjectBitrixIntegartion.DeleteProjectBitrixIntegration(ctx, id)
	if err != nil {
		s.logger.Error("error delete project bitrix integration: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadProjectBitrixIntegration(ctx context.Context, projectId uuid.UUID) (integration *integration.ProjectBitrixIntegration, err error) {
	integration, err = s.repository.ProjectBitrixIntegartion.ReadProjectBitrixIntegrationById(ctx, projectId)
	if err != nil {
		if err.Error() != "no rows in result set" {
			s.logger.Error("read project bitrix integration: %s", err.Error())
		}
		return
	}
	return
}
