package integration

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateAmocrmIntegration(ctx context.Context, integration *integration.AmocrmIntegration) (err error) {
	err = s.repository.AmoCrmIntegration.CreateAmocrmIntegration(ctx, integration)
	if err != nil {
		s.logger.Error("error update amocrmIntegration: %s", err.Error())
		return err
	}
	return nil
}

func (s *service) UpdateAmocrmIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.AmocrmIntegration) (*integration.AmocrmIntegration, error)) (integration *integration.AmocrmIntegration, err error) {
	integration, err = s.repository.AmoCrmIntegration.UpdateAmocrmIntegration(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error update amocrmIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteAmocrmIntegration(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.AmoCrmIntegration.DeleteAmocrmIntegration(ctx, id)
	if err != nil {
		s.logger.Error("error delete amocrmIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadAmocrmIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.AmocrmIntegration, err error) {
	integration, err = s.repository.AmoCrmIntegration.ReadAmocrmIntegrationOfSite(ctx, siteId)
	if err != nil {
		s.logger.Error("error read amocrmIntegration: %s", err.Error())
		return
	}
	return
}
