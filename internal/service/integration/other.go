package integration

import (
	"backend/internal/domain/integration"
	"backend/internal/domain/site"
	"context"

	"github.com/google/uuid"
)

// TODO - refactor with transactions
func (s *service) ReadIntegrationsOfSite(ctx context.Context, siteId uuid.UUID) (amocrm *integration.AmocrmIntegration, bitrix *integration.BitrixIntegration, mail *integration.MailIntegration, leadactiv *integration.LeadactivIntegration, err error) {
	amocrm, _ = s.repository.AmoCrmIntegration.ReadAmocrmIntegrationOfSite(ctx, siteId)
	bitrix, _ = s.repository.BitrixIntegration.ReadBitrixIntegrationOfSite(ctx, siteId)
	mail, _ = s.repository.MailIntegration.ReadMailIntegrationOfSite(ctx, siteId)
	leadactiv, _ = s.repository.LeadactivIntegration.ReadLeadactivIntegrationOfSite(ctx, siteId)
	return
}

func (s *service) ReadSitesOfProjectWithIntegrations(ctx context.Context, projectId uuid.UUID) ([]*site.Site, error) {
	res, err := s.repository.Site.ReadSitesOfProjectWithIntegrations(ctx, projectId)
	if err != nil {
		s.logger.Error("error read sites with integrations: %s", err.Error())
		return res, err
	}
	return res, nil
}

func (s *service) ReadSitesOfProjectWithoutIntegrations(ctx context.Context, projectId uuid.UUID) ([]*site.Site, error) {
	res, err := s.repository.Site.ReadSitesOfProjectWithoutIntegrations(ctx, projectId)
	if err != nil {
		s.logger.Error("error read sites with integrations: %s", err.Error())
		return res, err
	}
	return res, nil
}
