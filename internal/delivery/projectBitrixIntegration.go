package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"backend/internal/domain/integration"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateProjectBitrixIntegration
// @Summary Создать интеграцию bitrix для проекта.
// @Description Создать интеграцию bitrix для проекта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.CreateProjectBitrixIntegration true "Body"
// @Success 201			{object} 	jsonIntegration.ProjectBitrixIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/bitrix/project [put]
func (d *Delivery) CreateProjectBitrixIntegration(c *gin.Context) {
	request := jsonIntegration.CreateProjectBitrixIntegration{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectId, err := uuid.Parse(request.ProjectId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectBitrixIntegration, err := integration.NewProjectBitrixIntegration(projectId, request.BaseUrl)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.CreateProjectBitrixIntegration(context.Background(), projectBitrixIntegration)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToProjectBitrixIntegration(projectBitrixIntegration))
}

// UpdateProjectBitrixIntegration
// @Summary Обновить интеграцию bitrix для проекта.
// @Description Обновить интеграцию bitrix для проекта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.UpdateProjectBitrixIntegrationRequest true "Body"
// @Success 200			{object} 	jsonIntegration.ProjectBitrixIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/bitrix/project [post]
func (d *Delivery) UpdateProjectBitrixIntegration(c *gin.Context) {
	request := jsonIntegration.UpdateProjectBitrixIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectId, err := uuid.Parse(request.ProjectId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	updateFn := func(oldIntegration *integration.ProjectBitrixIntegration) (*integration.ProjectBitrixIntegration, error) {
		return integration.NewProjectBitrixIntegrationWithId(id, projectId, request.BaseUrl, oldIntegration.CreatedAt(), time.Now())
	}

	projectBitrixIntegration, err := d.services.Integration.UpdateProjectBitrixIntegration(context.Background(), id, updateFn)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToProjectBitrixIntegration(projectBitrixIntegration))
}

// DeleteProjectBitrixIntegration
// @Summary Удалить интеграцию проекта.
// @Description Удалить интеграцию проекта.
// @Tags integration
// @Produce json
// @Param id path string true "BitrixIntegration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/bitrix/project/{id} [delete]
func (d *Delivery) DeleteProjectBitrixIntegration(c *gin.Context) {
	id := c.Param("id")
	integrationId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.DeleteProjectBitrixIntegration(context.Background(), integrationId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadProjectBitrixIntegration
// @Summary Получить интеграцию bitrix для проекта.
// @Description Получить интеграцию bitrix для проекта.
// @Tags integration
// @Produce json
// @Param id path string true "BitrixIntegration project ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.ProjectBitrixIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/bitrix/project/{id} [get]
func (d *Delivery) ReadProjectBitrixIntegration(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectBitrixIntegration, err := d.services.Integration.ReadProjectBitrixIntegration(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToProjectBitrixIntegration(projectBitrixIntegration))
}
