package kviz

import (
	"backend/internal/domain/kviz"
	"backend/pkg/tools"
	"context"
	"fmt"

	"github.com/google/uuid"
)

func (s *service) CreateKviz(ctx context.Context, kviz *kviz.Kviz, photoFileName string, photoContent []byte) (kvizResp *kviz.Kviz, err error) {
	fileName, err := tools.SaveFileToFileStorage(photoFileName, photoContent)
	if err != nil {
		return nil, err
	}

	kviz.SetBackground(fileName)

	err = s.repository.Kviz.CreateKviz(ctx, kviz)
	if err != nil {
		s.logger.Error("error creating kviz: %s", err.Error())
		return
	}
	return
}

func (s *service) CreateKvizComplex(ctx context.Context, kviz *Kviz, advantages []*Advantage, plans []*Plan, photos [][]byte, user uuid.UUID) (kvizResp *kviz.Kviz, err error) {
	// TODO - объединить в одну транзакцию все

	domainKviz, err := s.toDomainKviz(kviz, user)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting: %s", err)
	}

	domainPlans, err := s.toDomainPlanList(plans, domainKviz.Id())
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting: %s", err)
	}

	domainAdvantages, err := s.toDomainAdvantageList(advantages, domainKviz.Id())
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting: %s", err)
	}

	domainPhotos, err := s.toDomainPhotosList(photos, domainKviz.Id())
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting: %s", err)
	}

	err = s.repository.Kviz.CreateKviz(ctx, domainKviz)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while storing kviz: %s", err)
	}

	for _, plan := range domainPlans {
		err = s.repository.Plan.CreatePlan(context.Background(), plan)
		if err != nil {
			return nil, fmt.Errorf("CreateKvizComplex error, while storing plan: %s", err)
		}
	}

	for _, advantage := range domainAdvantages {
		err = s.repository.Advantage.CreateAdvantage(context.Background(), advantage)
		if err != nil {
			return nil, fmt.Errorf("CreateKvizComplex error, while storing plan: %s", err)
		}
	}

	for _, photo := range domainPhotos {
		err = s.repository.Photo.CreatePhoto(context.Background(), photo)
		if err != nil {
			return nil, fmt.Errorf("CreateKvizComplex error, while storing plan: %s", err)
		}
	}

	return domainKviz, err
}

func (s *service) UpdateKviz(ctx context.Context, kvizInput *kviz.Kviz, photoFileName string, photoContent []byte) (*kviz.Kviz, error) {
	updateFn := func(oldKviz *kviz.Kviz) (*kviz.Kviz, error) {
		return kviz.NewWithId(oldKviz.Id(), kvizInput.SiteId(), kvizInput.Name(), defaultTemplate, kvizInput.Background(), kvizInput.MainColor(), kvizInput.SecondaryColor(), kvizInput.SubTitle(), kvizInput.SubTitleItems(), kvizInput.PhoneStepTitle(), kvizInput.FooterTitle(), kvizInput.Phone(), kvizInput.Politics(), kvizInput.Roistat(), kvizInput.AdvantageTitle(), kvizInput.PhotosTitle(), kvizInput.PlansTitle(), kvizInput.ResultStepText(), kvizInput.Qoopler(), kvizInput.DmpOne(), kvizInput.ValidatePhone(), oldKviz.CreatedBy(), oldKviz.CreatedAt(), kvizInput.ModifiedAt())
	}

	kviz, err := s.repository.Kviz.UpdateKviz(ctx, kvizInput.Id(), updateFn)
	if err != nil {
		s.logger.Error("error updating kviz: %s", err.Error())
		return nil, err
	}
	return kviz, err
}

func (s *service) DeleteKviz(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.Kviz.DeleteKviz(ctx, id)
	if err != nil {
		s.logger.Error("error deleting kviz: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadKvizList(ctx context.Context) (kvizsList []*kviz.Kviz, err error) {
	kvizsList, err = s.repository.Kviz.ReadKvizList(ctx)
	if err != nil {
		s.logger.Error("error reading kviz list: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadKvizListOfProject(ctx context.Context, projectId uuid.UUID) (kvizsList []*kviz.Kviz, err error) {
	kvizsList, err = s.repository.Kviz.ReadKvizListOfProject(ctx, projectId)
	if err != nil {
		s.logger.Error("error reading kviz list: %s", err.Error())
		return
	}
	return
}
