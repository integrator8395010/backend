package bitrixFiledAssociation

type BitrixFiledAssociation struct {
	leadField   string
	bitrixField string
}

func NewBitrixFieldAssociation(leadField string, bitrixField string) *BitrixFiledAssociation {
	return &BitrixFiledAssociation{
		leadField:   leadField,
		bitrixField: bitrixField,
	}
}

func (b BitrixFiledAssociation) LeadField() string {
	return b.leadField
}

func (b BitrixFiledAssociation) BitrixField() string {
	return b.bitrixField
}
