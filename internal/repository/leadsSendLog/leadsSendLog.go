package repository

import (
	"backend/internal/domain/lead"
	"backend/internal/repository/leadsSendLog/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "leads_send_log"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"leads_send_log_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateLeadSendLog(ctx context.Context, leadsSendLog *lead.LeadsSendLog) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsLeadsSendLog...).Values(leadsSendLog.Id(), leadsSendLog.LeadId(), leadsSendLog.IntegrationId(), leadsSendLog.IntegrationType(), leadsSendLog.ResponseId(), leadsSendLog.Source(), leadsSendLog.Comment(), leadsSendLog.Sended(), leadsSendLog.CreatedAt(), leadsSendLog.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateLeadSendLog(ctx context.Context, id uuid.UUID, updateFn func(leadsSendLog *lead.LeadsSendLog) (*lead.LeadsSendLog, error)) (leadsSendLog *lead.LeadsSendLog, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLeadsSendLog, err := r.oneLeadSendLogTx(ctx, tx, id)
	if err != nil {
		return
	}

	leadsSendLogNew, err := updateFn(upLeadsSendLog)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("lead_id", leadsSendLogNew.LeadId()).Set("integration_id", leadsSendLogNew.IntegrationId()).Set("integration_type", leadsSendLogNew.IntegrationType()).Set("response_id", leadsSendLogNew.ResponseId()).Set("source", leadsSendLogNew.Source()).Set("sended", leadsSendLogNew.Sended()).Set("comment", leadsSendLogNew.Comment()).Set("modified_at", leadsSendLogNew.ModifiedAt()).Where("id = ?", leadsSendLogNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return leadsSendLogNew, nil
}

func (r *Repository) DeleteLeadSendLog(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadLeadSendLogList(ctx context.Context) (leadsSendLogsList []*lead.LeadsSendLog, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLeadsSendLog...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLeadsSendLog, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.LeadsSendLog])
	if err != nil {
		return nil, err
	}

	leadsSendLogsList = []*lead.LeadsSendLog{}

	for _, leadsSendLog := range daoLeadsSendLog {
		leadsSendLogIns, err := r.toDomainLeadsSendLog(&leadsSendLog)
		if err != nil {
			return nil, err
		}

		leadsSendLogsList = append(leadsSendLogsList, leadsSendLogIns)
	}
	return
}

func (r *Repository) ReadLeadsSendLogByLeadIdAndTypeList(ctx context.Context, leadId uuid.UUID, integrationType lead.IntegrationType) (leadLog *lead.LeadsSendLog, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLeadsSendLog...).From(tableName).Where("lead_id = ? and integration_type = ?", leadId, integrationType)

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLeadsSendLog, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.LeadsSendLog])
	if err != nil {
		return nil, err
	}

	return r.toDomainLeadsSendLog(&daoLeadsSendLog)
}

func (r *Repository) oneLeadSendLogTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *lead.LeadsSendLog, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsLeadsSendLog...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLeadsSendLog, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.LeadsSendLog])
	if err != nil {
		return nil, err
	}

	return r.toDomainLeadsSendLog(&daoLeadsSendLog)
}
