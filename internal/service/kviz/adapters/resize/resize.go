package resize

type Resize interface {
	ResizeImage(imgBytes []byte, width int, height int) (bytesR []byte, err error)
}
