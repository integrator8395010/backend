package plan

import (
	"time"
)

type CreatePlanRequest struct {
	KvizId       string  `form:"kvizId"`
	Title        string  `form:"title"`
	Photo        string  `form:"photo"`
	Rooms        int     `form:"rooms"`
	TotalArea    float32 `form:"totalArea"`
	LivingArea   float32 `form:"livingArea"`
	KitchenArea  float32 `form:"kitchenArea"`
	BedRoomArea  float32 `form:"bedRoomArea"`
	BathRoomArea float32 `form:"bathRoomArea"`
	Price        int     `form:"price"`
}

type PlanListResponse struct {
	Plans []*PlanResponse `json:"planList"`
}

type PlanResponse struct {
	ID           string    `json:"id"`
	KvizId       string    `json:"kvizId"`
	Title        string    `json:"title"`
	Photo        string    `json:"photo"`
	Rooms        int       `json:"rooms"`
	TotalArea    float32   `json:"totalArea"`
	LivingArea   float32   `json:"livingArea"`
	KitchenArea  float32   `json:"kitchenArea"`
	BedRoomArea  float32   `json:"bedRoomArea"`
	BathRoomArea float32   `json:"bathRoomArea"`
	Price        int       `json:"price"`
	CreatedAt    time.Time `json:"createdAt"`
	ModifiedAt   time.Time `json:"modifiedAt"`
}

type UpdatePlanRequest struct {
	ID           string  `json:"id"`
	KvizId       string  `json:"kvizId"`
	Title        string  `json:"title"`
	Photo        string  `json:"photo"`
	Rooms        int     `json:"rooms"`
	TotalArea    float32 `json:"totalArea"`
	LivingArea   float32 `json:"livingArea"`
	KitchenArea  float32 `json:"kitchenArea"`
	BedRoomArea  float32 `json:"bedRoomArea"`
	BathRoomArea float32 `json:"bathRoomArea"`
	Price        int     `json:"price"`
}
