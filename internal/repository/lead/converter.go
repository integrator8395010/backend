package repository

import (
	"backend/internal/domain/lead"
	"backend/internal/repository/lead/dao"
)

func (r *Repository) toDomainLead(dao *dao.Lead) (*lead.Lead, error) {
	return lead.NewWithId(dao.Id, dao.Url, dao.SiteId, dao.Name, dao.Phone, dao.Email, dao.Comment, dao.Rositat, dao.Ip, dao.UserAgent, dao.UtmMedium, dao.UtmContent, dao.UtmTerm, dao.UtmSource, dao.UtmCampaign, dao.Yclid, dao.Gclid, dao.Fbid, lead.Source(dao.Source), dao.Spam, dao.YandexClientId, dao.CreatedAt, dao.ModifiedAt)
}
