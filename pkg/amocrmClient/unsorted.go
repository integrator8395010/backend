package amocrmClient

import (
	"backend/pkg/amocrmClient/contact/customField"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
)

const (
	sendUnsorted = "/api/v4/leads/unsorted/forms"
)

func (c *Client) SendUnsorted(embeeded *UnsortedEmbeeded, pipelineId int) (responseId int, err error) {
	phone := ""
	switch embeeded.Contacts[0].CustomFieldValues[0].(type) {
	case customField.CustomFieldValue:
		phone = embeeded.Contacts[0].CustomFieldValues[0].(customField.CustomFieldValue).Values[0].Value
	default:
		phone = embeeded.Contacts[0].CustomFieldValues[0].(customField.CustomField).Values[0].Value
	}

	responseId, duplicate, err := c.checkContact(phone, embeeded.Leads[0].ResponsibleUserId, embeeded.Leads[0].Text)
	if err != nil {
		return 0, fmt.Errorf("error check on duplicate: %s", err.Error())
	}

	if duplicate {
		return responseId, nil
	}

	return c.doSendLeadUnsorted(embeeded, pipelineId)
}

func (c *Client) doSendLeadUnsorted(embeeded *UnsortedEmbeeded, pipelineId int) (responseId int, err error) {

	request := &[]SendToUnsortedRequest{{
		SourceUuid: uuid.New().String(),
		SourceName: "lead_from_leadactiv",
		PipelineId: pipelineId,
		Embeded:    *embeeded,
		Metadata: UnsortedMetadata{
			FormId:     uuid.New().String(),
			FormName:   "lead_from_leadactiv",
			FormPage:   "https://leadactiv.ru/",
			FormSentAt: int(time.Now().Unix()),
		},
	}}

	response := &UnsortedLeadSendResponse{}

	code, err := c.sendRequest(fmt.Sprintf("https://%s%s", c.baseDomain, sendUnsorted), http.MethodPost, "application/json", request, &response)
	if err != nil || code != 200 {
		if err == nil {
			return 0, fmt.Errorf("bad response code from server: %d", code)
		}
		return 0, err
	}

	responseId = response.Embeeded.Unsorted[0].Embeeded.Leads[0].Id
	err = c.AddNoteToLead(responseId, embeeded.Leads[0].ResponsibleUserId, embeeded.Leads[0].Text)
	if err != nil {
		return 0, fmt.Errorf("error adding note to lead: %s", err.Error())
	}

	return
}
