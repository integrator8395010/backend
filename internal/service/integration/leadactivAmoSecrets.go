package integration

import (
	"backend/internal/domain/integration"
	"backend/pkg/amocrmClient"
	"context"
	"time"
)

const (
	leadactivAmoBaseUrl = "leadactiv.amocrm.ru"
)

func (s *service) ReadLeadactivAmoSecrets(ctx context.Context) (*integration.LeadactivAmoSecrets, error) {
	res, err := s.repository.LeadactivAmoSecrets.ReadLeadactivAmoSecrets(ctx)
	if err != nil {
		s.logger.Error("error ReadLeadactivAmoSecrets: %s", err.Error())
		return res, err
	}
	return res, nil
}

func (s *service) ProcessLeadactivAmocrmTokenRequestWithAuthorizationCode(ctx context.Context, authCode, referer string) (*integration.LeadactivAmoSecrets, error) {
	leadactivIntegration, err := s.repository.LeadactivAmoSecrets.ReadLeadactivAmoSecrets(ctx)
	if err != nil {
		return nil, err
	}
	redirectUri := "https://" + s.baseUrl + "/integration/amocrm/leadactiv/code"

	amocrmClient := amocrmClient.NewAmoClient(referer, leadactivIntegration.ClientId(), leadactivIntegration.ClientSecret(), redirectUri, "", "", s.logger)
	token, refreshToken, err := amocrmClient.RequestTokenWithAuthorizationCode(authCode)
	if err != nil {
		s.logger.Error("error ProcessAmocrmTokenRequestWithAuthorizationCode get token and refresh token with authorization code: %s", err.Error())
		return nil, err
	}

	updateFn := func(oldProjectIntegration *integration.LeadactivAmoSecrets) (*integration.LeadactivAmoSecrets, error) {
		return integration.NewLeadactivAmoSecretsWithId(oldProjectIntegration.Id(), leadactivAmoBaseUrl, oldProjectIntegration.ClientId(), oldProjectIntegration.ClientSecret(), redirectUri, token, refreshToken, oldProjectIntegration.CreatedAt(), time.Now())
	}

	res, err := s.repository.LeadactivAmoSecrets.UpdateLeadactivAmoSecrets(ctx, updateFn)
	if err != nil {
		s.logger.Error("error ProcessAmocrmTokenRequestWithAuthorizationCode update amocrmIntegration: %s", err.Error())
		return res, err
	}
	return res, nil
}

func (s *service) createLeadactivAmocrmIntegration(ctx context.Context, integration *integration.LeadactivAmoSecrets) (err error) {
	err = s.repository.LeadactivAmoSecrets.CreateLeadactivAmoSecrets(ctx, integration)
	if err != nil {
		s.logger.Error("error create ProjectamocrmIntegration: %s", err.Error())
		return
	}
	return
}

func (s *service) ProcessLeadactivAmocrmSecrets(ctx context.Context, clientId, clientSecret string) (*integration.LeadactivAmoSecrets, error) {
	_, err := s.repository.LeadactivAmoSecrets.ReadLeadactivAmoSecrets(ctx)
	if err != nil {
		amocrmIntegration, err := integration.NewLeadactivSecretsOauth(clientId, clientSecret)
		if err != nil {
			s.logger.Error("error ProcessLeadactivAmocrmSecrets: %s", err.Error())
			return nil, err
		}
		err = s.createLeadactivAmocrmIntegration(ctx, amocrmIntegration)
		return amocrmIntegration, err
	}

	updateFn := func(oldIntegration *integration.LeadactivAmoSecrets) (*integration.LeadactivAmoSecrets, error) {
		return integration.NewLeadactivAmoSecretsWithId(oldIntegration.Id(), leadactivAmoBaseUrl, clientId, clientSecret, "", "", "", oldIntegration.CreatedAt(), time.Now())
	}

	res, err := s.repository.LeadactivAmoSecrets.UpdateLeadactivAmoSecrets(ctx, updateFn)
	if err != nil {
		s.logger.Error("error ProcessLeadactivAmocrmSecrets update: %s", err.Error())
		return res, err
	}
	return res, nil
}
