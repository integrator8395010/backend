package advantage

import (
	"backend/internal/domain/advantage"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreateAdvantage(ctx context.Context, advantage *advantage.Advantage, inputFileName string, inputFileContent []byte) (*advantage.Advantage, error)
	UpdateAdvantage(ctx context.Context, id uuid.UUID, updateFn func(advantage *advantage.Advantage) (*advantage.Advantage, error)) (advantage *advantage.Advantage, err error)
	DeleteAdvantage(ctx context.Context, id uuid.UUID) (err error)
	ReadAdvantageList(ctx context.Context) (advantagesList []*advantage.Advantage, err error)
	ReadAdvantagesOfKviz(ctx context.Context, kvizId uuid.UUID) (advantagesList []*advantage.Advantage, err error)
}
