package lead

import (
	"backend/internal/repository"
	"backend/pkg/mail"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository  *repository.Repository
	logger      logger.Interface
	mailSender  mail.Sender
	options     Options
	whiteListIp []string
}

type Options struct {
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repo *repository.Repository, mailSender mail.Sender, whiteListIp []string, logger logger.Interface, options Options) (*service, error) {
	service := &service{
		repository:  repo,
		mailSender:  mailSender,
		logger:      logger,
		whiteListIp: whiteListIp,
	}

	service.SetOptions(options)
	return service, nil
}
