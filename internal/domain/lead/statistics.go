package lead

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

type LeadsStatistics struct {
	projectId  uuid.UUID
	siteUrl    string
	days       map[time.Time]*DayStatistic
	leadsCount uint64
	from       time.Time
	to         time.Time
}

func (l LeadsStatistics) ProjectId() uuid.UUID {
	return l.projectId
}

func (l LeadsStatistics) SiteUrl() string {
	return l.siteUrl
}

func (l LeadsStatistics) LeadsCount() uint64 {
	return l.leadsCount
}

func (l *LeadsStatistics) IncLeadsCount() uint64 {
	return l.leadsCount
}

func (l LeadsStatistics) Days() map[time.Time]*DayStatistic {
	return l.days
}

func (l *LeadsStatistics) AddDay(day time.Time) {
	if _, ok := l.days[day]; ok {
		return
	}
	l.days[day] = NewDayStatistics()
}

func (l *LeadsStatistics) GetDay(day time.Time) (*DayStatistic, error) {
	if _, ok := l.days[day]; !ok {
		return nil, fmt.Errorf("no day founded")
	}
	return l.days[day], nil
}

func (l LeadsStatistics) From() time.Time {
	return l.from
}

func (l LeadsStatistics) To() time.Time {
	return l.to
}

func NewLeadsStatistics(projectId uuid.UUID, siteUrl string, from time.Time, to time.Time) (*LeadsStatistics, error) {
	duration := to.Sub(from)
	daysCount := duration.Hours() / 24
	if daysCount == 0 {
		return nil, fmt.Errorf("no days in range")
	}
	daysList := map[time.Time]*DayStatistic{}
	for i := 0; i < int(daysCount); i++ {
		day := CreateStartOfTheDay(from.Year(), int(from.Month()), from.Day())

		daysList[day.Add(time.Hour*24*time.Duration(i))] = NewDayStatistics()
	}
	return &LeadsStatistics{
		projectId: projectId,
		siteUrl:   siteUrl,
		days:      daysList,
		from:      from,
		to:        to,
	}, nil
}

func CreateStartOfTheDay(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}
