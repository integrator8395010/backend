package step

import "backend/internal/domain/step"

func ToStepResponse(step *step.Step) *StepResponse {
	return &StepResponse{
		ID:         step.Id().String(),
		KvizId:     step.KvizId().String(),
		Title:      step.Title(),
		Answers:    step.Answers(),
		CreatedAt:  step.CreatedAt(),
		ModifiedAt: step.ModifiedAt(),
	}
}
