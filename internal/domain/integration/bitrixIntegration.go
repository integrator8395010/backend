package integration

import (
	"backend/internal/domain/integration/bitrixDefaultValue"
	"backend/internal/domain/integration/bitrixFiledAssociation"
	"time"

	"github.com/google/uuid"
)

type SendType string

const (
	Lead SendType = "lead"
	Deal SendType = "deal"
)

type BitrixIntegration struct {
	id            uuid.UUID
	siteId        uuid.UUID
	responsible   int
	sendType      SendType
	associations  []*bitrixFiledAssociation.BitrixFiledAssociation
	defaultValues []*bitrixDefaultValue.BitrixDefaultValue
	createdAt     time.Time
	modifiedAt    time.Time
}

func NewBitrixIntegrationWithId(
	id uuid.UUID,
	siteId uuid.UUID,
	responsible int,
	sendType SendType,
	createdAt time.Time,
	modifiedAt time.Time,
	associations []*bitrixFiledAssociation.BitrixFiledAssociation,
	defaultValues []*bitrixDefaultValue.BitrixDefaultValue) (*BitrixIntegration, error) {
	return &BitrixIntegration{
		id:            id,
		siteId:        siteId,
		responsible:   responsible,
		sendType:      sendType,
		associations:  associations,
		defaultValues: defaultValues,
		createdAt:     createdAt,
		modifiedAt:    modifiedAt,
	}, nil
}

func NewBitrixIntegration(
	siteId uuid.UUID,
	responsible int,
	sendType SendType,
	associations []*bitrixFiledAssociation.BitrixFiledAssociation,
	defaultValues []*bitrixDefaultValue.BitrixDefaultValue) (*BitrixIntegration, error) {
	return &BitrixIntegration{
		id:            uuid.New(),
		siteId:        siteId,
		responsible:   responsible,
		sendType:      sendType,
		associations:  associations,
		defaultValues: defaultValues,
		createdAt:     time.Now(),
		modifiedAt:    time.Now(),
	}, nil
}

func (m BitrixIntegration) Id() uuid.UUID {
	return m.id
}

func (m BitrixIntegration) SiteId() uuid.UUID {
	return m.siteId
}

func (m BitrixIntegration) Responsible() int {
	return m.responsible
}

func (m BitrixIntegration) SendType() SendType {
	return m.sendType
}

func (m BitrixIntegration) Associations() []*bitrixFiledAssociation.BitrixFiledAssociation {
	return m.associations
}

func (m *BitrixIntegration) AddAssociations(associations ...*bitrixFiledAssociation.BitrixFiledAssociation) {
	m.associations = append(m.associations, associations...)
}

func (m BitrixIntegration) DefaultValues() []*bitrixDefaultValue.BitrixDefaultValue {
	return m.defaultValues
}

func (m *BitrixIntegration) AddDefaultValues(defaultValues ...*bitrixDefaultValue.BitrixDefaultValue) {
	m.defaultValues = append(m.defaultValues, defaultValues...)
}

func (m BitrixIntegration) CreatedAt() time.Time {
	return m.createdAt
}

func (m BitrixIntegration) ModifiedAt() time.Time {
	return m.modifiedAt
}
