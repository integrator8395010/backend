package excel

import (
	"strconv"

	"github.com/xuri/excelize/v2"
)

type Excel interface {
	CreateExcelFile(filename string, data [][]string) (err error)
}

type excel struct {
}

func NewExcelService() Excel {
	return &excel{}
}

func (e *excel) CreateExcelFile(filename string, data [][]string) (err error) {
	f := excelize.NewFile()
	for rowIndex, values := range data {
		for cellIndex, value := range values {
			f.SetCellValue("Sheet1", toChar(cellIndex+1)+strconv.Itoa(rowIndex+1), value)
		}
	}
	return f.SaveAs(filename)
}

func toChar(i int) string {
	return string(rune('A' - 1 + i))
}
