package dao

import (
	"time"

	"github.com/google/uuid"
)

type Site struct {
	Id         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	Url        string    `db:"url"`
	ProjectId  uuid.UUID `db:"project_id"`
	CreatedBy  uuid.UUID `db:"created_by"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsSite = []string{
	"id",
	"name",
	"url",
	"project_id",
	"created_by",
	"created_at",
	"modified_at",
}
