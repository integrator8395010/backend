package amocrmClient

import (
	"fmt"
	"time"
)

func (c *Client) checkContact(phone string, responsible int, comment string) (responseId int, duplicate bool, err error) {
	contacts, _ := c.searchContactByPhone(phone)

	responseId = 0
	for _, contactId := range contacts {
		leads, err := c.getLeadsLinkedToContact(contactId)
		if err != nil {
			return 0, false, err
		}
		if len(leads) > 0 {
			responseId = leads[0]
			break
		}
	}

	if responseId != 0 {
		duplicate = true
		err = c.AddNoteToLead(responseId, responsible, comment)
		if err != nil {
			return 0, false, fmt.Errorf("error adding note to lead: %s", err.Error())
		}

		err = c.AddTask(responseId, "leads", responsible, "Повторная сделка", int(time.Now().Add(time.Hour*72).Unix()))
		if err != nil {
			return 0, false, fmt.Errorf("error adding task for lead: %s", err.Error())
		}

	}
	return
}
