package repository

import (
	"backend/internal/domain/site"
	"backend/internal/repository/site/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "site"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"site_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateSite(ctx context.Context, site *site.Site) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsSite...).Values(site.Id(), site.Name(), site.Url(), site.ProjectId(), site.CreatedBy(), site.CreatedAt(), site.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateSite(ctx context.Context, id uuid.UUID, updateFn func(site *site.Site) (*site.Site, error)) (site *site.Site, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upSite, err := r.oneSiteTx(ctx, tx, id)
	if err != nil {
		return
	}

	siteNew, err := updateFn(upSite)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", siteNew.Name()).Set("url", siteNew.Url()).Set("project_id", siteNew.ProjectId()).Set("created_by", siteNew.CreatedBy()).Set("modified_at", siteNew.ModifiedAt()).Where("id = ?", siteNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return siteNew, err
}

func (r *Repository) DeleteSite(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) ReadSiteList(ctx context.Context) (sitesList []*site.Site, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsSite...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoSite, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Site])
	if err != nil {
		return nil, err
	}

	sitesList = []*site.Site{}

	for _, site := range daoSite {
		siteIns, err := r.toDomainSite(&site)
		if err != nil {
			return nil, err
		}

		sitesList = append(sitesList, siteIns)
	}

	return
}

func (r *Repository) ReadSiteById(ctx context.Context, siteId uuid.UUID) (site *site.Site, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	site, err = r.oneSiteTx(ctx, tx, siteId)
	return
}

func (r *Repository) ReadSitesOfProject(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsSite...).From(tableName).Where("project_id = ?", projectId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoSite, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Site])
	if err != nil {
		return nil, err
	}

	sitesList = []*site.Site{}

	for _, site := range daoSite {
		siteIns, err := r.toDomainSite(&site)
		if err != nil {
			return nil, err
		}

		sitesList = append(sitesList, siteIns)
	}

	return
}

func (r *Repository) ReadSitesOfProjectWithIntegrations(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefix := make([]string, len(dao.ColumnsSite))

	for index, field := range dao.ColumnsSite {
		columnsWithPrefix[index] = "site." + field
	}

	rawQuery := r.Builder.Select(columnsWithPrefix...).Distinct().From(tableName).JoinClause("FULL OUTER JOIN amocrm_integration as amocrm_integration on amocrm_integration.site_id = site.id").JoinClause("FULL OUTER JOIN mail_integration as mail_integration on mail_integration.site_id = site.id").JoinClause("FULL OUTER JOIN bitrix_integration as bitrix_integration on bitrix_integration.site_id = site.id").Where("site.project_id = ? and (bitrix_integration.id is not null or amocrm_integration.id is not null or mail_integration.id is not null)", projectId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoSite, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Site])
	if err != nil {
		return nil, err
	}

	sitesList = []*site.Site{}

	for _, site := range daoSite {
		siteIns, err := r.toDomainSite(&site)
		if err != nil {
			return nil, err
		}

		sitesList = append(sitesList, siteIns)
	}

	return
}

func (r *Repository) ReadSitesOfProjectWithoutIntegrations(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefix := make([]string, len(dao.ColumnsSite))

	for index, field := range dao.ColumnsSite {
		columnsWithPrefix[index] = "site." + field
	}

	rawQuery := r.Builder.Select(columnsWithPrefix...).Distinct().From(tableName).JoinClause("FULL OUTER JOIN amocrm_integration as amocrm_integration on amocrm_integration.site_id = site.id").JoinClause("FULL OUTER JOIN mail_integration as mail_integration on mail_integration.site_id = site.id").JoinClause("FULL OUTER JOIN bitrix_integration as bitrix_integration on bitrix_integration.site_id = site.id").Where("site.project_id = ? and bitrix_integration.id is null and amocrm_integration.id is null and mail_integration.id is null", projectId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoSite, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Site])
	if err != nil {
		return nil, err
	}

	sitesList = []*site.Site{}

	for _, site := range daoSite {
		siteIns, err := r.toDomainSite(&site)
		if err != nil {
			return nil, err
		}

		sitesList = append(sitesList, siteIns)
	}

	return
}

func (r *Repository) oneSiteTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *site.Site, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsSite...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoSite, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Site])
	if err != nil {
		return nil, err
	}

	return r.toDomainSite(&daoSite)
}
