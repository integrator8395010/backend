package project

import "backend/internal/domain/project"

func ToProjectResponse(project *project.Project) *ProjectResponse {
	return &ProjectResponse{
		ID:         project.Id().String(),
		Name:       project.Name(),
		Photo:      project.Photo(),
		CreatedBy:  project.CreatedBy(),
		CreatedAt:  project.CreatedAt(),
		ModifiedAt: project.ModifiedAt(),
	}
}
