package dao

import (
	"time"

	"github.com/google/uuid"
)

type LeadsSendLog struct {
	Id              uuid.UUID `db:"id"`
	LeadId          uuid.UUID `db:"lead_id"`
	IntegrationId   uuid.UUID `db:"integration_id"`
	IntegrationType string    `db:"integration_type"`
	ResponseId      int       `db:"response_id"`
	Source          string    `db:"source"`
	Comment         string    `db:"comment"`
	Sended          bool      `db:"sended"`
	CreatedAt       time.Time `db:"created_at"`
	ModifiedAt      time.Time `db:"modified_at"`
}

var ColumnsLeadsSendLog = []string{
	"id",
	"lead_id",
	"integration_id",
	"integration_type",
	"response_id",
	"source",
	"comment",
	"sended",
	"created_at",
	"modified_at",
}
