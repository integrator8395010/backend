package service

import (
	"backend/internal/repository"
	"backend/internal/service/advantage"
	"backend/internal/service/integration"
	"backend/internal/service/kviz"
	"backend/internal/service/lead"
	"backend/internal/service/photo"
	"backend/internal/service/plan"
	"backend/internal/service/project"
	"backend/internal/service/site"
	"backend/internal/service/step"
	"backend/internal/service/user"
	"backend/pkg/mail"
	"time"

	"gitlab.com/kanya384/gotools/imageResizer"
	"gitlab.com/kanya384/gotools/logger"
)

const (
	tokenTTLDefault = time.Hour * 2
)

type Service struct {
	Advantage   advantage.Service
	Kviz        kviz.Service
	Photo       photo.Service
	Plan        plan.Service
	Lead        lead.Service
	Project     project.Service
	Site        site.Service
	Step        step.Service
	Auth        user.Service
	Integration integration.Integration
}

func NewServices(mailSender mail.Sender, repository *repository.Repository, logger logger.Interface, tokenSecret string, passSalt string, baseUrl string, whiteListIp []string) (*Service, error) {
	resizer := imageResizer.NewImageResizer()

	advantage, err := advantage.New(repository, logger, advantage.Options{})
	if err != nil {
		return nil, err
	}

	kviz, err := kviz.New(repository, logger, resizer, kviz.Options{})
	if err != nil {
		return nil, err
	}

	lead, err := lead.New(repository, mailSender, whiteListIp, logger, lead.Options{})
	if err != nil {
		return nil, err
	}

	plan, err := plan.New(repository, logger, plan.Options{})
	if err != nil {
		return nil, err
	}

	project, err := project.New(repository, logger, project.Options{})
	if err != nil {
		return nil, err
	}

	site, err := site.New(repository, logger, site.Options{})
	if err != nil {
		return nil, err
	}

	step, err := step.New(repository, logger, step.Options{})
	if err != nil {
		return nil, err
	}

	photo, err := photo.New(repository, logger, photo.Options{})
	if err != nil {
		return nil, err
	}

	user, err := user.New(repository, tokenSecret, passSalt, logger, user.Options{
		TokenTTL: tokenTTLDefault,
	})
	if err != nil {
		return nil, err
	}

	integration, err := integration.New(repository, baseUrl, logger, integration.Options{})
	if err != nil {
		return nil, err
	}

	return &Service{
		advantage,
		kviz,
		photo,
		plan,
		lead,
		project,
		site,
		step,
		user,
		integration,
	}, nil
}
