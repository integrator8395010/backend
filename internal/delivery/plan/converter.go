package plan

import "backend/internal/domain/plan"

func ToPlanResponse(plan *plan.Plan) *PlanResponse {
	return &PlanResponse{
		ID:           plan.Id().String(),
		KvizId:       plan.KvizId().String(),
		Title:        plan.Title(),
		Photo:        plan.Photo(),
		Rooms:        plan.Rooms(),
		TotalArea:    plan.TotalArea(),
		LivingArea:   plan.LivingArea(),
		KitchenArea:  plan.KitchenArea(),
		BedRoomArea:  plan.BedRoomArea(),
		BathRoomArea: plan.BathRoomArea(),
		Price:        plan.Price(),
		CreatedAt:    plan.CreatedAt(),
		ModifiedAt:   plan.ModifiedAt(),
	}
}
