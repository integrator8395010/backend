package kviz

import (
	"backend/internal/domain/kviz"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreateKviz(ctx context.Context, kviz *kviz.Kviz, photoFileName string, photoContent []byte) (kvizResp *kviz.Kviz, err error)
	CreateKvizComplex(ctx context.Context, kviz *Kviz, advantages []*Advantage, plans []*Plan, photos [][]byte, user uuid.UUID) (kvizResp *kviz.Kviz, err error)
	UpdateKviz(ctx context.Context, kviz *kviz.Kviz, photoFileName string, photoContent []byte) (*kviz.Kviz, error)
	DeleteKviz(ctx context.Context, id uuid.UUID) (err error)
	ReadKvizList(ctx context.Context) (kvizsList []*kviz.Kviz, err error)
	ReadKvizListOfProject(ctx context.Context, projectId uuid.UUID) (kvizsList []*kviz.Kviz, err error)
}
