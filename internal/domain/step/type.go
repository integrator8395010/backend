package step

import (
	"time"

	"github.com/google/uuid"
)

type Step struct {
	id         uuid.UUID
	kvizId     uuid.UUID
	title      string
	answers    []*string
	createdAt  time.Time
	modifiedAt time.Time
}

func (c Step) Id() uuid.UUID {
	return c.id
}

func (c Step) KvizId() uuid.UUID {
	return c.kvizId
}

func (c Step) Title() string {
	return c.title
}

func (c Step) Answers() []*string {
	return c.answers
}

func (c Step) CreatedAt() time.Time {
	return c.createdAt
}

func (c Step) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	kvizId uuid.UUID,
	title string,
	answers []*string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Step, error) {
	return &Step{
		id:         id,
		kvizId:     kvizId,
		title:      title,
		answers:    answers,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	kvizId uuid.UUID,
	title string,
	answers []*string,
) (*Step, error) {
	return &Step{
		id:         uuid.New(),
		kvizId:     kvizId,
		title:      title,
		answers:    answers,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
