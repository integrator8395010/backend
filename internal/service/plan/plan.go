package plan

import (
	"backend/internal/domain/plan"
	"backend/pkg/tools"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreatePlan(ctx context.Context, plan *plan.Plan, inputFileName string, inputFileContent []byte) (*plan.Plan, error) {
	fileName, err := tools.SaveFileToFileStorage(inputFileName, inputFileContent)
	if err != nil {
		return nil, err
	}
	plan.SetPhoto(fileName)

	err = s.repository.CreatePlan(ctx, plan)
	if err != nil {
		s.logger.Error("error creating plan: %s", err.Error())
		return nil, err
	}
	return plan, err
}

func (s *service) UpdatePlan(ctx context.Context, planNew *plan.Plan, inputFileName string, inputFileContent []byte) (*plan.Plan, error) {
	fileName := ""
	if inputFileName != "" {
		newFile, err := tools.SaveFileToFileStorage(inputFileName, inputFileContent)
		if err != nil {
			return nil, err
		}
		fileName = newFile
	}

	upFn := func(oldPlan *plan.Plan) (*plan.Plan, error) {
		image := oldPlan.Photo()
		if fileName != "" {
			image = fileName
		}
		return plan.NewWithId(oldPlan.Id(), planNew.KvizId(), planNew.Title(), image, planNew.Rooms(), planNew.TotalArea(), planNew.LivingArea(), planNew.KitchenArea(), planNew.BedRoomArea(), planNew.BathRoomArea(), planNew.Price(), oldPlan.CreatedAt(), planNew.ModifiedAt())
	}

	plan, err := s.repository.UpdatePlan(ctx, planNew.Id(), upFn)
	if err != nil {
		s.logger.Error("error creating plan: %s", err.Error())
		return nil, err
	}
	return plan, err
}

func (s *service) DeletePlan(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.DeletePlan(ctx, id)
	if err != nil {
		s.logger.Error("error deleting plan: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadPlanList(ctx context.Context) (plansList []*plan.Plan, err error) {
	plansList, err = s.repository.ReadPlanList(ctx)
	if err != nil {
		s.logger.Error("error reading plans list: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadPlanListOfKviz(ctx context.Context, kvizId uuid.UUID) (plansList []*plan.Plan, err error) {
	plansList, err = s.repository.ReadPlanListOfKviz(ctx, kvizId)
	if err != nil {
		s.logger.Error("error reading plans list: %s", err.Error())
		return
	}
	return
}
