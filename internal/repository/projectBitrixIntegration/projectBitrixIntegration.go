package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/projectBitrixIntegration/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "project_bitrix_integration"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"project_bitrix_integration_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateProjectBitrixIntegration(ctx context.Context, projectBitrixIntegration *integration.ProjectBitrixIntegration) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsProjectBitrixIntegration...).Values(projectBitrixIntegration.Id(), projectBitrixIntegration.ProjectId(), projectBitrixIntegration.BaseUrl(), projectBitrixIntegration.CreatedAt(), projectBitrixIntegration.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateProjectBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(projectBitrixIntegration *integration.ProjectBitrixIntegration) (*integration.ProjectBitrixIntegration, error)) (projectBitrixIntegration *integration.ProjectBitrixIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upProjectBitrixIntegration, err := r.oneProjectBitrixIntegrationTx(ctx, tx, id)
	if err != nil {
		return
	}

	projectBitrixIntegrationNew, err := updateFn(upProjectBitrixIntegration)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("project_id", projectBitrixIntegrationNew.ProjectId()).Set("base_url", projectBitrixIntegrationNew.BaseUrl()).Set("modified_at", projectBitrixIntegrationNew.ModifiedAt()).Where("id = ?", projectBitrixIntegrationNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return projectBitrixIntegrationNew, nil
}

func (r *Repository) DeleteProjectBitrixIntegration(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadProjectBitrixIntegrationById(ctx context.Context, projectId uuid.UUID) (projectBitrixIntegrationsList *integration.ProjectBitrixIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsProjectBitrixIntegration...).From(tableName).Where("project_id = ?", projectId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectBitrixIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectBitrixIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainProjectBitrixIntegration(&daoProjectBitrixIntegration)
}

func (r *Repository) ReadProjectBitrixIntegrationBySiteId(ctx context.Context, siteId uuid.UUID) (projectBitrixIntegration *integration.ProjectBitrixIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsWithPrefixes := []string{}
	prefix := "integration"
	for _, column := range dao.ColumnsProjectBitrixIntegration {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).From("site").LeftJoin("project as project on project.id = site.project_id").LeftJoin("project_bitrix_integration as integration on integration.project_id = project.id").Where("site.id = ?", siteId)

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectBitrixIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectBitrixIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainProjectBitrixIntegration(&daoProjectBitrixIntegration)
}

func (r *Repository) oneProjectBitrixIntegrationTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *integration.ProjectBitrixIntegration, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsProjectBitrixIntegration...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoProjectBitrixIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ProjectBitrixIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainProjectBitrixIntegration(&daoProjectBitrixIntegration)
}
