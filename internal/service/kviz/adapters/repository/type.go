package repository

import (
	"backend/internal/domain/kviz"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreateKviz(ctx context.Context, kviz *kviz.Kviz) (err error)
	UpdateKviz(ctx context.Context, id uuid.UUID, updateFn func(kviz *kviz.Kviz) (*kviz.Kviz, error)) (kviz *kviz.Kviz, err error)
	DeleteKviz(ctx context.Context, id uuid.UUID) (err error)
	ReadKvizList(ctx context.Context) (kvizsList []*kviz.Kviz, err error)
	ReadKvizListOfProject(ctx context.Context, projectId uuid.UUID) (kvizsList []*kviz.Kviz, err error)
}
