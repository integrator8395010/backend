package repository

import (
	"backend/internal/domain/lead"
	"context"
	"time"

	"github.com/google/uuid"
)

type LeadsRepository interface {
	CreateLead(ctx context.Context, lead *lead.Lead) (err error)
	UpdateLead(ctx context.Context, id uuid.UUID, updateFn func(lead *lead.Lead) (*lead.Lead, error)) (lead *lead.Lead, err error)
	DeleteLead(ctx context.Context, id uuid.UUID) (err error)
	GetUnsendedLeadsToIntegrationOfSite(ctx context.Context, siteId uuid.UUID, integrationId uuid.UUID, integrationType lead.IntegrationType) (leads []*lead.Lead, err error)
	ReadLeadList(ctx context.Context) (leadsList []*lead.Lead, err error)
	ReadFiltredLeadList(ctx context.Context, filter *lead.LeadsFilter, offset, limit int) (list []*lead.Lead, err error)
	ReadSpamFiltredLeadList(ctx context.Context, filter *lead.SpamLeadsFilter) (list []*lead.Lead, err error)
	GetLeadsByIpCount(ctx context.Context, phone string, ip string, from time.Time) (count int, err error)
	GetLeadsByYandexClientIdCount(ctx context.Context, yandexClientId int, from time.Time) (count int, err error)
	GetLeadsOfProject(ctx context.Context, projectId uuid.UUID, url string, from time.Time, to time.Time) (leads []*lead.Lead, err error)
}

type LeadSendLogRepository interface {
	CreateLeadSendLog(ctx context.Context, leadsSendLog *lead.LeadsSendLog) (err error)
	UpdateLeadSendLog(ctx context.Context, id uuid.UUID, updateFn func(leadsSendLog *lead.LeadsSendLog) (*lead.LeadsSendLog, error)) (leadsSendLog *lead.LeadsSendLog, err error)
	DeleteLeadSendLog(ctx context.Context, id uuid.UUID) (err error)
	ReadLeadsSendLogByLeadIdAndTypeList(ctx context.Context, leadId uuid.UUID, integrationType lead.IntegrationType) (leadLog *lead.LeadsSendLog, err error)
	ReadLeadSendLogList(ctx context.Context) (leadsSendLogsList []*lead.LeadsSendLog, err error)
}
