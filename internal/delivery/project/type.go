package project

import (
	"time"

	"github.com/google/uuid"
)

type CreateProjectRequest struct {
	Name string `json:"name" form:"name" example:"Атлант"`
}

type ProjectListResponse struct {
	Projects []*ProjectResponse `json:"projectList"`
}

type ProjectResponse struct {
	ID         string    `json:"id"`
	Name       string    `json:"name"`
	Photo      string    `json:"photo"`
	CreatedBy  uuid.UUID `json:"createdBy"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type UpdateProjectRequest struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
