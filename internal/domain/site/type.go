package site

import (
	"time"

	"github.com/google/uuid"
)

type Site struct {
	id         uuid.UUID
	name       string
	url        string
	projectId  uuid.UUID
	createdBy  uuid.UUID
	createdAt  time.Time
	modifiedAt time.Time
}

func (c Site) Id() uuid.UUID {
	return c.id
}

func (c Site) Name() string {
	return c.name
}

func (c Site) Url() string {
	return c.url
}

func (c Site) ProjectId() uuid.UUID {
	return c.projectId
}

func (c Site) CreatedBy() uuid.UUID {
	return c.createdBy
}

func (c Site) CreatedAt() time.Time {
	return c.createdAt
}

func (c Site) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	name string,
	url string,
	projectId uuid.UUID,
	createdBy uuid.UUID,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Site, error) {
	return &Site{
		id:         id,
		name:       name,
		url:        url,
		projectId:  projectId,
		createdBy:  createdBy,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	name string,
	url string,
	projectId uuid.UUID,
	createdBy uuid.UUID,
) (*Site, error) {
	return &Site{
		id:         uuid.New(),
		name:       name,
		url:        url,
		projectId:  projectId,
		createdBy:  createdBy,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
