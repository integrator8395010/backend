package repository

import (
	"backend/internal/domain/plan"
	"backend/internal/repository/plan/dao"
)

func (r *Repository) toDomainPlan(dao *dao.Plan) (*plan.Plan, error) {
	return plan.NewWithId(dao.Id, dao.KvizId, dao.Title, dao.Photo, dao.Rooms, dao.TotalArea, dao.LivingArea, dao.KitchenArea, dao.BathRoomArea, dao.BathRoomArea, dao.Price, dao.CreatedAt, dao.ModifiedAt)
}
