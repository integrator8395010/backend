package dao

import (
	"time"

	"github.com/google/uuid"
)

type LeadactivIntegration struct {
	ID          uuid.UUID `db:"id"`
	SiteId      uuid.UUID `db:"site_id"`
	Responsible int       `db:"responsible"`
	CreatedAt   time.Time `db:"created_at"`
	ModifiedAt  time.Time `db:"modified_at"`
}

var ColumnsLeadactivIntegration = []string{
	"id",
	"site_id",
	"responsible",
	"created_at",
	"modified_at",
}
