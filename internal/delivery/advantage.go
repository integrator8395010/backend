package delivery

import (
	jsonAdvantage "backend/internal/delivery/advantage"
	"backend/internal/domain/advantage"
	"context"
	"io"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CrateAdvantage
// @Summary Создание преимущества для сайта.
// @Description Создание преимущества для сайта.
// @Tags advantage
// @Accept multipart/form-data
// @Produce json
// @Param data formData jsonAdvantage.CreateAdvantageRequest true "Body"
// @Param file formData file true "Photo of advantage"
// @Success 201			{object} 	jsonAdvantage.AdvantageResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /advantage/ [put]
func (d *Delivery) CreateAdvantage(c *gin.Context) {
	request := jsonAdvantage.CreateAdvantageRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	advantage, err := advantage.New(kvizId, request.Title, "")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var photoReader multipart.File
	photoReader, err = photo.Open()
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	defer photoReader.Close()

	photoContent, err := io.ReadAll(photoReader)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	advantageResp, err := d.services.Advantage.CreateAdvantage(context.Background(), advantage, photo.Filename, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonAdvantage.ToAdvantageResponse(advantageResp))
}

// UpdateAdvantage
// @Summary Обновление преимущества.
// @Description Обновление преимущества.
// @Tags advantage
// @Accept json
// @Produce json
// @Param data body jsonAdvantage.UpdateAdvantageRequest true "Body"
// @Success 200			{object} 	jsonAdvantage.AdvantageResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /advantage/ [post]
func (d *Delivery) UpdateAdvantage(c *gin.Context) {
	request := jsonAdvantage.UpdateAdvantageRequest{}
	if err := c.ShouldBindJSON(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.ID)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	kvizId, err := uuid.Parse(request.KvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	advantageNew, err := advantage.NewWithId(id, kvizId, request.Title, "", time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldAdvantage *advantage.Advantage) (*advantage.Advantage, error) {
		return advantage.NewWithId(oldAdvantage.Id(), advantageNew.KvizId(), advantageNew.Title(), advantageNew.Photo(), oldAdvantage.CreatedAt(), advantageNew.ModifiedAt())
	}

	advantageResp, err := d.services.Advantage.UpdateAdvantage(context.Background(), advantageNew.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonAdvantage.ToAdvantageResponse(advantageResp))
}

// DeleteAdvantage
// @Summary Удаление преимущества.
// @Description Удаление преимущества.
// @Tags advantage
// @Produce json
// @Param id path string true "Advantage ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /advantage/{id} [delete]
func (d *Delivery) DeleteAdvantage(c *gin.Context) {
	id := c.Param("id")
	advantageId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Advantage.DeleteAdvantage(context.Background(), advantageId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadAdvantageList
// @Summary Список преимуществ.
// @Description Список преимуществ.
// @Tags advantage
// @Produce json
// @Success 200			{object} 	jsonAdvantage.AdvantageListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /advantage/ [get]
func (d *Delivery) ReadAdvantageList(c *gin.Context) {
	advantageList, err := d.services.Advantage.ReadAdvantageList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	advantageListResponse := make([]*jsonAdvantage.AdvantageResponse, len(advantageList))

	for index, advantage := range advantageList {
		advantageListResponse[index] = jsonAdvantage.ToAdvantageResponse(advantage)
	}

	c.JSON(http.StatusOK, jsonAdvantage.AdvantageListResponse{Advantages: advantageListResponse})
}

// ReadAdvantageOfKviz
// @Summary Список преимуществ квиза.
// @Description Список преимуществ квиза.
// @Tags advantage
// @Produce json
// @Param id path string true "Plan ID"
// @Success 200			{object} 	jsonAdvantage.AdvantageListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /advantage/{id} [get]
func (d *Delivery) ReadAdvantageOfKviz(c *gin.Context) {
	id := c.Param("id")
	kvizId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	advantageList, err := d.services.Advantage.ReadAdvantagesOfKviz(context.Background(), kvizId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	advantageListResponse := make([]*jsonAdvantage.AdvantageResponse, len(advantageList))

	for index, advantage := range advantageList {
		advantageListResponse[index] = jsonAdvantage.ToAdvantageResponse(advantage)
	}

	c.JSON(http.StatusOK, jsonAdvantage.AdvantageListResponse{Advantages: advantageListResponse})
}
