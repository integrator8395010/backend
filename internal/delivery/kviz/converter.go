package kviz

import "backend/internal/domain/kviz"

func ToKvizResponse(kviz *kviz.Kviz) *KvizResponse {
	return &KvizResponse{
		ID:              kviz.Id().String(),
		SiteId:          kviz.SiteId().String(),
		Name:            kviz.Name(),
		TemplateId:      kviz.TemplateId().String(),
		Background:      kviz.Background(),
		MainColor:       kviz.MainColor(),
		SecondaryColor:  kviz.SecondaryColor(),
		SubTitle:        kviz.SubTitle(),
		SubTitleItems:   kviz.SubTitleItems(),
		PhoneStepTitle:  kviz.PhoneStepTitle(),
		FooterTitle:     kviz.FooterTitle(),
		Phone:           kviz.Phone(),
		Politics:        kviz.Politics(),
		Roistat:         kviz.Roistat(),
		AdvantagesTitle: kviz.AdvantageTitle(),
		PhotosTitle:     kviz.PhotosTitle(),
		PlansTitle:      kviz.PlansTitle(),
		ResultStepText:  kviz.ResultStepText(),
		Qoopler:         kviz.Qoopler(),
		DmpOne:          kviz.DmpOne(),
		ValidatePhone:   kviz.ValidatePhone(),
		CreatedAt:       kviz.CreatedAt(),
		ModifiedAt:      kviz.ModifiedAt(),
	}
}
