package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/leadactivAmoSecrets/dao"
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
)

const (
	tableName = "leadactiv_amo_secrets"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"leadactiv_amo_secrets_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateLeadactivAmoSecrets(ctx context.Context, integration *integration.LeadactivAmoSecrets) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsLeadactivAmoSecrets...).Values(integration.Id(), integration.BaseUrl(), integration.ClientId(), integration.ClientSecret(), integration.RedirectUri(), integration.Token(), integration.RefreshToken(), integration.CreatedAt(), integration.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateLeadactivAmoSecrets(ctx context.Context, updateFn func(integration *integration.LeadactivAmoSecrets) (*integration.LeadactivAmoSecrets, error)) (integration *integration.LeadactivAmoSecrets, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLeadactivAmoSecrets, err := r.oneLeadactivAmoSecretsTx(ctx, tx)
	if err != nil {
		return
	}

	leadactivAmoSecrets, err := updateFn(upLeadactivAmoSecrets)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("base_url", leadactivAmoSecrets.BaseUrl()).Set("client_id", leadactivAmoSecrets.ClientId()).Set("client_secret", leadactivAmoSecrets.ClientSecret()).Set("redirect_uri", leadactivAmoSecrets.RedirectUri()).Set("token", leadactivAmoSecrets.Token()).Set("refresh_token", leadactivAmoSecrets.RefreshToken()).Set("modified_at", leadactivAmoSecrets.ModifiedAt()).Where("id = ?", leadactivAmoSecrets.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return leadactivAmoSecrets, nil
}

func (r *Repository) DeleteLeadactivAmoSecrets(ctx context.Context) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadLeadactivAmoSecrets(ctx context.Context) (leadactivAmoSecrets *integration.LeadactivAmoSecrets, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	return r.oneLeadactivAmoSecretsTx(ctx, tx)
}

func (r *Repository) oneLeadactivAmoSecretsTx(ctx context.Context, tx pgx.Tx) (response *integration.LeadactivAmoSecrets, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsLeadactivAmoSecrets...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.LeadactivAmoSecrets])
	if err != nil {
		return nil, err
	}

	return r.toDomainAmocrmIntegration(&daoIntegration)
}
