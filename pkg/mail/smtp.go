package mail

import (
	"errors"
	"fmt"
	"net/smtp"
	"strings"
)

type Sender interface {
	SendEmailText(from string, to []string, subject, message string) (err error)
}

type sender struct {
	host string
	port int
	auth smtp.Auth
}

func NewSender(host string, port int, username, pass string) *sender {

	auth := smtp.PlainAuth("", username, pass, host)

	return &sender{
		auth: auth,
		host: host,
		port: port,
	}
}

func (s *sender) address() string {
	return fmt.Sprintf("%s:%d", s.host, s.port)
}

func (s *sender) SendEmailText(from string, to []string, subject, message string) (err error) {
	m := NewMessage()
	m.SetSubject(subject)
	m.SetFrom(from)
	m.SetContentType("text/html")
	m.SetBody(message)
	sendErrorMessages := []string{}
	for _, email := range to {
		m.SetTo(email)
		errM := smtp.SendMail(s.address(), s.auth, from, []string{email}, m.GenerateMessage())
		if errM != nil {
			sendErrorMessages = append(sendErrorMessages, fmt.Sprintf("not sended to %s: %s", email, errM.Error()))
		}
	}
	if len(sendErrorMessages) > 0 {
		return errors.New(strings.Join(sendErrorMessages, ","))
	}
	return
}

func (s *sender) SendEmail(m *message) (err error) {

	err = smtp.SendMail(s.address(), s.auth, m.From(), []string{m.To()}, m.GenerateMessage())
	return
}
