package repository

import (
	"backend/internal/domain/integration"
	"context"

	"github.com/google/uuid"
)

type AmocrmIntegrationRepository interface {
	CreateAmocrmIntegration(ctx context.Context, integration *integration.AmocrmIntegration) (err error)
	UpdateAmocrmIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.AmocrmIntegration) (*integration.AmocrmIntegration, error)) (integration *integration.AmocrmIntegration, err error)
	DeleteAmocrmIntegration(ctx context.Context, id uuid.UUID) (err error)
	ReadAmocrmIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.BitrixIntegration, err error)
	ReadAmocrmIntegrationById(ctx context.Context, id uuid.UUID) (integration *integration.AmocrmIntegration, err error)
	ReadAllSitesAmocrmIntegrations(ctx context.Context) (integrationList []*integration.AmocrmIntegration, err error)
}
