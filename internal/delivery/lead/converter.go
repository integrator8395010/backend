package lead

import "backend/internal/domain/lead"

func ToLeadResponse(lead *lead.Lead) *LeadResponse {
	return &LeadResponse{
		Id:             lead.Id(),
		Url:            lead.Url(),
		SiteId:         lead.SiteId().String(),
		Name:           lead.Name(),
		Phone:          lead.Phone(),
		Email:          lead.Email(),
		Comment:        lead.Comment(),
		Roistat:        lead.Roistat(),
		Ip:             lead.Ip(),
		UserAgent:      lead.UserAgent(),
		UtmMedium:      lead.UtmMedium(),
		UtmContent:     lead.UtmContent(),
		UtmTerm:        lead.UtmTerm(),
		UtmSource:      lead.UtmSource(),
		UtmCampaign:    lead.UtmCampaign(),
		Yclid:          lead.Yclid(),
		Gclid:          lead.Gclid(),
		FbId:           lead.Fbid(),
		Source:         string(lead.Source()),
		Spam:           lead.Spam(),
		YandexClientId: lead.YandexClientId(),
		CreatedAt:      lead.CreatedAt().String(),
		ModifiedAt:     lead.ModifiedAt().String(),
	}
}

func ToDomainFilter(leadsInput *LeadsFilter) *lead.LeadsFilter {
	filter := lead.NewLeadsFilter()

	filter.SetProjectId(leadsInput.ProjectId)
	filter.SetUrl(leadsInput.Url)
	filter.SetPhone(leadsInput.Phone)
	filter.SetSource(lead.Source(leadsInput.Source))
	filter.SetFrom(leadsInput.From)
	filter.SetTo(leadsInput.To)

	return filter
}

func ToDomainSpamFilter(leadsInput *LeadsSpamFilter) *lead.SpamLeadsFilter {
	filter := lead.NewSpamLeadsFilter()

	filter.SetProjectId(leadsInput.ProjectId)
	filter.SetUrl(leadsInput.Url)
	filter.SetFrom(leadsInput.From)
	filter.SetTo(leadsInput.To)

	return filter
}
