package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// ReadProjectAmocrmIntegrationOfSite
// @Summary Получить интеграции проекта с amocrm.
// @Description Получить интеграции проекта с amocrm.
// @Tags integration
// @Produce json
// @Param id path string true "Project ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.ProjectAmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/project/{id} [get]
func (d *Delivery) ReadProjectAmocrmIntegration(c *gin.Context) {
	id := c.Param("id")
	projectId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectIntegration, err := d.services.Integration.ReadProjectAmocrmIntegration(context.Background(), projectId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToProjectAmocrmIntegration(projectIntegration))
}

// CreateProjectAmocrmIntegrationOauthSecret
// @Summary Получаем секреты из oauth и обновляем или создаем интеграцию.
// @Description Получаем секреты из oauth и обновляем или создаем интеграцию.
// @Tags integration
// @Accept json
// @Produce json
// @Param id path string true "Project ID"
// @Param data body jsonIntegration.CreateAmocrmIntegrationOauthSecretRequest true "Body"
// @Success 200			{object} 	jsonIntegration.AmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/amocrm/project/secret/{id} [post]
func (d *Delivery) CreateProjectAmocrmIntegrationOauthSecret(c *gin.Context) {
	id := c.Param("id")

	siteId, err := uuid.Parse(id)
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthSecret parse id error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonIntegration.CreateAmocrmIntegrationOauthSecretRequest{}
	if err := c.ShouldBind(&request); err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthSecret json parse error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	integration, err := d.services.Integration.ProcessAmocrmSecrets(context.Background(), siteId, request.ClientId, request.ClientSecret)
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthSecret store error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToProjectAmocrmIntegration(integration))
}

// CreateProjectAmocrmIntegrationOauthCode
// @Summary Получаем код авторизации из oauth и обновляем или создаем интеграцию.
// @Description Получаем код авторизации из oauth и обновляем или создаем интеграцию.
// @Tags integration
// @Accept json
// @Produce json
// @Param id path string true "Project ID"
// @Param data query jsonIntegration.CreateAmocrmIntegrationOauthCodeRequest true "Body"
// @Success 200			{object} 	jsonIntegration.AmocrmIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/amocrm/project/code/{id} [get]
func (d *Delivery) CreateProjectAmocrmIntegrationOauthCode(c *gin.Context) {
	id := c.Param("id")

	siteId, err := uuid.Parse(id)
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthCode parse id error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonIntegration.CreateAmocrmIntegrationOauthCodeRequest{}
	if err := c.BindQuery(&request); err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthCode bind query error : %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	integration, err := d.services.Integration.ProcessAmocrmTokenRequestWithAuthorizationCode(context.Background(), siteId, request.Code, request.Referer)
	if err != nil {
		d.logger.Error(fmt.Sprintf("CreateAmocrmIntegrationOauthCode error processing: %s", err.Error()))
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToProjectAmocrmIntegration(integration))
}

// ReadAmocrmUsersList
// @Summary Получить список полей интеграции.
// @Description Получить список полей интеграции.
// @Tags integration
// @Produce json
// @Param id path string true "integration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/users/{id} [get]
func (d *Delivery) ReadAmocrmUsersList(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	fields, err := d.services.Integration.ReadAmocrmUsersList(context.Background(), id)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, fields)
}

// ReadAmocrmFieldsList
// @Summary Получить список полей интеграции.
// @Description Получить список полей интеграции.
// @Tags integration
// @Produce json
// @Param id path string true "integration ID"
// @Param page path string true "page"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/fields/{id}/{page} [get]
func (d *Delivery) ReadAmocrmFieldsList(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	page, err := strconv.Atoi(c.Param("page"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	fields, err := d.services.Integration.ReadAmocrmFieldsList(context.Background(), id, page)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, fields)
}

// ReadAmocrmPipelinesList
// @Summary Получить список воронок интеграции.
// @Description Получить список воронок интеграции.
// @Tags integration
// @Produce json
// @Param id path string true "integration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/pipelines/{id} [get]
func (d *Delivery) ReadAmocrmPipelinesList(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	fields, err := d.services.Integration.ReadAmocrmPipelinesList(context.Background(), id)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, fields)
}

// ReadAmocrmStatusesList
// @Summary Получить список статусов интеграции.
// @Description Получить список статусов интеграции.
// @Tags integration
// @Produce json
// @Param id path string true "integration ID"
// @Param pipelineId path string true "page"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/amocrm/statuses/{id}/{pipelineId} [get]
func (d *Delivery) ReadAmocrmStatusesList(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	pipelineId, err := strconv.Atoi(c.Param("pipelineId"))
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	fields, err := d.services.Integration.ReadAmocrmStatusesOfPipelineList(context.Background(), id, pipelineId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, fields)
}
