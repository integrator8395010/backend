package kviz

type Kviz struct {
	SiteId          string
	TemplateId      string
	Name            string
	Background      []byte
	MainColor       string
	SecondaryColor  string
	SubTitle        string
	SubTitleItems   string
	PhoneStepTitle  string
	FooterTitle     string
	Phone           string
	Politics        bool
	Roistat         string
	AdvantagesTitle string
	PhotosTitle     string
	PlansTitle      string
	ResultStepText  string
	Qoopler         bool
	DmpOne          string
	ValidatePhone   bool
}

type Advantage struct {
	Title string
	Photo []byte
}

type Plan struct {
	KvizId       string
	Title        string
	Photo        []byte
	Rooms        int
	TotalArea    float32
	LivingArea   float32
	KitchenArea  float32
	BedRoomArea  float32
	BathRoomArea float32
	Price        int
}
