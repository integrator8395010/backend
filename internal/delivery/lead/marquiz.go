package lead

type CreateLeadFromMarquizRequqest struct {
	Answers  []*MarquizAnswer `json:"answers"`
	Contacts *MarquizContact  `json:"contacts"`
	Extra    *MarquizExtra    `json:"extra"`
}

type MarquizAnswer struct {
	Question string      `json:"q"`
	Answer   interface{} `json:"a"`
}

type MarquizContact struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

type MarquizExtra struct {
	Site    string         `json:"href"`
	Utm     MarquizUtm     `json:"utm"`
	Ip      string         `json:"ip"`
	Cookies MarquizCookies `json:"cookies"`
}

type MarquizUtm struct {
	Source   string `json:"source"`
	Medium   string `json:"medium"`
	Campaign string `json:"name"`
	Content  string `json:"content"`
	Term     string `json:"term"`
}

type MarquizCookies struct {
	RoistatVist    string `json:"roistat_visit"`
	Gclid          string `json:"_ga"`
	Yclid          string `json:"yclid"`
	YandexClientId string `json:"_ym_uid"`
	Fbid           string `json:"fb_id"`
}
