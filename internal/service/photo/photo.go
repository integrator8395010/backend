package photo

import (
	"backend/internal/domain/photo"
	"backend/pkg/tools"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreatePhoto(ctx context.Context, photo *photo.Photo, inputFileName string, inputFileContent []byte) (*photo.Photo, error) {
	fileName, err := tools.SaveFileToFileStorage(inputFileName, inputFileContent)
	if err != nil {
		return nil, err
	}

	photo.SetPhoto(fileName)

	err = s.repository.CreatePhoto(ctx, photo)
	if err != nil {
		s.logger.Error("error creating photo: %s", err.Error())
		return nil, err
	}
	return photo, nil
}

func (s *service) DeletePhoto(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.DeletePhoto(ctx, id)
	if err != nil {
		s.logger.Error("error deleting photo: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadPhotoList(ctx context.Context) (photosList []*photo.Photo, err error) {
	photosList, err = s.repository.ReadPhotoList(ctx)
	if err != nil {
		s.logger.Error("error deleting photo: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadPhotosOfKviz(ctx context.Context, kvizId uuid.UUID) (photosList []*photo.Photo, err error) {
	photosList, err = s.repository.ReadPhotoListOfKviz(ctx, kvizId)
	if err != nil {
		s.logger.Error("error deleting photo: %s", err.Error())
		return
	}
	return
}
