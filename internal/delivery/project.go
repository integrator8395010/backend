package delivery

import (
	jsonProject "backend/internal/delivery/project"
	"backend/internal/domain/project"
	"context"
	"io"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CrateProject
// @Summary Создание проекта.
// @Description Создание проекта.
// @Tags project
// @Accept multipart/form-data
// @Produce json
// @Param data formData jsonProject.CreateProjectRequest true "Body"
// @Param file formData file true "Photo of project"
// @Success 201			{object} 	jsonProject.ProjectResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /project/ [put]
func (d *Delivery) CreateProject(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonProject.CreateProjectRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	project, err := project.New(request.Name, "", auth.UserId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	photo, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	var photoReader multipart.File
	photoReader, err = photo.Open()
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	defer photoReader.Close()

	photoContent, err := io.ReadAll(photoReader)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectResp, err := d.services.Project.CreateProject(context.Background(), project, photo.Filename, photoContent)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonProject.ToProjectResponse(projectResp))
}

// UpdateProject
// @Summary Обновление проекта.
// @Description Обновление проекта.
// @Tags project
// @Accept json
// @Produce json
// @Param data body jsonProject.UpdateProjectRequest true "Body"
// @Success 200			{object} 	jsonProject.ProjectResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /project/ [post]
func (d *Delivery) UpdateProject(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	request := jsonProject.UpdateProjectRequest{}
	if err := c.ShouldBindJSON(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.ID)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectNew, err := project.NewWithId(id, request.Name, "", auth.UserId, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldProject *project.Project) (*project.Project, error) {
		return project.NewWithId(oldProject.Id(), projectNew.Name(), oldProject.Photo(), oldProject.CreatedBy(), oldProject.CreatedAt(), projectNew.ModifiedAt())
	}

	projectResp, err := d.services.Project.UpdateProject(context.Background(), projectNew.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonProject.ToProjectResponse(projectResp))
}

// DeleteProject
// @Summary Удаление проекта.
// @Description Удаление проекта.
// @Tags project
// @Produce json
// @Param id path string true "Project ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /project/{id} [delete]
func (d *Delivery) DeleteProject(c *gin.Context) {
	id := c.Param("id")
	projectId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Project.DeleteProject(context.Background(), projectId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadProjectList
// @Summary Список проектов.
// @Description Список проектов.
// @Tags project
// @Produce json
// @Success 200			{object} 	jsonProject.ProjectListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /project/ [get]
func (d *Delivery) ReadProjectList(c *gin.Context) {
	projectList, err := d.services.Project.ReadProjectList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	projectListResponse := make([]*jsonProject.ProjectResponse, len(projectList))

	for index, project := range projectList {
		projectListResponse[index] = jsonProject.ToProjectResponse(project)
	}

	c.JSON(http.StatusOK, jsonProject.ProjectListResponse{Projects: projectListResponse})
}
