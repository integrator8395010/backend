package bitrixClient

type Client struct {
	baseUrl string
}

func NewBitrixClient(baseUrl string) *Client {
	return &Client{
		baseUrl: baseUrl,
	}
}
