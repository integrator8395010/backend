package repository

import (
	"backend/internal/domain/integration"
	"backend/internal/repository/bitrixIntegration/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "bitrix_integration"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"bitrix_integration_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateBitrixIntegration(ctx context.Context, integration *integration.BitrixIntegration) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	daoIntegration, err := r.toRepositoryDaoBitrixIntegration(integration)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsBitrixIntegration...).Values(daoIntegration.Id, daoIntegration.SiteId, daoIntegration.Responsible, daoIntegration.SendType, daoIntegration.Associations, daoIntegration.DefaultValues, daoIntegration.CreatedAt, daoIntegration.ModifiedAt)
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateBitrixIntegration(ctx context.Context, id uuid.UUID, updateFn func(integration *integration.BitrixIntegration) (*integration.BitrixIntegration, error)) (integration *integration.BitrixIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLead, err := r.oneIntegrationTx(ctx, tx, id)
	if err != nil {
		return
	}

	bitrixIntegrationNew, err := updateFn(upLead)
	if err != nil {
		return
	}

	daoIntegration, err := r.toRepositoryDaoBitrixIntegration(bitrixIntegrationNew)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("responsible", daoIntegration.Responsible).Set("site_id", daoIntegration.SiteId).Set("send_type", daoIntegration.SendType).Set("associations", daoIntegration.Associations).Set("default_values", daoIntegration.DefaultValues).Set("modified_at", daoIntegration.ModifiedAt).Where("id = ?", daoIntegration.Id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return bitrixIntegrationNew, nil
}

func (r *Repository) DeleteBitrixIntegration(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadBitrixIntegrationOfSite(ctx context.Context, siteId uuid.UUID) (integration *integration.BitrixIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsBitrixIntegration...).From(tableName).Where("site_id = ?", siteId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegration, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.BitrixIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainBitrixIntegration(&daoIntegration)
}

func (r *Repository) ReadAllSitesBitrixIntegrations(ctx context.Context) (integrationList []*integration.BitrixIntegration, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsBitrixIntegration...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoIntegrations, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.BitrixIntegration])
	if err != nil {
		return nil, err
	}

	integrationList = make([]*integration.BitrixIntegration, len(daoIntegrations))

	for index, daoIntegration := range daoIntegrations {
		domainIntegration, err := r.toDomainBitrixIntegration(&daoIntegration)
		if err != nil {
			return nil, err
		}

		integrationList[index] = domainIntegration
	}

	return
}

func (r *Repository) oneIntegrationTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *integration.BitrixIntegration, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsBitrixIntegration...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.BitrixIntegration])
	if err != nil {
		return nil, err
	}

	return r.toDomainBitrixIntegration(&daoLead)
}
