package delivery

import (
	jsonIntegration "backend/internal/delivery/integration"
	"backend/internal/domain/integration"
	"backend/internal/domain/integration/email"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateMailIntegration
// @Summary Создание интеграцию mail для сайта.
// @Description Создание интеграцию mail для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.CreateMailIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.MailIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/mail [put]
func (d *Delivery) CreateMailIntegration(c *gin.Context) {
	request := jsonIntegration.CreateMailIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	mailList := []email.Email{}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	for _, val := range request.Recipients {
		email, err := email.New(val)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		mailList = append(mailList, email)
	}

	mailIntegration, err := integration.NewMailIntegration(siteId, mailList...)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.CreateMailIntegration(context.Background(), mailIntegration)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToMailIntegrationResponse(mailIntegration))
}

// UpdateMailIntegration
// @Summary Обновление интеграцию mail для сайта.
// @Description Обновление интеграцию mail для сайта.
// @Tags integration
// @Accept json
// @Produce json
// @Param data body jsonIntegration.UpdateMailIntegrationRequest true "Body"
// @Success 201			{object} 	jsonIntegration.MailIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /integration/mail [post]
func (d *Delivery) UpdateMailIntegration(c *gin.Context) {
	request := jsonIntegration.UpdateMailIntegrationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	mailList := []email.Email{}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	siteId, err := uuid.Parse(request.SiteId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	for _, val := range request.Recipients {
		email, err := email.New(val)
		if err != nil {
			SetError(c, http.StatusBadRequest, err.Error())
			return
		}
		mailList = append(mailList, email)
	}

	updateFn := func(oldIntegration *integration.MailIntegration) (*integration.MailIntegration, error) {
		return integration.NewMailIntegrationWithId(id, siteId, oldIntegration.CreatedAt().Local(), time.Now(), mailList...)
	}

	mailIntegrationNew, err := d.services.Integration.UpdateMailIntegration(context.Background(), id, updateFn)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonIntegration.ToMailIntegrationResponse(mailIntegrationNew))
}

// DeleteMailIntegration
// @Summary Удаление интеграцию сайта.
// @Description Удаление интеграцию сайта.
// @Tags integration
// @Produce json
// @Param id path string true "MailIntegration ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/mail/{id} [delete]
func (d *Delivery) DeleteMailIntegration(c *gin.Context) {
	id := c.Param("id")
	integrationId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Integration.DeleteMailIntegration(context.Background(), integrationId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadMailIntegrationOfSite
// @Summary Получить интеграцию mail для сайта.
// @Description Получить интеграцию mail для сайта.
// @Tags integration
// @Produce json
// @Param id path string true "MailIntegration site ID"
// @Produce json
// @Success 200			{object} 	jsonIntegration.MailIntegrationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /integration/mail/{id} [get]
func (d *Delivery) ReadMailIntegrationOfSite(c *gin.Context) {
	id := c.Param("id")
	siteId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	mailIntegration, err := d.services.Integration.ReadMailIntegrationOfSite(context.Background(), siteId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonIntegration.ToMailIntegrationResponse(mailIntegration))
}
