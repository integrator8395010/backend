package repository

import (
	"backend/internal/domain/kviz"
	"backend/internal/repository/kviz/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "kviz"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"kviz_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateKviz(ctx context.Context, kviz *kviz.Kviz) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsKviz...).Values(kviz.Id(), kviz.SiteId(), kviz.Name(), kviz.TemplateId(), kviz.Background(), kviz.MainColor(), kviz.SecondaryColor(), kviz.SubTitle(), kviz.SubTitleItems(), kviz.PhoneStepTitle(), kviz.FooterTitle(), kviz.Phone(), kviz.Politics(), kviz.Roistat(), kviz.AdvantageTitle(), kviz.PhotosTitle(), kviz.PlansTitle(), kviz.ResultStepText(), kviz.Qoopler(), kviz.DmpOne(), kviz.ValidatePhone(), kviz.CreatedBy(), kviz.CreatedAt(), kviz.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateKviz(ctx context.Context, id uuid.UUID, updateFn func(kviz *kviz.Kviz) (*kviz.Kviz, error)) (kviz *kviz.Kviz, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upKviz, err := r.oneKvizTx(ctx, tx, id)
	if err != nil {
		return
	}

	kvizNew, err := updateFn(upKviz)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("site_id", kvizNew.SiteId()).Set("name", kvizNew.Name()).Set("template_id", kvizNew.TemplateId()).Set("background", kvizNew.Background()).Set("main_color", kvizNew.MainColor()).Set("secondary_color", kvizNew.SecondaryColor()).Set("sub_title", kvizNew.SubTitle()).Set("sub_title_items", kvizNew.SubTitleItems()).Set("phone_step_title", kvizNew.PhoneStepTitle()).Set("footer_title", kvizNew.FooterTitle()).Set("phone", kvizNew.Phone()).Set("politics", kvizNew.Politics()).Set("roistat", kvizNew.Roistat()).Set("advantages_title", kvizNew.AdvantageTitle()).Set("photos_title", kvizNew.PhotosTitle()).Set("plans_title", kvizNew.PlansTitle()).Set("result_step_text", kvizNew.ResultStepText()).Set("qoopler", kvizNew.Qoopler()).Set("dmp_one", kvizNew.DmpOne()).Set("validate_phone", kvizNew.ValidatePhone()).Set("created_by", kvizNew.CreatedBy()).Set("modified_at", kvizNew.ModifiedAt()).Where("id = ?", kvizNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)

	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return kvizNew, err
}

func (r *Repository) DeleteKviz(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) ReadKvizList(ctx context.Context) (kvizsList []*kviz.Kviz, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsKviz...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoKviz, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Kviz])
	if err != nil {
		return nil, err
	}

	kvizsList = []*kviz.Kviz{}

	for _, kviz := range daoKviz {
		kvizIns, err := r.toDomainKviz(&kviz)
		if err != nil {
			return nil, err
		}

		kvizsList = append(kvizsList, kvizIns)
	}

	return
}

func (r *Repository) ReadKvizListOfProject(ctx context.Context, projectId uuid.UUID) (kvizsList []*kviz.Kviz, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	columnsKvizWithPrefix := make([]string, len(dao.ColumnsKviz))

	for index, field := range dao.ColumnsKviz {
		columnsKvizWithPrefix[index] = "kviz." + field
	}

	rawQuery := r.Builder.Select(columnsKvizWithPrefix...).LeftJoin("site on kviz.site_id = site.id").From(tableName).Where("site.project_id = ?", projectId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoKviz, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Kviz])
	if err != nil {
		return nil, err
	}

	kvizsList = []*kviz.Kviz{}

	for _, kviz := range daoKviz {
		kvizIns, err := r.toDomainKviz(&kviz)
		if err != nil {
			return nil, err
		}

		kvizsList = append(kvizsList, kvizIns)
	}

	return
}

func (r *Repository) oneKvizTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *kviz.Kviz, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsKviz...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoKviz, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Kviz])
	if err != nil {
		return nil, err
	}

	return r.toDomainKviz(&daoKviz)
}
