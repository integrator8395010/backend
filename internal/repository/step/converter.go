package repository

import (
	"backend/internal/domain/step"
	"backend/internal/repository/step/dao"
)

func (r *Repository) toDomainStep(dao *dao.Step) (*step.Step, error) {
	return step.NewWithId(dao.Id, dao.KvizId, dao.Title, dao.Answers, dao.CreatedAt, dao.ModifiedAt)
}
