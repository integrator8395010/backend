package repository

import (
	"backend/internal/domain/plan"
	"backend/internal/repository/plan/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "plan"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"plan_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreatePlan(ctx context.Context, plan *plan.Plan) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsPlan...).Values(plan.Id(), plan.KvizId(), plan.Title(), plan.Photo(), plan.Rooms(), plan.TotalArea(), plan.LivingArea(), plan.KitchenArea(), plan.BathRoomArea(), plan.BathRoomArea(), plan.Price(), plan.CreatedAt(), plan.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdatePlan(ctx context.Context, id uuid.UUID, updateFn func(plan *plan.Plan) (*plan.Plan, error)) (plan *plan.Plan, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upPlan, err := r.onePlanTx(ctx, tx, id)
	if err != nil {
		return
	}

	planNew, err := updateFn(upPlan)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("kviz_id", planNew.KvizId()).Set("title", planNew.Title()).Set("photo", planNew.Photo()).Set("rooms", planNew.Rooms()).Set("total_area", planNew.TotalArea()).Set("living_area", planNew.LivingArea()).Set("kitchen_area", planNew.KitchenArea()).Set("bed_room_area", planNew.BedRoomArea()).Set("bath_room_area", planNew.BathRoomArea()).Set("price", planNew.Price()).Set("modified_at", planNew.ModifiedAt()).Where("id = ?", planNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeletePlan(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadPlanList(ctx context.Context) (plansList []*plan.Plan, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsPlan...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPlan, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Plan])
	if err != nil {
		return nil, err
	}

	plansList = []*plan.Plan{}

	for _, plan := range daoPlan {
		planIns, err := r.toDomainPlan(&plan)
		if err != nil {
			return nil, err
		}

		plansList = append(plansList, planIns)
	}
	return
}

func (r *Repository) ReadPlanListOfKviz(ctx context.Context, kvizId uuid.UUID) (plansList []*plan.Plan, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsPlan...).From(tableName).Where("kviz_id = ?", kvizId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPlan, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Plan])
	if err != nil {
		return nil, err
	}

	plansList = []*plan.Plan{}

	for _, plan := range daoPlan {
		planIns, err := r.toDomainPlan(&plan)
		if err != nil {
			return nil, err
		}

		plansList = append(plansList, planIns)
	}
	return
}

func (r *Repository) onePlanTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *plan.Plan, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsPlan...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPlan, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Plan])
	if err != nil {
		return nil, err
	}

	return r.toDomainPlan(&daoPlan)
}
