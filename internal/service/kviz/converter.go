package kviz

import (
	"backend/internal/domain/advantage"
	"backend/internal/domain/kviz"
	"backend/internal/domain/photo"
	"backend/internal/domain/plan"
	"backend/pkg/tools"
	"fmt"

	"github.com/google/uuid"
)

const (
	kvizBackgroundWidth = 1920
	planWidth           = 800
	advantageWidth      = 300
	photoWidth          = 1280
)

var (
	defaultTemplate, _ = uuid.Parse("cc8f3f63-bffa-425a-b363-6821e4c3e684")
)

func (s *service) toDomainKviz(kvizStruct *Kviz, userId uuid.UUID) (*kviz.Kviz, error) {
	siteId, err := uuid.Parse(kvizStruct.SiteId)
	if err != nil {
		return nil, err
	}
	backgroundFile, err := s.resizer.ResizeImage(kvizStruct.Background, kvizBackgroundWidth, 0)
	if err != nil {
		return nil, fmt.Errorf("error, while converting file: %s", err)
	}
	backgroundFileName, err := tools.SaveConvertedFileToFileStorage(backgroundFile)
	if err != nil {
		return nil, fmt.Errorf("error, while saving background file to storage: %s", err)
	}

	return kviz.New(siteId, kvizStruct.Name, defaultTemplate, backgroundFileName, kvizStruct.MainColor, kvizStruct.SecondaryColor, kvizStruct.SubTitle, kvizStruct.SubTitleItems, kvizStruct.PhoneStepTitle, kvizStruct.PhoneStepTitle, kvizStruct.Phone, kvizStruct.Politics, kvizStruct.Roistat, kvizStruct.AdvantagesTitle, kvizStruct.PhotosTitle, kvizStruct.PlansTitle, kvizStruct.ResultStepText, kvizStruct.Qoopler, kvizStruct.DmpOne, kvizStruct.ValidatePhone, userId)
}

func (s *service) toDomainPlanList(planStructs []*Plan, kvizId uuid.UUID) ([]*plan.Plan, error) {
	plans := make([]*plan.Plan, len(planStructs))

	for index, plan := range planStructs {
		plan, err := s.toDomainPlan(plan, kvizId)
		if err != nil {
			return nil, fmt.Errorf("error, while converting to domain plan: %s", err)
		}
		plans[index] = plan
	}
	return plans, nil
}

func (s *service) toDomainPlan(planStruct *Plan, kvizId uuid.UUID) (*plan.Plan, error) {
	planFile, err := s.resizer.ResizeImage(planStruct.Photo, planWidth, 0)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting file: %s", err)
	}
	planFileName, err := tools.SaveConvertedFileToFileStorage(planFile)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while saving background file to storage: %s", err)
	}
	return plan.New(kvizId, planStruct.Title, planFileName, planStruct.Rooms, planStruct.TotalArea, planStruct.LivingArea, planStruct.KitchenArea, planStruct.BedRoomArea, planStruct.BathRoomArea, planStruct.Price)
}

func (s *service) toDomainAdvantageList(advantageStructs []*Advantage, kvizId uuid.UUID) ([]*advantage.Advantage, error) {
	advantages := make([]*advantage.Advantage, len(advantageStructs))

	for index, advantage := range advantageStructs {
		advantage, err := s.toDomainAdvantage(advantage, kvizId)
		if err != nil {
			return nil, fmt.Errorf("error, while converting to domain advantage: %s", err)
		}
		advantages[index] = advantage
	}
	return advantages, nil
}

func (s *service) toDomainAdvantage(advantageStruct *Advantage, kvizId uuid.UUID) (*advantage.Advantage, error) {
	advantageFile, err := s.resizer.ResizeImage(advantageStruct.Photo, advantageWidth, 0)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting file: %s", err)
	}
	advantageFileName, err := tools.SaveConvertedFileToFileStorage(advantageFile)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while saving background file to storage: %s", err)
	}
	return advantage.New(kvizId, advantageStruct.Title, advantageFileName)
}

func (s *service) toDomainPhotosList(photos [][]byte, kvizId uuid.UUID) ([]*photo.Photo, error) {
	photosList := make([]*photo.Photo, len(photos))

	for index, photo := range photos {
		photo, err := s.toDomainPhoto(photo, kvizId)
		if err != nil {
			return nil, fmt.Errorf("error, while converting to domain advantage: %s", err)
		}
		photosList[index] = photo
	}
	return photosList, nil
}

func (s *service) toDomainPhoto(photoInput []byte, kvizId uuid.UUID) (*photo.Photo, error) {
	photoFile, err := s.resizer.ResizeImage(photoInput, photoWidth, 0)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while converting file: %s", err)
	}
	photoFileName, err := tools.SaveConvertedFileToFileStorage(photoFile)
	if err != nil {
		return nil, fmt.Errorf("CreateKvizComplex error, while saving background file to storage: %s", err)
	}
	return photo.New(kvizId, photoFileName)
}
