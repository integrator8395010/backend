package repository

import (
	"backend/internal/domain/advantage"
	"backend/internal/repository/advantage/dao"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "advantage"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"kviz_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateAdvantage(ctx context.Context, advantage *advantage.Advantage) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsAdvantage...).Values(advantage.Id(), advantage.KvizId(), advantage.Title(), advantage.Photo(), advantage.CreatedAt(), advantage.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateAdvantage(ctx context.Context, id uuid.UUID, updateFn func(advantage *advantage.Advantage) (*advantage.Advantage, error)) (advantage *advantage.Advantage, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upAdvantage, err := r.oneAdvantageTx(ctx, tx, id)
	if err != nil {
		return
	}

	advantageNew, err := updateFn(upAdvantage)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("kviz_id", advantageNew.KvizId()).Set("title", advantageNew.Title()).Set("photo", advantageNew.Photo()).Set("modified_at", advantageNew.ModifiedAt()).Where("id = ?", advantageNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteAdvantage(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}
	return
}

func (r *Repository) ReadAdvantageList(ctx context.Context) (advantagesList []*advantage.Advantage, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsAdvantage...).From(tableName).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoAdvantage, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Advantage])
	if err != nil {
		return nil, err
	}

	advantagesList = []*advantage.Advantage{}

	for _, advantage := range daoAdvantage {
		advantageIns, err := r.toDomainAdvantage(&advantage)
		if err != nil {
			return nil, err
		}

		advantagesList = append(advantagesList, advantageIns)
	}
	return
}

func (r *Repository) ReadAdvantagesOfKviz(ctx context.Context, kvizId uuid.UUID) (advantagesList []*advantage.Advantage, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsAdvantage...).From(tableName).Where("kviz_id = ?", kvizId).OrderBy("created_at")

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoAdvantage, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Advantage])
	if err != nil {
		return nil, err
	}

	advantagesList = []*advantage.Advantage{}

	for _, advantage := range daoAdvantage {
		advantageIns, err := r.toDomainAdvantage(&advantage)
		if err != nil {
			return nil, err
		}

		advantagesList = append(advantagesList, advantageIns)
	}
	return
}

func (r *Repository) oneAdvantageTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *advantage.Advantage, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsAdvantage...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoAdvantage, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Advantage])
	if err != nil {
		return nil, err
	}

	return r.toDomainAdvantage(&daoAdvantage)
}
