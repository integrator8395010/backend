package site

import (
	"backend/internal/domain/site"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreateSite(ctx context.Context, site *site.Site) (err error) {
	err = s.repository.CreateSite(ctx, site)
	if err != nil {
		s.logger.Error("error creating site: %s", err.Error())
		return
	}
	return
}

func (s *service) UpdateSite(ctx context.Context, id uuid.UUID, updateFn func(site *site.Site) (*site.Site, error)) (site *site.Site, err error) {
	site, err = s.repository.UpdateSite(ctx, id, updateFn)
	if err != nil {
		s.logger.Error("error updating site list: %s", err.Error())
		return
	}
	return
}

func (s *service) DeleteSite(ctx context.Context, id uuid.UUID) (err error) {
	err = s.repository.DeleteSite(ctx, id)
	if err != nil {
		s.logger.Error("error deleting site: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadSiteList(ctx context.Context) (sitesList []*site.Site, err error) {
	sitesList, err = s.repository.ReadSiteList(ctx)
	if err != nil {
		s.logger.Error("error reading sites list: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadSiteById(ctx context.Context, id uuid.UUID) (site *site.Site, err error) {
	return s.repository.ReadSiteById(ctx, id)
}

func (s *service) ReadSitesOfProject(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error) {
	sitesList, err = s.repository.ReadSitesOfProject(ctx, projectId)
	if err != nil {
		s.logger.Error("error reading sites list: %s", err.Error())
		return
	}
	return
}
