package lead

import (
	"backend/internal/domain/lead"
	"context"
	"time"

	"github.com/google/uuid"
)

type Service interface {
	CreateLead(ctx context.Context, lead *lead.Lead) (err error)
	UpdateLead(ctx context.Context, id uuid.UUID, updateFn func(lead *lead.Lead) (*lead.Lead, error)) (lead *lead.Lead, err error)
	DeleteLead(ctx context.Context, id uuid.UUID) (err error)
	ReadLeadList(ctx context.Context) (leadsList []*lead.Lead, err error)
	ReadFiltredLeadList(ctx context.Context, filter *lead.LeadsFilter, offset, limit int) (list []*lead.Lead, count int, err error)
	ReadSpamFiltredLeadList(ctx context.Context, filter *lead.SpamLeadsFilter) (list []*lead.Lead, err error)
	GetSpamLeadListExcel(ctx context.Context, filter *lead.SpamLeadsFilter) (fileName string, err error)
	ProcessLeadsToEmail(ctx context.Context) (err error)
	ProcessLeadsToBitrix(ctx context.Context) (err error)
	ProcessLeadsToAmocrm(ctx context.Context) (err error)
	ProcessLeadsToLeadactiv(ctx context.Context) (err error)
	GetLeadsStatisticsOfProject(ctx context.Context, projectId uuid.UUID, url string, from time.Time, to time.Time) (leadsStatistics *lead.LeadsStatistics, err error)
}
