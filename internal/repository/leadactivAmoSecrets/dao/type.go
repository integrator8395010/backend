package dao

import (
	"time"

	"github.com/google/uuid"
)

type LeadactivAmoSecrets struct {
	Id           uuid.UUID `db:"id"`
	BaseUrl      string    `db:"base_url"`
	ClientId     string    `db:"client_id"`
	ClientSecret string    `db:"client_secret"`
	RedirectUri  string    `db:"redirect_uri"`
	Token        string    `db:"token"`
	RefreshToken string    `db:"refresh_token"`
	CreatedAt    time.Time `db:"created_at"`
	ModifiedAt   time.Time `db:"modified_at"`
}

var ColumnsLeadactivAmoSecrets = []string{
	"id",
	"base_url",
	"client_id",
	"client_secret",
	"redirect_uri",
	"token",
	"refresh_token",
	"created_at",
	"modified_at",
}
