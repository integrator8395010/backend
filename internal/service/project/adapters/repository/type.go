package repository

import (
	"backend/internal/domain/project"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreateProject(ctx context.Context, project *project.Project) (err error)
	UpdateProject(ctx context.Context, id uuid.UUID, updateFn func(project *project.Project) (*project.Project, error)) (project *project.Project, err error)
	DeleteProject(ctx context.Context, id uuid.UUID) (err error)
	ReadProjectList(ctx context.Context) (projectsList []*project.Project, err error)
}
