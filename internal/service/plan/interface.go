package plan

import (
	"backend/internal/domain/plan"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreatePlan(ctx context.Context, plan *plan.Plan, inputFileName string, inputFileContent []byte) (*plan.Plan, error)
	UpdatePlan(ctx context.Context, plan *plan.Plan, inputFileName string, inputFileContent []byte) (*plan.Plan, error)
	DeletePlan(ctx context.Context, id uuid.UUID) (err error)
	ReadPlanList(ctx context.Context) (plansList []*plan.Plan, err error)
	ReadPlanListOfKviz(ctx context.Context, kvizId uuid.UUID) (plansList []*plan.Plan, err error)
}
