package site

import (
	"backend/internal/domain/site"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreateSite(ctx context.Context, site *site.Site) (err error)
	UpdateSite(ctx context.Context, id uuid.UUID, updateFn func(site *site.Site) (*site.Site, error)) (site *site.Site, err error)
	DeleteSite(ctx context.Context, id uuid.UUID) (err error)
	ReadSiteList(ctx context.Context) (sitesList []*site.Site, err error)
	ReadSitesOfProject(ctx context.Context, projectId uuid.UUID) (sitesList []*site.Site, err error)
	ReadSiteById(ctx context.Context, id uuid.UUID) (site *site.Site, err error)
}
