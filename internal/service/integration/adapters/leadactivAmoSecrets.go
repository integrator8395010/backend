package repository

import (
	"backend/internal/domain/integration"
	"context"
)

type LeadactivAmoSecretsRepository interface {
	CreateLeadactivAmoSecrets(ctx context.Context, integration *integration.LeadactivAmoSecrets) (err error)
	UpdateLeadactivAmoSecrets(ctx context.Context, updateFn func(integration *integration.LeadactivAmoSecrets) (*integration.LeadactivAmoSecrets, error)) (integration *integration.LeadactivAmoSecrets, err error)
	DeleteLeadactivAmoSecrets(ctx context.Context) (err error)
	ReadLeadactivAmoSecrets(ctx context.Context) (leadactivAmoSecrets *integration.LeadactivAmoSecrets, err error)
}
